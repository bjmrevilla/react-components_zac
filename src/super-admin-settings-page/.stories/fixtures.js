export const testRoles =
[{name: 'role 1'},
 {name: 'role 2'},
 {name: 'role 3'}];

 export const testAdminRoles =
[{name: 'admin 1',
  canCreateManual: true,
  canViewAdminsWithRoles: [
  { name: 'admin 1' }
],
canEditAdminWithRoles: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
],
canAddToAdminRoles: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
],
canDeleteAdminWithRoles: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
],
canViewProgramStaffWithRoles: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
],
canEditProgramStaffWithRoles: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
],
canAddToProgramStaffRoles: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
],
canDeleteProgramStaffWithRoles: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
]},
 {name: 'admin 2',
  canViewAdminsWithRoles: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
],
canEditAdminWithRoles: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
],
canAddToAdminRoles: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
],
canDeleteAdminWithRoles: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
],
canViewProgramStaffWithRoles: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
],
canEditProgramStaffWithRoles: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
],
canAddToProgramStaffRoles: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
],
canDeleteProgramStaffWithRoles: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
]},
 {name: 'admin 3',
  canViewAdminWithRoles: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
],
canEditAdminWithRoles: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
],
canAddToAdminRoles: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
],
canDeleteAdminWithRoles: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
],
canViewProgramStaffWithRoles: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
],
canEditProgramStaffWithRoles: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
],
canAddToProgramStaffRoles: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
],
canDeleteProgramStaffWithRoles: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
]}];

 export const testProgramStaffRoles =
[{name: 'programStaff 1',
  canViewMaterialsFilteredWith: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
], isExemptedToExamsOnMaterialsWithTag: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
]},
 {name: 'programStaff 2',
  canViewMaterialWithTag: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
], isExemptedToExamsOnMaterialsWithTag: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
]},
 {name: 'programStaff 3',
  canViewMaterialWithTag: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
], isExemptedToExamsOnMaterialsWithTag: [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
]}];

export let psRole = testProgramStaffRoles[0];
export let adminRole = testAdminRoles[0];

export const selectPSRole = role => {
  psRole = role;
  console.log(role);
}
export const selectAdminRole = role => {
  adminRole = role;
  console.log(role);
}
export const savePSRole = (changes, cb) => {console.log(changes) };
export const saveAdminRole = (changes, cb) => {console.log(changes) };
export const sourceViewFilters = [
  {name: 'Chip'},
  {name: 'Chap'},
  {name: 'Chop'}
];

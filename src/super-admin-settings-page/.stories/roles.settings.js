import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RolesSettings from '../roles-settings';
import {testRoles} from './fixtures';



storiesOf('SuperAdminSettingsPage',module)
  .add('roles.settings', () => (
    <MuiThemeProvider>
      <RolesSettings roles={testRoles} />
    </MuiThemeProvider>
  ));
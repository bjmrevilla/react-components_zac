import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RoleSettings from '../role-settings';
import {testRoles} from './fixtures';

// Deprecated

storiesOf('SuperAdminSettingsPage',module)
  .add('role.settings', () => (
    <MuiThemeProvider>
      <RoleSettings role={testRoles[0]} />
    </MuiThemeProvider>
  ));
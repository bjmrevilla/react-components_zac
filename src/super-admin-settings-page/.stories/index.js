import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import SuperAdminSettingsPage from '../super-admin-settings-page.component.jsx';
import * as f from './fixtures';

storiesOf('SuperAdminSettingsPage',module)
  .add('component', () => (
    <MuiThemeProvider>
      <SuperAdminSettingsPage
        psRole={f.psRole}
        adminRole={f.adminRole}
        selectPSRole={f.selectPSRole}
        selectAdminRole={f.selectAdminRole}
        savePSRole={f.savePSRole}
        saveAdminRole={f.saveAdminRole}
        sourceAdminRoles={f.testAdminRoles}
        sourceExemptionFilters={f.sourceViewFilters}
        sourceViewFilters={f.sourceViewFilters}
        sourceProgramStaffRoles={f.testProgramStaffRoles} />
    </MuiThemeProvider>
  ));

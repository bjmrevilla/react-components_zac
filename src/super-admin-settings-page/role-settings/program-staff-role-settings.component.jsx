'use strict';

import React from 'react';
import TagsSetting from './tags-setting.component.jsx';
import Section from './section.component.jsx';
import styles from './role-settings.scss';

export default class ProgramStaffRoleSettings extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    role: React.PropTypes.object.isRequired,
    onChange: React.PropTypes.func.isRequired,
    edit: React.PropTypes.bool
  }

  static defaultProps = {
    edit: false
  }

  getTagsSettings = () => {
    var settingsArrangement =
    [{description: 'I can view materials with tags:',
      field: 'canViewMaterialsFilteredWith'},
     {description: 'I am exempted to assessment on materials with tags:',
      field: 'isExemptedFromExamsOnMaterialsFilteredWith'}];

    return settingsArrangement.map((tagsSetting, index) => {
      let source = (index === 0)
        ? this.props.sourceViewFilters
        : this.props.sourceExemptionFilters;
      let search = (index === 0)
        ? this.props.searchViewFilters
        : this.props.searchExemptionFilters;
      let add = (index === 0)
        ? this.props.addViewFilterAction
        : this.props.addExemptionFilterAction;
      let setting = this.props.role[tagsSetting.field] || [];
      let handleChange = value => this.props.onChange(tagsSetting.field,
        value);

      return (
        <TagsSetting
          key={index}
          description={tagsSetting.description}
          edit={this.props.edit}
          setting={setting}
          source={source}
          search={search}
          addAction={add}
          onChange={handleChange}
          disabled={!this.props.edit} />
      );
    });
  }

  render() {
    return (
      <div className={styles.root}>
        <Section title='Manuals'>
          {this.getTagsSettings() }
        </Section>
      </div>
    );
  }
}

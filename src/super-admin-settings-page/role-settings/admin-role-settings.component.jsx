'use strict';

import React from 'react';
import TagsSetting from './tags-setting.component.jsx';
import Section from './section.component.jsx';
import styles from './role-settings.scss';
import Toggle from 'material-ui/Toggle';

export default class AdminRoleSettings extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    role: React.PropTypes.object.isRequired,
    onChange: React.PropTypes.func.isRequired,
    edit: React.PropTypes.bool,
    adminRoles: React.PropTypes.arrayOf(React.PropTypes.object)
      .isRequired,
    programStaffRoles: React.PropTypes.arrayOf(React.PropTypes.object)
      .isRequired
  };

  static defaultProps = {
    edit: false
  };

  getAdminTagsSettings = () => {
    //set this up, because I have to hardcode one way or another
    var settingsArrangement =
      [{
        description: 'I can view admins with roles:',
        field: 'canViewAdminsWithRoles'
      },
      {
        description: 'I can edit admins with roles:',
        field: 'canEditAdminsWithRoles'
      },
      {
        description: 'I can add these roles to admins I can edit:',
        field: 'canPromoteDemoteAdminsUsingRoles'
      },
      {
        description: 'I can delete admins with roles:',
        field: 'canDeleteAdminsWithRoles'
      }];

    return settingsArrangement.map((tagsSetting, index) => {
      let setting = this.props.role[tagsSetting.field] || [];
      let handleChange = value => this.props.onChange(tagsSetting.field,
        value);

      return (
        <TagsSetting
          key={index}
          description={tagsSetting.description}
          setting={setting}
          source={this.props.adminRoles}
          edit={this.props.edit}
          onChange={handleChange}
          search={this.props.searchAdminRoles}
          disabled={!this.props.edit} />
      );
    });
  };

  getProgramStaffTagsSettings = () => {
    var settingsArrangement =
      [{
        description: 'I can view programStaff with roles:',
        field: 'canViewPSWithRoles'
      },
      {
        description: 'I can edit programStaff with roles:',
        field: 'canEditPSWithRoles'
      },
      {
        description: 'I can add these roles to programStaff I can edit:',
        field: 'canPromoteDemotePSUsingRoles'
      },
      {
        description: 'I can delete programStaff with roles:',
        field: 'canDeletePSWithRoles'
      }];

    return settingsArrangement.map((tagsSetting, index) => {
      let setting = this.props.role[tagsSetting.field] || [];
      let handleChange = value => this.props.onChange(tagsSetting.field,
        value);

      return (
        <TagsSetting
          key={index}
          description={tagsSetting.description}
          setting={setting}
          source={this.props.programStaffRoles}
          edit={this.props.edit}
          onChange={handleChange}
          search={this.props.searchProgramStaffRoles}
          disabled={!this.props.edit} />
      );
    });
  };

  getManualSettings = () => {
    var settingsArrangement =
      [{
        description: 'Can create manuals',
        field: 'canCreateManual'
      },
      {
        description: 'Can delete manuals',
        field: 'canDeleteManual'
      }];

    return settingsArrangement.map((settingArrangement, index) => {
      let setting = this.props.role[settingArrangement.field];
      // console.log(setting);
      let handleChange = (e, value) => this.props.onChange(settingArrangement.field, value);

      return <Toggle
				key={index}
        label={settingArrangement.description}
        toggled={setting}
        onToggle={handleChange}
        disabled={!this.props.edit} />;
    });
  };

  getCurriculumSettings = () => {
    var settingsArrangement =
      [{
        description: 'Can create curricula',
        field: 'canCreateCurriculum'
      },
      {
        description: 'Can delete curricula',
        field: 'canDeleteCurriculum'
      }];

    return settingsArrangement.map((settingArrangement, index) => {
      let setting = this.props.role[settingArrangement.field];
      let handleChange = (e, value) => this.props.onChange(settingArrangement.field, value);

      return <Toggle
				key={index}
        label={settingArrangement.description}
        toggled={setting}
        onToggle={handleChange}
        disabled={!this.props.edit} />;
    });
  };

  getRoleSettings = () => {
    var settingsArrangement =
      [{
        description: 'Can create roles',
        field: 'canCreateRole'
      },
      {
        description: 'Can delete roles',
        field: 'canDeleteRole'
      }];

    return settingsArrangement.map((settingArrangement, index) => {
      let setting = this.props.role[settingArrangement.field];
      let handleChange = (e, value) => this.props.onChange(settingArrangement.field, value);

      return <Toggle
				key={index}
        label={settingArrangement.description}
        toggled={setting}
        onToggle={handleChange}
        disabled={!this.props.edit} />;
    });
  };

  getAccountSettings = () => {
    var settingsArrangement =
      [{
        description: 'Can create admin accounts',
        field: 'canCreateAdmin'
      },
      {
        description: 'Can create program staff accounts',
        field: 'canCreateProgramStaff'
      }];

    return settingsArrangement.map((settingArrangement, index) => {
      let setting = this.props.role[settingArrangement.field];
      let handleChange = (e, value) => this.props.onChange(settingArrangement.field, value);

      return <Toggle
				key={index}
        label={settingArrangement.description}
        toggled={setting}
        onToggle={handleChange}
        disabled={!this.props.edit} />;
    });
  };

  getStatisticsSettings = () => {
    var settingsArrangement =
      [{
        description: 'Can view statistics',
        field: 'canViewStatistics'
      }];

    return settingsArrangement.map((settingArrangement, index) => {
      let setting = this.props.role[settingArrangement.field];
      let handleChange = (e, value) => this.props.onChange(settingArrangement.field, value);

      return <Toggle
				key={index}
        label={settingArrangement.description}
        toggled={setting}
        onToggle={handleChange}
        disabled={!this.props.edit} />;
    });
  };

  render() {
    return (
      <div className={styles.root}>
        <Section title='Manual Settings'>
          {this.getManualSettings()}
        </Section>
        <Section title='Curriculum Settings'>
          {this.getCurriculumSettings()}
        </Section>
        <Section title='Role Settings'>
          {this.getRoleSettings()}
        </Section>
        <Section title='Account Settings'>
          {this.getAccountSettings()}
        </Section>
        <Section title='Statistics Settings'>
          {this.getStatisticsSettings()}
        </Section>
        <Section title='Admin accounts'>
          {this.getAdminTagsSettings()}
        </Section>
        <Section title='ProgramStaff accounts'>
          {this.getProgramStaffTagsSettings()}
        </Section>
      </div>
    );
  }
}

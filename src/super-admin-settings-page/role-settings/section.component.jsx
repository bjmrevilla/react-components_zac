'use strict';

import React from 'react';
import styles from './section.scss';

export default class Section extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    title: React.PropTypes.string.isRequired
  };

  render() {
    return (
      <div className={styles.root}>
        <span className={styles.header}>
          {this.props.title}
        </span>
        <hr className={styles.separator} />
        {this.props.children}
      </div>
    );
  }
}

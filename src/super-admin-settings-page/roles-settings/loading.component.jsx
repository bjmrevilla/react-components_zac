'use strict';

import React from 'react';
import styles from './loading.scss';
import CircularProgress from 'material-ui/CircularProgress';

export default class Loading extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    saveStatus: React.PropTypes.oneOf(['loading', 'loaded', 'failed'])
  }

  static defaultProps = {
    saveStatus: 'loaded'
  }

  getLoading = () => {
    if (this.props.saveStatus === 'loading') {
      return (
        <CircularProgress size={0.5} />
      );
    }
    if (this.props.saveStatus === 'loaded') {
      return '';
    }
    if (this.props.saveStatus === 'failed') {
      return (
        <span className={styles.failedText}>
          Couldn't save.
        </span>
      );
    }
  }

  render() {
    return (
      <div className={styles.root}>
        {this.getLoading() }
      </div>
    );
  }
}
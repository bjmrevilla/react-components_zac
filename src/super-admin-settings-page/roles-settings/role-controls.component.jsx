'use strict';

import React from 'react';
import injectTapEventPlugin from 'react-tap-event-plugin';
import Loading from './loading.component.jsx';
import styles from './role-controls.scss';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';

export default class RoleControls extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    roles: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
    role: React.PropTypes.object,
    onRoleChange: React.PropTypes.func.isRequired,
    handleSave: React.PropTypes.func.isRequired,
    handleEdit: React.PropTypes.func.isRequired,
    edit: React.PropTypes.bool,
    saveStatus: React.PropTypes.oneOf(['loaded', 'loading', 'failed']),
    handleCancel: React.PropTypes.func.isRequired
  };

  static defaultProps = {
    saveStatus: 'loaded',
    edit: false
  };

  componentWillMount() {
    try {
      injectTapEventPlugin();
    } catch (err) {

    }
  }

  getMenuItems = () => {
    return this.props.roles.map((role, index) => {
      return <MenuItem
        key={index}
        value={role.name}
        primaryText={role.name} />;
    });
  };

  getIsDisabled = () => {
    return (this.props.saveStatus === 'loading');
  };

  getButtons = () => {
    return (this.props.edit)
      ? (
        <div className={styles.buttons}>
         <Loading saveStatus={this.props.saveStatus} />
          <FlatButton
            className={styles.cancelButton}
            label='Cancel'
            labelStyle={{ color: '#D50000' }}
            onClick={this.props.handleCancel}
            disabled={this.getIsDisabled()} />
          <RaisedButton
            label='Save'
            primary={true}
            onClick={this.props.handleSave}
            disabled={this.getIsDisabled()} />
        </div>
        )
      : <RaisedButton
          label='Edit'
          primary={true}
          onClick={this.props.handleEdit}
          disabled={this.getIsDisabled()} />;
  };

  render() {
    return (
      <div className={styles.root}>
        <div className={styles.rolePicker}>
          <SelectField
            value={this.props.role.name}
            onChange={this.props.onRoleChange}
            disabled={this.getIsDisabled()}>
            {this.getMenuItems() }
          </SelectField>
          <span className={styles.rolePickerLabel}>
            settings for role {this.props.role.name}
          </span>
        </div>
        <span className={styles.settings}>
          Settings
        </span>
        {this.getButtons()}
      </div>
    );
  }
}

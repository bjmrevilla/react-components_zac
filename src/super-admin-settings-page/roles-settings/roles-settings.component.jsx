'use strict';

import _ from 'lodash';
import React from 'react';
import RoleSettings from '../role-settings/role-settings.component.jsx';
import RoleControls from './role-controls.component.jsx';
import styles from './roles-settings.scss';
import { diff } from '../../list-view-page/utils/utils.js';

export default class RolesSettings extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tempRole: this.clone(this.props.role),
      saveStatus: 'loaded', //enum: loaded, loading, failed
      edit: false
    };
  }

  componentWillUpdate = (newProps) => {
  	const { role } = newProps;
		const { tempRole } = this.state;
		if (!_.isEmpty(role) && _.isEmpty(tempRole))
			this.setState({ tempRole: { ...role } })
	};

  static propTypes = {
    roles: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
    role: React.PropTypes.object,
    selectRole: React.PropTypes.func,
    save: React.PropTypes.func.isRequired,
    type: React.PropTypes.oneOf(['admin', 'programStaff']).isRequired,
    adminRoles: React.PropTypes.arrayOf(React.PropTypes.object),
    programStaffRoles: React.PropTypes.arrayOf(React.PropTypes.object)
  };

  clone = (obj) => {
    // deep clone by just reparsing the stringified object
    return JSON.parse(JSON.stringify(obj));
  };

  handleChange = (field, value) => {
    let newTempRole = this.state.tempRole;

    newTempRole[field] = value;

    this.setState({ tempRole: newTempRole });
  };

  onRoleChange = (event, index, value) => {
    let hasSelectedName = role => role.name === value;
    let selectedRole = this.props.roles.find(hasSelectedName);
    this.props.selectRole(selectedRole);

    this.setState({
      tempRole: this.clone(this.props.role),
      edit: false
    });
  };

  handleCancel = () => {
    this.setState({
      tempRole: this.clone(this.props.role),
      edit: false
    });
  };

  handleEdit = () => {
    this.setState({ edit: true });
  };

  handleSave = () => {
    this.setState({ saveStatus: 'loading' });

    let changes = diff(this.props.role, this.state.tempRole);

    this.props.save(changes, (err) => {
      if (err) {
        this.setState({ saveStatus: 'failed' });
      } else {
        this.setState({
          tempRole: this.clone(this.props.role),
          edit: false,
          saveStatus: 'loaded'
        });
      }
    });
  };

  render() {
    return (
      <div className={styles.root}>
        <RoleControls
          roles={this.props.roles}
          role={this.state.tempRole}
          onRoleChange={this.onRoleChange}
          handleEdit={this.handleEdit}
          edit={this.state.edit}
          saveStatus={this.state.saveStatus}
          handleCancel={this.handleCancel}
          handleSave={this.handleSave} />
        <RoleSettings
          type={this.props.type}
          role={this.state.tempRole}
          onChange={this.handleChange}
          edit={this.state.edit && !(this.state.saveStatus === 'loading')}
          sourceViewFilters={this.props.sourceViewFilters}
          sourceExemptionFilters={this.props.sourceExemptionFilters}
          adminRoles={this.props.adminRoles}
          searchAdminRoles={this.props.searchAdminRoles}
          searchProgramStaffRoles={this.props.searchProgramStaffRoles}
          searchExemptionFilters={this.props.searchExemptionFilters}
          searchViewFilters={this.props.searchViewFilters}
          addViewFilterAction={this.props.addViewFilterAction}
          addExemptionFilterAction={this.props.addExemptionFilterAction}
          programStaffRoles={this.props.programStaffRoles} />
      </div>
    );
  }
}

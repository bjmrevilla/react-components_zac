/**
 * Created by metsys on 11/14/16.
 */
'use strict';
const log = require('debug')('CoverPic::Component');

import React, { PropTypes } from 'react';
import styles from './cover-photo.scss';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog';
import Dropzone from 'react-dropzone';
import CircularProgress from 'material-ui/CircularProgress';

const STATUS_NORMAL = 'status::normal';
const STATUS_LOADING = 'status::loading';

export default class CurriculumDetails extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			open: false,
			openPreview: false,
			status: STATUS_NORMAL
		}
	}

	static PropTypes = {
		prefix: PropTypes.string,
		cover: PropTypes.string,
		coverKey: PropTypes.string,

		defaultCover: PropTypes.string,

		readOnly: PropTypes.bool,
		disabled: PropTypes.bool,

		uploadFunc: PropTypes.func,
		onChange: PropTypes.func
	};

	static defaultProps = {
		defaultCover: '/',
		prefix: '/cdn/',
		cover: '',
		coverKey: 'cover',
		readOnly: false,
		disabled: false,
		uploadFunc: () => {},
		onChange: PropTypes.func
	};

	onDrop = (files) => {
		if (!files.length) return;
		const { uploadFunc, onChange, coverKey } = this.props;

		this.setState({ status: STATUS_LOADING });
		uploadFunc(files[0], (err, filename) => {
			if (err) {

				// TODO: improve
				alert(err.message);

			} else {

				onChange(coverKey, filename);

			}

			this.setState({ status: STATUS_NORMAL, open: false });
		});
	};

	onDropRejected = () => {
		// TODO: improve
		alert('Drop rejected');
	};

	render(){
		const { openPreview, open, status } = this.state;
		const { readOnly, disabled } = this.props;
		const { cover, prefix, defaultCover } = this.props;
		const _src = cover
			? `${prefix}${cover}`
			: defaultCover;

		log(`#render readOnly ${readOnly}`);
		log(`#render disabled ${disabled}`);
		log(`#render src ${_src}`);

		return (
			<Paper className={`${styles.root} ${readOnly? '': styles.rootWithButton}`.trim()}>
				<img
					className={styles.pic}
					src={_src}
					onTouchTap={() => this.setState({ openPreview: true })}
				/>
				{
					!readOnly &&
					<RaisedButton
						onClick={() => this.setState({ open: true })}
						label='Upload Picture'
						primary
						disabled={disabled}
					/>
				}

				<Dialog
					open={openPreview}
					onRequestClose={() => this.setState({ openPreview: false })}
					contentClassName={styles.preview}
				>
					<img
						className={styles.previewPic}
						src={_src}
						alt="Preview"
					/>
				</Dialog>

				<Dialog
					title='Upload Picture'
					modal={status === STATUS_LOADING}
					open={open}
					onRequestClose={() => this.setState({ open: false })}
				>
					{
						status === STATUS_LOADING
							? <CircularProgress size={0.5} />
							: <Dropzone
							className={styles.dropzone}
							ref='dropzone'
							multiple={false}
							accept='image/*'
							onDrop={this.onDrop}
							onDropRejected={this.onDropRejected}
							disableClick
							onTouchTap={ e => {
								e.preventDefault();
								this.refs.dropzone.open();
							}}
						>

							<div>
										<span className={styles.dropzoneHeader}>
											Drag and drop an image here.
										</span>
								<br/>
								<span className={styles.dropzoneText}>
											Alternatively, click this area to browse your PC.
										</span>
							</div>

						</Dropzone>
					}
				</Dialog>
			</Paper>
		)
	}
}

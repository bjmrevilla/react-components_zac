import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import ProfilePage from '..';
import {testAccount,
  testOnChange,
  testSave,
  testDelete,
  testChangePassword,
  testUploadProfilePic,
  testCancel,
  testSourceRoles,
  testSourceViewFilters,
  testSourceExemptionFilters} from './fixtures';

storiesOf('ProfilePage',module)
  .add('component', () => (
    <MuiThemeProvider>
      <ProfilePage account={testAccount}
        save={testSave}
        delete={testDelete}
        canChangePassword={true}
        changePassword={testChangePassword}
        uploadProfilePic={testUploadProfilePic}
        sourceRoles={testSourceRoles}
        sourceViewFilters={testSourceViewFilters}
        sourceExemptionFilters={testSourceExemptionFilters} />
    </MuiThemeProvider>
  ));
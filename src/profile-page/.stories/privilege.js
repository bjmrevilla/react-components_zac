import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Privilege from '../privilege';
import {testAccount,
  testOnChange,
  testSourceRoles,
  testSourceViewFilters,
  testSourceExemptionFilters} from './fixtures';

storiesOf('ProfilePage',module)
  .add('privilege', () => (
    <MuiThemeProvider>
      <Privilege account={testAccount}
        onChange={testOnChange}
        sourceRoles={testSourceRoles}
        sourceViewFilters={testSourceViewFilters}
        sourceExemptionFilters={testSourceExemptionFilters} />
    </MuiThemeProvider>
  ));
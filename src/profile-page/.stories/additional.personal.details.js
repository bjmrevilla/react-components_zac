import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AdditionalPersonalDetails from '../additional-personal-details';

storiesOf('ProfilePage',module)
  .add('additionalPersonalDetails', () => (
    <MuiThemeProvider>
      <AdditionalPersonalDetails />
    </MuiThemeProvider>
  ));
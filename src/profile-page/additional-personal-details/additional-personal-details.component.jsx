'use strict';

import React from 'react';
import injectTapEventPlugin from 'react-tap-event-plugin';
import styles from './additional-personal-details.scss';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import DatePicker from 'material-ui/DatePicker';

export default class AdditionalPersonalDetails extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    account: React.PropTypes.object.isRequired,
    saveStatus: React.PropTypes.oneOf(['loaded', 'loading', 'failed']),
    readOnly: React.PropTypes.bool,
    onChange: React.PropTypes.func.isRequired
  };

  static defaultProps = {
    saveStatus: 'loaded',
    readOnly: false
  };

  // make better
  handleSelectMaritalStatus = (event, index, value) => {
    this.props.onChange('maritalStatus', value);
  }

  handleSelectSex = (event, index, value) => {
    this.props.onChange('sex', value);
  }

  handleSelectBirthdate = (event, date) => {
    this.props.onChange('dateOfBirth', date.toISOString());
  }

  getIsDisabled = () => {
    return (this.props.saveStatus === 'loading')
      || (this.props.readOnly);
  };

  render() {
    return (
      <div className={styles.root}>
        <div className={styles.dropdownPickers}>
          <div className={styles.maritalStatusPicker}>
            <SelectField
              className={styles.select}
              value={this.props.account.maritalStatus}
              floatingLabelText='Marital Status'
              onChange={this.handleSelectMaritalStatus}
              disabled={this.getIsDisabled()}>
              <MenuItem value='single' primaryText='Single' />
              <MenuItem value='married' primaryText='Married' />
              <MenuItem value='separated' primaryText='Separated' />
              <MenuItem value='divorced' primaryText='Divorced' />
              <MenuItem value='widowed' primaryText='Widowed' />
            </SelectField>
          </div>
          <div className={styles.sexPicker}>
            <SelectField
              className={styles.select}
              value={this.props.account.sex}
              floatingLabelText={'Sex'}
              onChange={this.handleSelectSex}
              disabled={this.getIsDisabled()}>
              <MenuItem value='male' primaryText='Male' />
              <MenuItem value='female' primaryText='Female' />
            </SelectField>
          </div>
        </div>
        <DatePicker
          className={styles.birthdatePicker}
          value={new Date(this.props.account.dateOfBirth)}
          onChange={this.handleSelectBirthdate}
          floatingLabelText='Birthdate'
          container='inline'
          autoOk={true}
          disabled={this.getIsDisabled()} />
      </div>
    );
  }
}

'use strict';

import React from 'react';
import Field from './field.component.jsx';
import styles from './fields-row.scss';

export default class FieldsRow extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    fields: React.PropTypes.array.isRequired,
    account: React.PropTypes.object,
    onChange: React.PropTypes.func.isRequired,
    disabled: React.PropTypes.bool
  }

  static defaultProps = {
    disabled: false
  }

  getRowItems = () => {
    let walkPathArray = (item, path) => {
      let tempValue = item;

      path.forEach((pathKey) => {
        tempValue = tempValue[pathKey];
      });

      return tempValue;
    };

    return this.props.fields.map((path) => {
      let key = '';
      let value = '';
      if (typeof path === 'string') {
        key = path;
        value = this.props.account[key];
      } else {
        key = path[path.length - 1]; // last key in path
        if (path[1] === 'barangay') key = path[1];

        value = walkPathArray(this.props.account, path);
      }

      let onChange = (newValue) => {
        this.props.onChange(path, newValue);
      };

      return (
        <Field
          key={key}
          field={key}
          value={value}
          onChange={onChange}
          disabled={this.props.disabled} />
      );
    });
  }

  render() {
    return (
      <div className={styles.root}>
        {this.getRowItems()}
      </div>
    );
  }
}
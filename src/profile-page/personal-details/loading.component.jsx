'use strict';

import React from 'react';
import styles from './loading.scss';
import CircularProgress from 'material-ui/CircularProgress';

export default class Loading extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    uploadStatus: React.PropTypes.oneOf(['loading', 'loaded', 'failed'])
  }

  static defaultProps = {
    uploadStatus: 'loaded'
  }

  getLoading = () => {
    if (this.props.uploadStatus === 'loading') {
      return (
        <CircularProgress size={0.5} />
      );
    }
    if (this.props.uploadStatus === 'loaded') {
      return '';
    }
    if (this.props.uploadStatus === 'failed') {
      return (
        <span className={styles.failedText}>
          Couldn't upload.
        </span>
      );
    }
  }

  render() {
    return (
      <div className={styles.root}>
        {this.getLoading() }
      </div>
    );
  }
}
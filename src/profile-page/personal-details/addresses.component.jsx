'use strict';

import React from 'react';
import Address from './address.component.jsx';
import styles from './addresses.scss';

export default class Addresses extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    account: React.PropTypes.object,
    onChange: React.PropTypes.func,
    disabled: React.PropTypes.bool,

  };

  getAddressArrangement = () => {
    return [
      'country',
      'region',
      'province',
      'municipality',
      'barangay'
    ];
  };

  getSourceMapping = () => {
    return {
			barangay: 'barangays',
      municipality: 'municipalities',
      province: 'provinces',
      region: 'regions',
      country: 'countries'
    };
  };

  getSelectMapping = () => {
    return {
      municipality: 'selectMunicipality',
      province: 'selectProvince',
      region: 'selectRegion',
      country: 'selectCountry'
    };
  };

  getAddresses = () => {
    if (!this.props.account
      || !this.props.account.address
      // || this.props.account.address.length === 0 // address not an array
		) {
      return;
    }

    return this.getAddressArrangement().map((key) => {
      let path = ['address', key, 'name'];
      let onChange = (newValue) => {
        this.props.onChange(path, newValue);
      };
      let value = this.props.account.address[key].name;
      let sourceKey = this.getSourceMapping()[key];
      let source = this.props[sourceKey];
      let selectKey = this.getSelectMapping()[key];
      let onSelect = (selectKey) ? this.props[selectKey] : null;

      return <Address
        key={key}
        field={key}
        value={value}
        source={source}
        onChange={onChange}
        onSelect={onSelect}
        disabled={this.props.disabled} />;
    });
  };

  render = () => {
    return (
      <div className={styles.root}>
        {this.getAddresses()}
      </div>
    );
  };
}

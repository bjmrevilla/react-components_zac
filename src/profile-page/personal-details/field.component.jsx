'use strict';

import React from 'react';
import styles from './field.scss';
import TextField from 'material-ui/TextField';

export default class Field extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    field: React.PropTypes.string.isRequired,
    value: React.PropTypes.string.isRequired,
    onChange: React.PropTypes.func.isRequired,
    disabled: React.PropTypes.bool
  }

  static defaultProps = {
    disabled: false
  }

  handleChange = (e) => {
    this.props.onChange(e.target.value);
  }

  prettify(str) {
    if (str === '') return;
    if (!str) return;

    // camel case to title case
    // this regex: /\S(?=[A-Z0-9])/
    // matches non-whitespace chars followed by a capital letter or a number
    // which is where a space should be inserted
    // e.g. CorrectHorseBatteryStapler => Correct Horse Battery Stapler
    var pos = str.search(/\S(?=[A-Z0-9])/);

    while (pos !== -1) {
      //insert space after pos
      str = str.slice(0, pos + 1) + ' ' + str.slice(pos + 1);

      pos = str.search(/\S(?=[A-Z0-9])/);
    }

    // capitalize
    str = str.charAt(0).toUpperCase() + str.slice(1);

    return str;
  }

  render() {
    return (
      <div className={styles.root}>
        <TextField value={this.props.value}
          onChange={this.handleChange}
          floatingLabelText={this.prettify(this.props.field)}
          disabled={this.props.disabled} />
      </div>
    );
  }
}
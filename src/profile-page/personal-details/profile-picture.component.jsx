'use strict';

import React from 'react';
import Loading from './loading.component.jsx';
import styles from './profile-picture.scss';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import Dropzone from 'react-dropzone';
import defaultProfile from '../../assets/kc-user-profile-photo.png';

export default class ProfilePicture extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isDialogOpen: false,
      isAlertOpen: false,
      filename: '',
      uploadStatus: 'loaded' // one of 'loaded', 'loading', 'failed'
    };
  }

  static propTypes = {
    profilePictureSrc: React.PropTypes.string,
    readOnly: React.PropTypes.bool,
    disabled: React.PropTypes.bool,
    uploadProfilePic: React.PropTypes.func.isRequired,
    onChange: React.PropTypes.func
  };

  static defaultProps = {
    readOnly: false,
    disabled: false
  };

  onDrop = (acceptedFiles, rejectedFiles) => {
    if (acceptedFiles.length >= 0) {
      let blob = acceptedFiles[0]; //only 1 file can be accepted by dropzone
      this.setState({
        uploadStatus: 'loading'
      });
      this.props.uploadProfilePic(blob, (err, filename) => {
        if (err) {
          this.setState({
            uploadStatus: 'failed'
          });
        } else {
          this.setState({
            isDialogOpen: false,
            uploadStatus: 'loaded',
            filename: filename
          });

          this.props.onChange('pic', filename);
        }
      });
    }
  };

  onDropRejected = () => {
    this.handleOpenAlert();
  };

  onDropzoneOpen = () => {
    this.dropzone.open();
  };

  handleOpenDialog = () => {
    this.setState({
      isDialogOpen: true,
      uploadStatus: 'loaded'
    });
  };

  handleCloseDialog = () => {
    this.setState({
      isDialogOpen: false
    });
  };

  handleOpenAlert = () => {
    this.setState({
      isAlertOpen: true
    });
  };

  handleCloseAlert = () => {
    this.setState({
      isAlertOpen: false
    });
  };

  getUploadButton = () => {
    return (this.props.readOnly)
      ? ''
      : (
        <RaisedButton
          onClick={this.handleOpenDialog}
          label='Upload Picture'
          primary={true}
          disabled={this.props.disabled} />
      );
  };

  getUploadDialog = () => {
    return (
      <Dialog
        title='Upload Profile Picture'
        modal={this.state.uploadStatus === 'loading'}
        open={this.state.isDialogOpen}
        onRequestClose={this.handleCloseDialog}>
        {this.getUploadDialogChild()}
      </Dialog>
    );
  };

  getUploadDialogChild = () => {
    return (this.state.uploadStatus === 'loaded')
      ? (
        <Dropzone
          className={styles.dropzone}
          ref={(node) => { this.dropzone = node; } }
          multiple={false}
          accept='image/*'
          onDrop={this.onDrop}
          onDropRejected={this.onDropRejected}
          onTouchTap={this.onDropzoneOpen}>
          <span className={styles.dragAndDropText}>
            Drag and drop an image here.
          </span>
          <span className={styles.clickText}>
            Alternatively, click this area to browse your PC.
          </span>
          {this.getWrongFileTypeAlert()}
        </Dropzone>
      )
      : <Loading uploadStatus={this.state.uploadStatus} />;
  };

  getWrongFileTypeAlert = () => {
    let actions = [
      <FlatButton
        label='OK'
        onTouchTap={this.handleCloseAlert} />
      ];
    return (
      <Dialog
        title="Error"
        actions={actions}
        modal={true}
        open={this.state.isAlertOpen}
        onRequestClose={this.handleCloseAlert}>
        Please upload an image-type file.
      </Dialog>
    );
  };

  getProfilePictureSrc = () => {
    // check if filename is non-whitespace
    return (/^\s*$/.test(this.state.filename))
      ? this.props.profilePictureSrc
      : this.state.filename;
  };

  render() {
  	let _profSrc = this.getProfilePictureSrc();
		_profSrc = _profSrc
			? `/cdn/${_profSrc}`
			: defaultProfile;

    return (
      <Paper className={styles.root}>
        <img
          className={styles.profilePicture}
          src={_profSrc} />
        {this.getUploadButton()}
        {this.getUploadDialog()}
      </Paper>
    );
  }
}

'use strict';

import React from 'react';
import Fields from './fields.component.jsx';
import Addresses from './addresses.component.jsx';
import ProfilePicture from './profile-picture.component.jsx';
import styles from './personal-details.scss';

export default class PersonalDetails extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    saveStatus: React.PropTypes.oneOf(['loaded', 'loading', 'failed']),
    account: React.PropTypes.object.isRequired,
    readOnly: React.PropTypes.bool,
    onChange: React.PropTypes.func.isRequired,
    uploadProfilePic: React.PropTypes.func.isRequired
  };

  static defaultProps = {
    saveStatus: 'loaded',
    readOnly: false
  };

  getFields = () => {
    return (
      <Fields
        account={this.props.account}
        onChange={this.props.onChange}
        disabled={this.getIsDisabled()} />
    );
  };

  getAddresses = () => {
    return (
      <Addresses
        account={this.props.account}
        onChange={this.props.onChange}
        countries={this.props.countries}
        regions={this.props.regions}
        provinces={this.props.provinces}
        municipalities={this.props.municipalities}
				barangays={this.props.barangays} // Missed this
        selectCountry={this.props.selectCountry}
        selectRegion={this.props.selectRegion}
        selectProvince={this.props.selectProvince}
        selectMunicipality={this.props.selectMunicipality}
        disabled={this.getIsDisabled()} />
    );
  };

  getIsDisabled = () => {
    return (this.props.saveStatus === 'loading')
      || (this.props.readOnly);
  };

  render = () => {
    return (
      <div className={styles.root}>
        <div className={styles.profilePicture}>
          <ProfilePicture
            profilePictureSrc={this.props.account.pic}
            readOnly={this.props.readOnly}
            disabled={this.getIsDisabled()}
            uploadProfilePic={this.props.uploadProfilePic}
            onChange={this.props.onChange} />
        </div>
        <div className={styles.fields}>
          {this.getFields()}
          {this.getAddresses()}
        </div>
      </div>
    );
  }
}

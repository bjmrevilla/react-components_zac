'use strict';

import React from 'react';
import Loading from './loading.component.jsx';
import styles from './standard-controls.scss';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';

export default class StandardControls extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isDialogOpen: false,
      isAlertOpen: false,
      oldPassword: '',
      newPassword: '',
      confirmPassword: '',
      savePasswordStatus: 'loaded' // one of 'loaded', 'loading', 'failed'
    };
  }

  static propTypes = {
    saveStatus: React.PropTypes.oneOf(['loaded', 'loading', 'failed']),
    save: React.PropTypes.func.isRequired,
    changePassword: React.PropTypes.func.isRequired,
    cancel: React.PropTypes.func.isRequired
  };

  static defaultProps = {
    saveStatus: 'loaded'
  };

  handleChangePassword = () => {
    if (/^\s*$/.test(this.state.newPassword)) return;

    if (this.state.newPassword === this.state.confirmPassword) {
      let options = {
        newPass: this.state.newPassword,
        oldPass: this.state.oldPassword
      };

      this.setState({
        savePasswordStatus: 'loading',
        confirmPasswordErrorText: ''
      });
      this.props.changePassword(options, (err) => {
        if (err) {
          this.setState({
            savePasswordStatus: 'failed'
          });
        } else {
          this.setState({
            savePasswordStatus: 'loaded'
          });
          this.handleCloseDialog();
        }
      });
    }
  };

  handleOpenDialog = () => {
    this.setState({
      isDialogOpen: true
    });
  };

  handleCloseDialog = () => {
    this.setState({
      isDialogOpen: false,
      newPassword: '',
      confirmPassword: '',
      savePasswordStatus: 'loaded'
    });
  };

  onOldPasswordChange = (e) => {
    this.setState({
      oldPassword: e.target.value
    });
  }

  onNewPasswordChange = (e) => {
    this.setState({
      newPassword: e.target.value
    });
  };

  onConfirmPasswordChange = (e) => {
    this.setState({
      confirmPassword: e.target.value
    });
  };

  isDisabled = () => {
    return (this.props.saveStatus === 'loading');
  };

  getChangePasswordDialog = () => {
    let isSaving = (this.state.savePasswordStatus === 'loading');
    let hasFailed = (this.state.savePasswordStatus === 'failed');
    let actions = [
      <FlatButton
        label='Cancel'
        disabled={isSaving}
        onTouchTap={this.handleCloseDialog} />,
      <FlatButton
        label='OK'
        disabled={isSaving || hasFailed}
        onTouchTap={this.handleChangePassword} />
    ];
    return (
      <Dialog
        className={styles.changePasswordDialog}
        open={this.state.isDialogOpen}
        title='Change Password'
        modal={isSaving}
        actions={actions}
        autoScrollBodyContent={true}
        onRequestClose={this.handleCloseDialog}>
        {this.getChangePasswordDialogChild()}
      </Dialog>
    );
  };

  getChangePasswordDialogChild = () => {
    let errorText = (this.state.newPassword === this.state.confirmPassword)
      ? ''
      : 'Passwords do not match';
    return (this.state.savePasswordStatus === 'loaded')
      ? (
        <div className={styles.fields}>
          <TextField
            type='password'
            fullWidth={true}
            value={this.state.oldPassword}
            floatingLabelText='Old Password'
            onChange={this.onOldPasswordChange} />
          <TextField
            type='password'
            fullWidth={true}
            value={this.state.newPassword}
            floatingLabelText='New Password'
            onChange={this.onNewPasswordChange} />
          <TextField
            type='password'
            fullWidth={true}
            value={this.state.confirmPassword}
            errorText={errorText}
            floatingLabelText='Confirm Password'
            onChange={this.onConfirmPasswordChange} />
        </div>
      )
      : <Loading saveStatus={this.state.savePasswordStatus} />;
  };

  getChangePasswordButton = () => {
    return (this.props.canChangePassword)
      ? <RaisedButton className={styles.changePasswordButton}
        label='Change Password'
        backgroundColor='#4CAF50'
        labelColor='white'
        onClick={this.handleOpenDialog}
        disabled={this.isDisabled()} />
      : '';
  };

  getButtons = () => {
    return (this.props.disabled)
      ? ''
      : <div>
			<FlatButton className={styles.cancelButton}
									label='Cancel'
									labelStyle={{ color: '#D50000' }}
									onClick={this.props.cancel}
									disabled={this.isDisabled()} />
			<RaisedButton className={styles.saveButton}
										label='Save Changes'
										primary={true}
										onClick={this.props.save}
										disabled={this.props.disabled} />
		</div>;
  }

  render() {
    return (
      <div className={styles.root}>
        {this.getChangePasswordButton()}
        <Loading saveStatus={this.props.saveStatus} />
        {this.getButtons()}
        {this.getChangePasswordDialog()}
      </div>
    );
  }
}

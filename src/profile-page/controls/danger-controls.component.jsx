'use strict';

import React from 'react';
import Loading from './loading.component.jsx';
import styles from './danger-controls.scss';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog';

export default class DangerControls extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isAlertOpen: false,
      deleteStatus: 'loaded' // one of 'loaded', 'loading', 'failed'
    };
  }

  static propTypes = {
    saveStatus: React.PropTypes.oneOf(['loaded', 'loading', 'failed']),
    delete: React.PropTypes.func.isRequired,
    id: React.PropTypes.string.isRequired
  }

  static defaultProps = {
    saveStatus: 'loaded'
  }

  handleDelete = () => {
    this.setState({
      deleteStatus: 'loading'
    });
    this.props.delete(this.props.id, (err) => {
      if (err) {
        this.setState({
          deleteStatus: 'failed'
        });
      } else {
        this.setState({
          deleteStatus: 'loaded'
        });

        this.handleCloseAlert();
      }
    });
  }

  handleOpenAlert = () => {
    this.setState({
      isAlertOpen: true
    });
  }

  handleCloseAlert = () => {
    this.setState({
      isAlertOpen: false,
      deleteStatus: 'loaded'
    });
  }

  isDisabled = () => {
    return (this.props.saveStatus === 'loading');
  }

  getAlert = () => {
    let isDeleting = (this.state.deleteStatus === 'loading');
    let hasFailed = (this.state.deleteStatus === 'failed');
    let actions = [
      <FlatButton
        label='Cancel'
        disabled={isDeleting}
        onTouchTap={this.handleCloseAlert} />,
      <FlatButton
        label='OK'
        disabled={isDeleting || hasFailed}
        onTouchTap={this.handleDelete} />
    ];
    return (
      <Dialog
        open={this.state.isAlertOpen}
        title='Delete Account'
        modal={isDeleting}
        actions={actions}
        onRequestClose={this.handleCloseAlert}>
        {this.getAlertChild()}
      </Dialog>
    );
  }

  getAlertChild = () => {
    return (this.state.deleteStatus === 'loaded')
      ? (
        <div className={styles.confirmText}>
          Do you really want to delete your account?
          This action is <span className={styles.emphasize}>permanent.</span>
        </div>
      )
      : <Loading saveStatus={this.state.deleteStatus} />;
  }

  render() {
    return (
      <div className={styles.root}>
        <div className={styles.warning}>
          <h4 className={styles.warningHeader}>
            Delete Account - Danger!
          </h4>
          <span className={styles.warningText}>
            Careful! Deleting your account cannot be undone. Please proceed
            with caution.
          </span>
        </div>
        <RaisedButton className={styles.deleteButton}
          label='Delete'
          backgroundColor='red'
          labelColor='white'
          onClick={this.handleOpenAlert}
          disabled={this.isDisabled()} />
        {this.getAlert()}
      </div>
    );
  }
}
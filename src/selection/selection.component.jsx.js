/**
 * Created by metsys on 11/13/16.
 */
'use strict';
const log = require('debug')('Selection::Component');

import _ from 'lodash';
import React, {PropTypes} from 'react';
import styles from './selection.scss';
import RaisedButton from 'material-ui/RaisedButton';
import Checkbox from 'material-ui/Checkbox';
import TextField from 'material-ui/TextField';

export default class Selection extends React.Component {
	constructor(props) {
		super(props);
		this.state = {searchString: '', tagToAdd: ''}
	}

	static propTypes = {
		// Labels
		label: PropTypes.string.isRequired,
		title: PropTypes.string,
		addInstruction: PropTypes.string,

		disableSearch: PropTypes.bool,
		selectedItems: PropTypes.arrayOf(PropTypes.any),
		onSelectedItemsChanged: PropTypes.func,
		keyToUse: PropTypes.string,

		// Choices
		sourceItems: PropTypes.arrayOf(PropTypes.any),
		disableAddNew: PropTypes.bool,

		// Add new
		addFunction: PropTypes.func,

		// Search
		search: PropTypes.func,
		onFinish: PropTypes.func
	};

	static defaultProps = {
		// Labels
		title: 'Tags',
		label: 'Tag',
		addInstruction: 'Add tags by checking them',

		disableSearch: false,
		selectedItems: [],
		onSelectedItemsChanged: () => {},
		keyToUse: 'name',

		// Choices
		sourceItems: [],
		disableAddNew: false,
		onFinish: () => {},

		compareUsingKeys: []
	};

	deleteTag = (tag) => {
		log(`#deleteTag ${JSON.stringify(tag)}`);
		const {tags, onTagsChanged, compareUsingKeys} = this.props;

		function isSameQ(a) {
			return (b) => _.isEmpty(compareUsingKeys)
				? _.isEqual(a, b)
				: compareUsingKeys.find(k => a[k] === b[k]);
		}

		onTagsChanged(tags.filter(t => !isSameQ(t, tag)))
	};

	addTag = (tag) => {
		log(`#addTag ${JSON.stringify(tag)}`);
		const {tags, onTagsChanged} = this.props;
		onTagsChanged(_.unionWith(tags, [tag], isSameQ))
	};

	getTags = () => {
		const {selectedItems, sourceItems, keyToUse} = this.props;

		if (_.isEmpty(selectedItems) && _.isEmpty(sourceItems)) {
			return <h3> No data. </h3>;
		}

		const onCheck = (tag, v) => {
			if (v) this.addTag(tag);
			else this.deleteTag(tag);
		};

		return (
			<div>
				{
					selectedItems.map((tag, ind) => {

						const label = _.isPlainObject(tag)
							? tag[keyToUse]
							: tag;

						return (
							<Checkbox
								key={ ind }
								label={ label }
								checked={ true }
								onCheck={ (_, v) => onCheck(tag, v) }
							/>
						)
					})
				}

				{
					sourceItems.filter(tag => !selectedItems.find(t => _.isEqual(t, tag)))
						.map((tag, ind) => {

						const label = _.isPlainObject(tag)
							? tag[keyToUse]
							: tag;

						return (
							<Checkbox
								key={ ind }
								label={ label }
								checked={ false }
								onCheck={ (_, v) => onCheck(tag, v) }
							/>
						)
					})
				}
			</div>
		)
	};

	getAddControl = () => {
		const {tagToAdd} = this.state;
		const {addFunction, label, selectedItems, sourceItems} = this.props;

		if (!_.isFunction(addFunction)) return;

		const items = selectedItems.concat(sourceItems);

		return (
			<div className={styles.footer}>
				<TextField
					className={styles.addText}
					value={tagToAdd}
					onChange={ e => this.setState({tagToAdd: e.target.value}) }
					hintText={`Add a ${label}`}
					fullWidth
				/>
				<RaisedButton
					label='Add'
					primary
					disabled={!tagToAdd}
					onClick={ () => {
						const toAdd = addFunction(tagToAdd);

						// Check if exist
						if (!items.find(i => _.isEqual(i, toAdd))) {
							this.addTag(toAdd);
							this.setState({ tagToAdd: '' });
							return;
						}

						// Exists
						// TODO: improve alert
						alert('Already exists!');

						this.setState({ tagToAdd: '' });
					}}
				/>
			</div>
		)
	};

	onSearch = (searchString) => {
		this.setState({searchString});
		const {search} = this.props;
		if (_.isFunction(search))
			search(searchString);
	};

	render() {
		const {searchString} = this.state;
		const {addInstruction, disableSearch} = this.props;
		return (
			<div className={styles.root}>
				{
					!disableSearch &&
					<TextField
						onChange={ e => this.onSearch(e.target.value) }
						hintText='Search'
						value={searchString}
						fullWidth
					/>
				}
				<h4 className={styles.addInstruction}>{ addInstruction }</h4>
				<div className={styles.tags}>
					{ this.getTags() }
				</div>

				{
					this.getAddControl()
				}

			</div>
		)
	}
}

'use strict';

import React from 'react';
import styles from './date.scss';

export default class Date extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    date: React.PropTypes.string
  }

  render() {
    return (
      <div className={styles.root}>
        {this.props.date}
      </div>
    );
  }
}

'use strict';

import React from 'react';
import styles from './send.scss';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

export default class Send extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    message: React.PropTypes.string.isRequired,
    onType: React.PropTypes.func.isRequired,
    onSend: React.PropTypes.func.isRequired
  }

  handleSend = (e) => {
    e.preventDefault();

    this.props.onSend();
  }

  render() {
    return (
      <form className={styles.root} onSubmit={this.handleSend}>
        <span className={styles.textbox}>
          <TextField type='text' value={this.props.message}
            onChange={this.props.onType} hintText='Type a message...'
            onKeyDown={this.props.onKeyDown}
            multiLine={true} fullWidth={true} />
        </span>
        <RaisedButton
          className={styles.send}
          type='submit'
          label='Send'
          primary={true} />
      </form>
    );
  }
}

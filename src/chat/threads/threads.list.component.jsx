'use strict';

import React from 'react';
import styles from './threads.list.scss';
import {List, ListItem} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import defaultImage from '../../assets/kc-user-profile-photo.png';

export default class ThreadsList extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    items: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
    type: React.PropTypes.oneOf(['room', 'account']).isRequired,
    onCreateRoom: React.PropTypes.func.isRequired
  };

  getPrimaryText = (item) => {
    if (this.props.type === 'room') {
      return item.title;
    }
    if (this.props.type === 'account') {
      return item.username;
    }

    return '';
  };

  getSecondaryText = (item) => {
    if (this.props.type === 'room') {
      return item.tags;
    }
    if (this.props.type === 'account') {
      return item.email;
    }

    return '';
  };

  getItemPictureSrc = (item) => {
    if (this.props.type === 'room') {
      let path = require('./assets/ic_group_black_36dp.png');
      return path;
    }
    if (this.props.type === 'account') {
    	return item.pic
				? `/cdn/${item.pic}`
				: defaultImage;
    }

    return '';
  };

  getItemPicture = (item) => {
    return <Avatar src={this.getItemPictureSrc(item) } />;
  };

  getOnClick = (item, index) => {
    if (this.props.type === 'room') {
      return () => {
        this.props.handleTouchTapRoom(index);
      };
    }
    if (this.props.type === 'account') {
      return () => {
        this.props.onCreateRoom(item);
      };
    }

    return '';
  };

  getListItems = () => {
    if (this.props.items === null
      || this.props.items === undefined
      || this.props.items.length === 0)
      return 'No items';

    return this.props.items.map((item, index) => {
      return (
        <ListItem
          key={item._id}
          primaryText={this.getPrimaryText(item) }
          secondaryText={this.getSecondaryText(item) }
          leftAvatar={this.getItemPicture(item) }
          onClick={this.getOnClick(item, index) } />
      );
    });
  };

  render() {
    return (
      <div className={styles.root}>
        <List>
          {this.getListItems() }
        </List>
      </div>
    );
  }
}

'use strict';

import React from 'react';
import Thread from './thread/thread.component.jsx';
import Threads from './threads/threads.component.jsx';
import styles from './chat.scss';

export default class Chat extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    rooms: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
    accounts: React.PropTypes.arrayOf(React.PropTypes.object),
    currentRoom: React.PropTypes.object,
    currentAccount: React.PropTypes.object.isRequired,
    search: React.PropTypes.func.isRequired,
    createRoom: React.PropTypes.func.isRequired,
    switchRoom: React.PropTypes.func,
    sendMessage: React.PropTypes.func.isRequired,
    updateRoomDetails: React.PropTypes.func.isRequired
  };

  handleTouchTapRoom = (index) => {
  	const { currentRoom, rooms, switchRoom } = this.props;
    let config = {
      from: currentRoom && currentRoom._id,
      to: rooms[index]._id
    };
    switchRoom(config, (err) => {
      // handle error?
    });
  };

  getThread = () => {
    return (this.props.currentRoom !== undefined
      && this.props.currentRoom !== null)
      ? <Thread
        room={this.props.currentRoom}
        currentAccount={this.props.currentAccount}
        sendMessage={this.props.sendMessage}
        updateRoomDetails={this.props.updateRoomDetails} />
      : (
        <div className={styles.noThread}>
          <span className={styles.noThreadMessage}>
            See your conversations here.
          </span>
        </div>
      );
  };

  render = () => {
    return (
      <div className={styles.root}>
        <div className={styles.threads}>
          <Threads
            rooms={this.props.rooms}
            accounts={this.props.accounts}
            currentAccount={this.props.currentAccount}
            search={this.props.search}
            createRoom={this.props.createRoom}
            handleTouchTapRoom={this.handleTouchTapRoom} />
        </div>
        <hr className={styles.separator} />
        <div className={styles.thread}>
          {this.getThread()}
        </div>
      </div>
    );
  }
}

import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Chat from '..';
import {
  accounts,
  currentAccount,
  longMessage,
  messages,
  room1,
  rooms,
  search,
  createRoom,
  sendMessage,
  updateRoomDetails
} from './fixtures'

storiesOf('Chat', module)
  .add('component', () => (
    <MuiThemeProvider>
      <Chat
        currentAccount={currentAccount}
        currentRoom={rooms[1]}
        rooms={rooms}
        accounts={accounts}
        search={search}
        createRoom={createRoom}
        sendMessage={sendMessage}
        updateRoomDetails={updateRoomDetails}
      />
    </MuiThemeProvider>
  ))
storiesOf('Chat', module)
  .add('no data', () => (
    <MuiThemeProvider>
      <Chat
        currentAccount={currentAccount}
        search={search}
        createRoom={createRoom}
        sendMessage={sendMessage}
        updateRoomDetails={updateRoomDetails}
      />
    </MuiThemeProvider>
  ))

'use strict';

import React from 'react';
import styles from './post.scss';
import {Card,
  CardActions,
  CardHeader,
  CardMedia,
  CardTitle,
  CardText} from 'material-ui/Card';
import ThumbUp from 'material-ui/svg-icons/action/thumb-up';
import IconButton from 'material-ui/IconButton';
import FlatButton from 'material-ui/FlatButton';

export default class Post extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    post: React.PropTypes.object,
    canLike: React.PropTypes.bool,
    like: React.PropTypes.func,
    canComment: React.PropTypes.bool,
    loadComments: React.PropTypes.func,
    readOnly: React.PropTypes.bool
  }

  static defaultProps = {
    canLike: false,
    canComment: false,
    readOnly: true
  }

  render = () => {
    const { post, onLike, readOnly } = this.props
    const {
      title,
      body,
      creator: { username, img, email, createdAt },
    } = post

    let { canComment, canLike } = this.props
    if (readOnly) {
      canLike = canLike || false
      canComment = canComment || false
    }

    return (
      <Card>
        <CardHeader
          title={username}
          subtitle={email}
          avatar={img || '/avatar.jpg'}
          >
          <div className={styles.createdAt}>{createdAt}</div>
        </CardHeader>
        <CardText>
          {body}
        </CardText>
        <CardActions>
          {canLike
            ? <IconButton onClick={onLike}>
              <ThumbUp />
            </IconButton>
            : ''
          }
          {canComment
            ? <FlatButton label='Show Comments' className={styles.showComments} />
            : ''
          }
        </CardActions>
      </Card>
    )
  }
}
'use strict';
 
import React from 'react';
import ListViewPage from '../../list-view-page/list-view-page.component.jsx';
import Selection from '../../list-view-page/selection/selection.component.jsx';
import styles from './curriculum-tab.scss';
import Dialog from 'material-ui/Dialog';
 
export default class CurriculumTab extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isDialogOpen: false
    };
  }
 
  static propTypes = {
    items: React.PropTypes.arrayOf(React.PropTypes.object),
    edit: React.PropTypes.bool,
    onChange: React.PropTypes.func,
    readOnly: React.PropTypes.bool,
    title: React.PropTypes.string,
    headers: React.PropTypes.arrayOf(React.PropTypes.object)
  }

  static defaultProps = {
    edit: false,
    readOnly: false
  }

  handleOpenDialog = () => {
    this.setState({isDialogOpen: true});
  }

  handleCloseDialog = () => {
    this.setState({isDialogOpen: false});
  }
 
  render = () => {
    const customBodyStyle = {
      padding: '0'
    };
    const customContentStyle = {
      width: '300px'
    };

    return (
      <div className={styles.root}>
        <ListViewPage
          title={this.props.title}
          headers={this.props.headers}
          data={this.props.items}
          showFloatingAddNew={false}
          onAddNewTouchTap={this.handleOpenDialog} />
        <Dialog
          contentStyle={customContentStyle}
          bodyStyle={customBodyStyle}
          open={this.state.isDialogOpen}
          onRequestClose={this.handleCloseDialog}
          modal={false}>
          <Selection
            title={this.props.title}
            sourceItems={this.props.items}
            disableAddNew={true} />
        </Dialog>
      </div>
    );
  }
}

'use strict';

import React from 'react';
import CurriculumDetails
  from './curriculum-details/curriculum-details.component.jsx';
import PostsList from './posts-list/posts-list.component.jsx';
import CurriculumTab from './curriculum-tab/curriculum-tab.component.jsx';
import {utils} from '../list-view-page/utils/utils.js';
import styles from './curriculum-view.scss';
import {Tabs, Tab} from 'material-ui/Tabs';

export default class CurriculumView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      enrolStatus: 'loaded',
      saveStatus: 'loaded',
      tempCurriculum: {...props.curriculum}
    };
  }

  static propTypes = {
    curriculum: React.PropTypes.object,
    readOnly: React.PropTypes.bool,
    edit: React.PropTypes.bool,
    save: React.PropTypes.func,
    isEnroled: React.PropTypes.bool
  }

  static defaultProps = {
    readOnly: false,
    edit: false,
    isEnroled: false,
    curriculum: {}
  }

  handleSave = () => {
    let changes = diff(this.props.curriculum, this.state.tempCurriculum);

    this.props.save(changes, err => {});
  }; 

  handleChange = (field, value) => {
    let newCurriculum = this.state.tempCurriculum;

    newCurriculum[field] = value;

    this.setState({
      tempCurriculum: newCurriculum
    });
  }

  curriedChange = field => value => handleChange(field, value);

  handleEnrol = () => {
    // loading
    this.props.enrol(this.props.curriculum, (err) => {
      if (err) {
        // failed
      } else {
        // loaded
      }
    });
  }

  getHeaders = () => {
    // TODO: make proper headers
  }

  render = () => {
    return (
      <div className={styles.root}>
        <div className={styles.details}>
          <CurriculumDetails
            curriculum={this.state.tempCurriculum}
            canEnrol={this.props.canEnrol}
            edit={this.props.edit}
            uploadCoverPic={this.props.uploadCoverPic}
            isEnroled={this.props.isEnroled}
            readOnly={this.props.readOnly}
            onChange={this.handleChange}
            onChangeTitle={this.curriedChange('title')}
            onChangeDescription={this.curriedChange('description')}
            canAddRemoveQuestions={this.props.canAddRemoveQuestions}
            save={this.handleSave}
            enrol={this.handleEnrol} />
        </div>
        <Tabs>
          <Tab
            label='Materials'>
            <div className={styles.child}>
              <CurriculumTab
                title='Materials'
                headers={[{label: 'Materials', key: 'title', format: (x)=>x}]}
                items={this.state.tempCurriculum.materials}
                edit={this.props.edit}
                getActions={this.props.getMaterialActions}
                onChange={this.handleChange}
                readOnly={this.props.readOnly} />
            </div>
          </Tab>
          <Tab
            label='Students'>
            <div className={styles.child}>
              <CurriculumTab
                title='Students'
              headers={[{label: 'Students', key: 'name', format: (name)=>`${name.firstName} ${name.lastName}`}]}
                items={this.state.tempCurriculum.students}
                edit={this.props.edit}
                getActions={this.props.getStudentActions}
                onChange={this.handleChange}
                readOnly={this.props.readOnly} />
            </div>
          </Tab>
          <Tab
            label='Admins'
            className={styles.child}>
            <CurriculumTab
              title='Admins'
              headers={[{label: 'Admins', key: 'name', format: (name)=>`${name.firstName} ${name.lastName}`}]}
              items={this.state.tempCurriculum.admins}
              edit={this.props.edit}
              getActions={this.props.getAdminActions}
              onChange={this.handleChange}
              readOnly={this.props.readOnly} />
          </Tab>
        </Tabs>
      </div>
    );
  }
}

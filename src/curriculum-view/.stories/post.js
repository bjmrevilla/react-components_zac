import React from 'react';
import {storiesOf, action} from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Post from '../post/post.component.jsx';
import {postProps} from './fixtures.js';
 
storiesOf('curriculum-view', module)
  .add('post', () => (
    <MuiThemeProvider>
      <Post {...postProps} />
    </MuiThemeProvider>
  ));
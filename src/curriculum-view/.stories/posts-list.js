import React from 'react';
import {storiesOf, action} from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import PostsList from '../posts-list/posts-list.component.jsx';
import PostEditor from '../posts-list/post-editor.component.jsx';
import {posts} from './fixtures.js';
 
storiesOf('curriculum-view', module)
  .add('post-editor', () => (
    <MuiThemeProvider>
      <PostEditor />
    </MuiThemeProvider>
  ));

storiesOf('curriculum-view', module)
  .add('posts-list', () => (
    <MuiThemeProvider>
      <PostsList posts={posts} />
    </MuiThemeProvider>
  ));
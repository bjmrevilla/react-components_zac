export const postProps = {
  post: {
    creator: {
      username: 'Secretmapper',
      email: 'Secretmapper16@gmail.com',
      createdAt: 'Janualy 16, 2016'
    },
    title: 'Post title',
    body: `Lorem ipsum dlor sit amet, consectetur adipiscing elit.
    Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
    Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.
    Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.`
  }
};

export const posts = [
  postProps.post,
  postProps.post,
  postProps.post
];

export const curriculum = {
  title: 'title',
  description: 'description',
  posts: posts,
  materials: [{title: 'material 1'}],
  students: [{name: {firstName: 'Steven', lastName: 'Student'}}],
  admins: [{name: {firstName: 'Bob', lastName: 'the Builder'}}]
};

export const testUploadCoverPic = (blob, cb) => {
  setTimeout(() => {
    console.log('blob:');
    console.log(blob);

    // good
    // cb(null, 'https://pixabay.com/static/uploads/photo/2015/03/04/22/35/head-659652_960_720.png');

    // bad
    cb(new Error());
  }, 2000);
};

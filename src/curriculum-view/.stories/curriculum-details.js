import React from 'react';
import {storiesOf, action} from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import CurriculumDetails
  from '../curriculum-details/curriculum-details.component.jsx';
import {curriculum} from './fixtures.js';
 
storiesOf('curriculum-view', module)
  .add('curriculum-details', () => (
    <MuiThemeProvider>
      <CurriculumDetails curriculum={curriculum} />
    </MuiThemeProvider>
  ));
import React from 'react';
import {storiesOf, action} from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import CurriculumView from '..';
import {
  curriculum,
  testUploadCoverPic
} from './fixtures.js';

require('./curriculum-details.js');
require('./post.js');
require('./posts-list.js');

storiesOf('curriculum-view', module)
  .add('component', () => (
    <MuiThemeProvider>
      <CurriculumView
        curriculum={curriculum}
        uploadCoverPic={testUploadCoverPic} />
    </MuiThemeProvider>
  ));

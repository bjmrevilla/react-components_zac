'use strict';
 
import React from 'react';
import Post from '../post/post.component.jsx';
import PostEditor from './post-editor.component.jsx';
import styles from './posts-list.scss';
import RaisedButton from 'material-ui/RaisedButton';
 
export default class PostsList extends React.Component {
  constructor(props) {
    super(props);
  }
 
  static propTypes = {
    posts: React.PropTypes.arrayOf(React.PropTypes.object),
    readOnly: React.PropTypes.bool,
    canPost: React.PropTypes.bool,
    post: React.PropTypes.func,
    postCreatorPosition: React.PropTypes.oneOf(['top', 'bottom']),
    loadMore: React.PropTypes.func
  }

  static defaultProps = {
    readOnly: true,
    canPost: true,
    postCreatorPosition: 'top'
  }

  handleLoadMore = () => {
    //loading
    let options = {};
    this.props.loadMore(options, (err) => {
      if (err) {
        //failed
      } else {
        //loaded
      }
    });
  }

  getPosts = () => {
    if (!this.props.posts) return;

    return this.props.posts.map((post) => {
      return (
        <div className={styles.post}>
          <Post post={post} />
        </div>
      );
    });
  }

  render = () => {
    return (
      <div className={styles.root}>
        {
          (this.props.postCreatorPosition === 'top' && !this.props.readOnly)
            ? (<div className={styles.editor}><PostEditor /></div>)
            : '' 
        }
        <div className={styles.posts}>
          {this.getPosts()}
        </div>
        <RaisedButton
          className={styles.viewMoreButton}
          label='View more'
          onTouchTap={this.props.handleLoadMore}
          primary={true} />
        {
          (this.props.postCreatorPosition === 'bottom' && !this.props.readOnly)
            ? (<div className={styles.editor}><PostEditor /></div>)
            : '' 
        }
      </div>
    );
  }
}

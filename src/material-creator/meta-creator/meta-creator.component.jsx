'use strict';

import React from 'react';
import Loading from './loading.component.jsx';
import styles from './meta-creator.scss';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog';
import Dropzone from 'react-dropzone';


export default class MetaCreator extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isDialogOpen: false,
      isAlertOpen: false,
      filename: '',
      uploadStatus: 'loaded' // one of 'loaded', 'loading', 'failed'
    };
  }

  static propTypes = {
    meta: React.PropTypes.object,
    edit: React.PropTypes.bool,
    onChange: React.PropTypes.func,
    uploadProfilePic: React.PropTypes.func
  };

  static defaultProps = {
    edit: false
  };

  onDrop = (acceptedFiles) => {
    if (acceptedFiles.length >= 0) {
      let blob = acceptedFiles[0]; //only 1 file can be accepted by dropzone
      this.setState({
        uploadStatus: 'loading'
      });
      this.props.uploadCover(blob, (err, filename) => {
        if (err) {
          this.setState({
            uploadStatus: 'failed'
          });
        } else {
          this.setState({
            isDialogOpen: false,
            uploadStatus: 'loaded',
            filename: filename
          });

          this.props.onChange('cover', filename);
        }
      });
    }
  };

  onDropRejected = () => {
    this.handleOpenAlert();
  };

  onDropzoneOpen = () => {
    this.dropzone.open();
  };

  handleOpenDialog = () => {
    this.setState({
      isDialogOpen: true
    });
  };

  handleCloseDialog = () => {
    this.setState({
      isDialogOpen: false,
      uploadStatus: 'loaded'
    });
  };

  handleOpenAlert = () => {
    this.setState({
      isAlertOpen: true
    });
  };

  handleCloseAlert = () => {
    this.setState({
      isAlertOpen: false
    });
  };

  getUploadDialog = () => {
    return (
      <Dialog
        title='Upload Profile Picture'
        modal={this.state.uploadStatus === 'loading'}
        open={this.state.isDialogOpen}
        onRequestClose={this.handleCloseDialog}>
        {this.getUploadDialogChild()}
      </Dialog>
    );
  };

  getUploadDialogChild = () => {
    return (this.state.uploadStatus === 'loaded')
      ? (
        <Dropzone
          className={styles.dropzone}
          ref={(node) => { this.dropzone = node; } }
          multiple={false}
          accept='image/*'
          onDrop={this.onDrop}
          onDropRejected={this.onDropRejected}
          onTouchTap={this.onDropzoneOpen}>
          <span className={styles.dragAndDropText}>
            Drag and drop an image here.
          </span>
          <span className={styles.clickText}>
            Alternatively, click this area to browse your PC.
          </span>
          {this.getWrongFileTypeAlert()}
        </Dropzone>
      )
      : <Loading saveStatus={this.state.uploadStatus} />;
  };

  getWrongFileTypeAlert = () => {
    let actions = [
      <FlatButton
        label='OK'
        onTouchTap={this.handleCloseAlert} />
      ];
    return (
      <Dialog
        title="Error"
        actions={actions}
        modal={true}
        open={this.state.isAlertOpen}
        onRequestClose={this.handleCloseAlert}>
        Please upload an image-type file.
      </Dialog>
    );
  };

  change = (key) => (e) => {
    const { onChange, meta } = this.props;
    onChange({ ...meta,  [key]: e.target.value });
  };

  render = () => {
    const { meta, type = meta.type } = this.props;
    const { filename } = this.state;
    const { change } = this;

		const _picSrc = (filename || meta.cover)
			? `/cdn/${filename || meta.cover}`
			: require(`../assets/kc-addnew-ic-${type}.png`);

    return (
      <div className={styles.root}>
        <div className={styles.cover}>
          <div className={styles.imageHolder}>
            <img  src={_picSrc}  className={styles.image} />
          </div>
          <RaisedButton
            label='Upload Cover'
            onClick={this.handleOpenDialog}
            fullWidth/>
          {this.getUploadDialog()}
        </div>
        <div>
          <TextField
            value={meta.title}
            onChange={change('title') }
            floatingLabelText='Title'
            fullWidth
          />
          <TextField
            value={meta.author}
            onChange={change('author') }
            floatingLabelText='Author'
            fullWidth
          />
          <TextField
            value={meta.description}
            onChange={change('description') }
            floatingLabelText='Description'
            fullWidth
          />
          <TextField
            value={meta.publisher}
            onChange={change('publisher') }
            floatingLabelText='Publisher'
            className={styles.publisher}
          />
          <TextField
            value={meta.publishedAt}
            onChange={change('publishedAt') }
            floatingLabelText='Date published'
            className={styles.inline}
          />
        </div>
      </div>
    );
  }
}

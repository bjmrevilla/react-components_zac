'use strict';

import React from 'react';
import styles from './file-selector.scss';

export default class FileSelector extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    onUrlSelected: React.PropTypes.func,
    onCancelTouchTapped: React.PropTypes.func,
    uploadFile: React.PropTypes.func
  }

  render = () => {
    return (
      <div className={styles.root}>
      </div>
    );
  }
}
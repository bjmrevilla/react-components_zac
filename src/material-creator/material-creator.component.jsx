'use strict';

import _ from 'lodash';
import React from 'react';
import AboutStep from './about-step/about-step.component.jsx';
import FileUploadStep from './file-upload-step/file-upload-step.component.jsx';
import QuestionStep from './question-step/question-step.component.jsx';
import ChapterStep from '../chapters-step';
import {diff} from '../list-view-page/utils/utils.js';
import styles from './material-creator.scss';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import { Stepper,  Step,  StepLabel } from 'material-ui/Stepper';
import Paper from 'material-ui/Paper';

const clone = (material) => {
	return {
		...material,
		$: { }
	}
};

export default class MaterialCreator extends React.Component {
  constructor(props) {
    super(props);

    let material = (props.edit)
      ? {...props.material}
      : {};

    this.state = {
      tempMaterial: clone(material),
      stepIndex: 0
    };
  }

  static propTypes = {
    material: React.PropTypes.object,
    edit: React.PropTypes.bool,
    delete: React.PropTypes.func,
    type: React.PropTypes.string,
    save: React.PropTypes.func
  };

  static defaultProps = {
    edit: false
  };

  handleChange = (field, value, additional) => {
    let tempMaterial = this.state.tempMaterial;

    tempMaterial[field] = value;
		tempMaterial.$ = {
			...tempMaterial.$,
			...additional
		};

    this.setState({ tempMaterial });
  };

  handleCancel = () => {
    this.setState({
      tempMaterial: clone(this.props.material)
    });
  };

  handleSave = () => {
    let changes = diff(this.props.material, this.state.tempMaterial, ['$']);
    this.props.save(changes, (err) => {
      // should show loading and disable buttons
      // while saving
    });
  };

  handleNextStep = () => {
    if (this.state.stepIndex < 2) {
      this.setState({
        stepIndex: this.state.stepIndex + 1
      });
    }
  };

  handleBackStep = () => {
    if (this.state.stepIndex > 0) {
      this.setState({
        stepIndex: this.state.stepIndex - 1
      });
    }
  };

  getControls = (material) => {
  	const { edit, type = material.type } = this.props;
    let mode = (edit)
      ? `Edit: ${_.upperFirst(type)}`
      : `Create: ${_.upperFirst(type)}`;

    return (
      <div className={styles.controls}>
        <div className={styles.header}>
          <span className={styles.mode}>
            {mode}
          </span>
          <span className={styles.title}>
            {material.title}
          </span>
        </div>
        <div className={styles.saveControls}>
          <FlatButton
            label='Cancel'
            onTouchTap={this.handleCancel} />
          <RaisedButton
            label='Save'
            primary={true}
            onTouchTap={this.handleSave} />
        </div>
        <div className={styles.backNextControls}>
          <FlatButton
            label='Back'
            primary={true}
            onTouchTap={this.handleBackStep} />
          <FlatButton
            label='Next'
            primary={true}
            onTouchTap={this.handleNextStep} />
        </div>
      </div>
    );
  };

  getStepper = (material) => {
  	const { type = material.type } = this.props;
    return (
      <Stepper
        activeStep={this.state.stepIndex}
        className={styles.stepper}>
        <Step>
          <StepLabel>
            About {_.upperFirst(type)}
          </StepLabel>
        </Step>
        <Step>
          <StepLabel>
            {_.upperFirst(type)}'s Assessments
          </StepLabel>
        </Step>
        <Step>
          <StepLabel>
						{ type === 'manual' || type === 'guideline'? 'Chapters': 'File' }
          </StepLabel>
        </Step>
      </Stepper>
    );
  };

  getChild = (material) => {
  	const { type = material.type } = this.props;

    if (this.state.stepIndex === 0) {
      return <AboutStep
        // Material
				material={material}
        edit={this.props.edit}
        type={this.props.type}

				uploadCover={this.props.uploadCover}
				onChange={this.handleChange}

        // changeVersion={this.props.changeVersion}

				// Sources
				// Tags
        addTagAction={this.props.addTagAction}
        searchTags={this.props.searchTags}
        sourceTags={this.props.sourceTags}

				// Filters
				addViewFilterAction={this.props.addViewFilterAction}
        searchViewFilters={this.props.searchViewFilters}
        sourceViewFilters={this.props.sourceViewFilters}

				// Filters
				addExemptionFilterAction={this.props.addExemptionFilterAction}
        searchExemptionFilters={this.props.searchExemptionFilters}
        sourceExemptionFilters={this.props.sourceExemptionFilters}

				// Admins
				searchAdmins={this.props.searchAdmins}
        sourceAdmins={this.props.sourceAdmins}
			/>;
    }
    if (this.state.stepIndex === 1) {
      return <QuestionStep
        questionList={material.questionList}
        edit={this.props.edit}
        onChange={this.handleChange} />;
    }
    if (this.state.stepIndex === 2) {
    	console.log(`Type ${type}`);
      return /(manual|guideline)/.test(type)
				? <ChapterStep
						edit={this.props.edit}
						material={material}
						chapters={material.chapters}
						onChange={this.handleChange}

						// Save draft
						saveDraft={this.props.saveChapterDraft}

						// Copy an existing
						forkChapter={this.props.forkChapter}

						// Create a blank draft
						createChapterDraft={this.props.createChapterDraft}
					/>
				:	<FileUploadStep
						url={material.url}
						edit={this.props.edit}
						type={this.props.type}
						uploadFile={this.uploadFile}
						onChange={this.handleChange} />;
    }

    return '';
  };

  render = () => {
    return (
      <div className={styles.root}>
        {this.getControls(this.state.tempMaterial) }
        {this.getStepper(this.state.tempMaterial) }
        <Paper className={styles.child}>
          {this.getChild(this.state.tempMaterial) }
        </Paper>
      </div>
    );
  }
}

'use strict';

import React from 'react';
import QuestionCreator
  from '../question-creator/question-creator.component.jsx';
import styles from './question-list.scss';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import TabularView from '../../list-view-page/tabular-view/tabular-view.component.jsx';

export default class QuestionList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { genType: 'preset', isCreatorOpen: false };
  }

  static propTypes = {
    questionList: React.PropTypes.object,
    edit: React.PropTypes.bool,
    onChange: React.PropTypes.func
  };

  static defaultProps = {
    edit: false
  };

  handleGenTypeChange = (e, index, value) => {
    this.setState({ genType: value });
  };

  openCreator = () => {
    this.setState({ isCreatorOpen: true });
  };

  closeCreator = () => {
    this.setState({ isCreatorOpen: false });
  };

  onAddQuestion = (value) => {
    if (this.props.onAddQuestion) {
      this.props.onAddQuestion(value);
    }
    this.closeCreator();
  };

  render = () => {
    const {type, description} = this.props;
    return (
      <div className={styles.root}>
        <div className={styles.topBar}>
          <div className={styles.meta}>
            <h3>{type} Questions</h3>
            <p>{description}</p>
          </div>
          <SelectField
            style={{ verticalAlign: 'top' }}
            value={ this.state.genType }
            onChange={this.handleGenTypeChange}
            floatingLabelText='Generation Type'
            >
            <MenuItem value='random' primaryText='random' />
            <MenuItem value='preset' primaryText='preset' />
          </SelectField>
          <TextField
            type='number'
            className={styles.numOfQuestions}
            floatingLabelText='no. of questions'
            />
          <Dialog
            title='Question Creator'
            onRequestClose={this.closeCreator}
            open={this.state.isCreatorOpen}
            >
            <QuestionCreator
              question={{}}
              onSave={this.onAddQuestion}
              onCancel={this.closeCreator}
              />
          </Dialog>
          <RaisedButton
            label='Add'
            onClick={this.openCreator}
            className={styles.button}
            primary
            />
        </div>
        <div>
          <TabularView
            headers={[{label: 'Question', key: 'question', format:(x)=>x}]}
            data={this.props.questionList.questions} />
        </div>
      </div>
    );
  }
}

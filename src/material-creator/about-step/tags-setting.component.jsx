'use strict';

import React from 'react';
import TagList from '../../list-view-page/tags-list/tags-list.component.jsx';
import styles from './tags-setting.scss';

export default class TagsSetting extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedItems: this.arrayClone(this.props.setting)
    };
  }

  static propTypes = {
    description: React.PropTypes.string.isRequired,
    setting: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
    onChange: React.PropTypes.func.isRequired,
    disabled: React.PropTypes.bool
  };

  arrayClone = (arr) => {
    return arr.slice(0);
  };

  remove = (arr, item) => {
    let newArr = this.arrayClone(arr);

    newArr.splice(arr.indexOf(item), 1);

    return newArr;
  };

  handleFinish = () => {
    this.props.onChange(this.state.selectedItems);
  };

  onSelectedItemsChanged = (key, isInputChecked) => {
    let item = this.props.source.find(e => e.name === key);

    if (isInputChecked) {
      this.setState({
        selectedItems: [...this.state.selectedItems, item]
      });
    } else {
      this.setState({
        selectedItems: this.remove(this.state.selectedItems, item)
      });
    }
  };

  handleAdd = addTag => {
    if (!this.props.addAction) return null;

    let addedTag = this.props.addAction(addTag);

    this.setState({
      selectedItems: [...this.state.selectedItems, addedTag]
    });
  };

  render = () => {
    const selectionProps = {
      selectedItems: this.state.selectedItems,
      onSelectedItemsChanged: this.onSelectedItemsChanged,
      search: this.props.search,
      addFunction: this.handleAdd
    };

    return (
      <div className={styles.root}>
        <div className={styles.description}>
          {this.props.description}
        </div>
        <TagList
          disableAddNew={!this.props.edit}
          tags={this.props.setting}
          sourceTags={this.props.source}
          onFinish={this.handleFinish}
          selectionProps={selectionProps} />
      </div>
    );
  }
}

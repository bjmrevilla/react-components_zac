import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {
  testAccount,
  menus,
  themes
} from '../../user-landing/.stories/fixtures'
import {
  testAccount as profileAccount,
  testOnChange,
  testSave,
  testDelete,
  testChangePassword,
  testUploadProfilePic,
  testCancel,
  testSourceRoles,
  testSourceViewFilters,
  testSourceExemptionFilters,
  countries,
  regions,
  provinces,
  municipalities,
  selectCountry,
  selectRegion,
  selectProvince,
  selectMunicipality
} from '../../profile-page/.stories/fixtures';
import {
  manual,
  props,
  chapterViewProps
} from '../../previews/.stories/fixtures';
import * as chatF from '../../chat/.stories/fixtures';
import App from '..';
import UserLanding from '../../user-landing';
import ProfilePage from '../../profile-page';
import Chat from '../../chat';
import ManualViewer from '../../previews';

storiesOf('app', module)
  .add('with user landing - profile page', () => (
    <App>
      <UserLanding
        account={testAccount}
        menus={menus}
        themes={themes}
        title='Profile'>
        <ProfilePage account={profileAccount}
          countries={countries}
          regions={regions}
          provinces={provinces}
          municipalities={municipalities}
          selectCountry={selectCountry}
          selectRegion={selectRegion}
          selectProvince={selectProvince}
          selectMunicipality={selectMunicipality}
          onChange={testOnChange}
          save={testSave}
          delete={testDelete}
          changePassword={testChangePassword}
          uploadProfilePic={testUploadProfilePic}
          cancel={testCancel}
          sourceRoles={testSourceRoles}
          sourceViewFilters={testSourceViewFilters}
          sourceExemptionFilters={testSourceExemptionFilters} />
      </UserLanding>
    </App>
  ));
storiesOf('app', module)
  .add('with user landing - profile page read only', () => (
    <App>
      <UserLanding
        account={testAccount}
        menus={menus}
        themes={themes}
        title='Profile'>
        <ProfilePage account={profileAccount}
          countries={countries}
          regions={regions}
          provinces={provinces}
          municipalities={municipalities}
          selectCountry={selectCountry}
          selectRegion={selectRegion}
          selectProvince={selectProvince}
          selectMunicipality={selectMunicipality}
          onChange={testOnChange}
          save={testSave}
          delete={testDelete}
          changePassword={testChangePassword}
          uploadProfilePic={testUploadProfilePic}
          cancel={testCancel}
          sourceRoles={testSourceRoles}
          sourceViewFilters={testSourceViewFilters}
          readOnly={true}
          sourceExemptionFilters={testSourceExemptionFilters} />
      </UserLanding>
    </App>
  ));
storiesOf('app', module)
  .add('with user landing - profile page cant delete', () => (
    <App>
      <UserLanding
        account={testAccount}
        menus={menus}
        themes={themes}
        title='Profile'>
        <ProfilePage account={profileAccount}
          countries={countries}
          regions={regions}
          provinces={provinces}
          municipalities={municipalities}
          selectCountry={selectCountry}
          selectRegion={selectRegion}
          selectProvince={selectProvince}
          selectMunicipality={selectMunicipality}
          onChange={testOnChange}
          save={testSave}
          delete={testDelete}
          canDelete={false}
          changePassword={testChangePassword}
          uploadProfilePic={testUploadProfilePic}
          cancel={testCancel}
          sourceRoles={testSourceRoles}
          sourceViewFilters={testSourceViewFilters}
          sourceExemptionFilters={testSourceExemptionFilters} />
      </UserLanding>
    </App>
  ));
storiesOf('app', module)
  .add('with user landing - chat', () => (
    <App>
      <UserLanding
        account={testAccount}
        menus={menus}
        themes={themes}
        title='Profile'>
        <Chat
          currentAccount={chatF.currentAccount}
          currentRoom={chatF.rooms[1]}
          rooms={chatF.rooms}
          accounts={chatF.accounts}
          search={chatF.search}
          createRoom={chatF.createRoom}
          sendMessage={chatF.sendMessage}
          updateRoomDetails={chatF.updateRoomDetails}
          />
      </UserLanding>
    </App>
  ));
storiesOf('app', module)
  .add('with manual viewer', () => (
    <App>
      <ManualViewer manual={manual} />
    </App>
  ));

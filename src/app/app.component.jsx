'use strict';

import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
import styles from './app.scss';

export default class App extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    children: React.PropTypes.any.isRequired
  }

  componentWillMount = () => {
    try {
      injectTapEventPlugin();
    } catch (err) {
      // in case injectTapEventPlugin was already called (hot reload issue)
    }
  }

  render = () => {
    return (
      <MuiThemeProvider>
        <div className={styles.root}>
          {this.props.children}
        </div>
      </MuiThemeProvider>
    );
  }
}
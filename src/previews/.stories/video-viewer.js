import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import VideoViewer from '../video-viewer/video-viewer.component.jsx'

storiesOf('Video Viewer', module)
  .add('component', () => (
    <MuiThemeProvider>
      <div style={{width: 500}}>
        <VideoViewer
          video={{
            title: 'Title',
            description: 'This is a video',
            url: 'http://www.youtube.com/embed/h3YVKTxTOgU',
            createdAt: 'Jan. 15, 2016'
          }}
        />
      </div>
    </MuiThemeProvider>
  ))

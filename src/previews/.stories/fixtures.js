import { storiesOf, action } from '@kadira/storybook'

export const props = {
  onManualDetails: action('onManual'),
  menus: [
    { key: 1, label: 'label 1', link: '#', onClick: action('click') },
    { key: 2, label: 'label 2', link: '#', onClick: action('click') },
    { key: 3, label: 'label 3', link: '#', onClick: action('click') },
    { key: 4, label: 'label 4', link: '#', onClick: action('click') },
    { key: 5, label: 'label 5', link: '#', onClick: action('click') },
  ]
};

export const manual = {
  draftDescription: 'Material Design for Dummies',
  chapters: [
    {
      sequenceNumber: 0,
      label: 'Chapter 1',
      title: 'Discourse on the Nature of the Human Mind',
      body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\nnewline\nnewline'
    },
    {
      sequenceNumber: 1,
      label: 'Chapter 2',
      title: 'chap',
      body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\nnewline\nnewline'
    },
    {
      sequenceNumber: 2,
      label: 'Chapter 3',
      title: 'chop',
      body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\nnewline\nnewline'
    }
  ],
  tags: [
    { key: 'Chip' },
    { key: 'Chap' },
    { key: 'Chop' }
  ],
  viewFilters: [
    { key: 'Chip' },
    { key: 'Chap' },
    { key: 'Chop' }
  ],
  exemptionFilters: [
    { key: 'Chip' },
    { key: 'Chap' },
    { key: 'Chop' }
  ],
  editors: [
    { key: 'Chip' },
    { key: 'Chap' },
    { key: 'Chop' }
  ]
};

export const chapterViewProps = {
  chapter: {
    label: 'label',
    title: 'title',
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras feugiat lectus non lobortis facilisis. Duis eu malesuada nunc. Duis vitae lorem id nibh sagittis commodo sed quis dolor. Morbi viverra neque in ultrices tincidunt. Sed fringilla metus mollis quam rhoncus, et bibendum risus consectetur. Vestibulum placerat rhoncus pharetra. Donec fringilla est ac libero consectetur congue eget sed nulla. Donec in nibh nisl. Nullam tincidunt, dui vel blandit viverra, ex arcu euismod neque, at suscipit sem orci in felis.'
  }
};
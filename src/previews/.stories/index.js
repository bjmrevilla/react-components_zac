import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import ManualViewer from '../manual-viewer.component.jsx';
import ManualChapterViewer
  from '../manual-chapter-viewer/manual-chapter-viewer.component.jsx';
import ManualDetailsViewer
  from '../manual-details-viewer/manual-details-viewer.component.jsx';
import {
  manual,
  props,
  chapterViewProps
} from './fixtures';

require('./pdf-viewer.js');
require('./video-viewer.js');

storiesOf('Manual Viewer', module)
  .add('component', () => (
    <MuiThemeProvider>
      <div style={{height: '100vh'}}>
        <ManualViewer manual={manual} />
      </div>
    </MuiThemeProvider>
  ));
'use strict';

import React from 'react';
import styles from './manual-navigation.scss';
import { compose, flattenProp } from 'recompose';
import { List, ListItem } from 'material-ui/List';
import FlatButton from 'material-ui/FlatButton';
import Drawer from 'material-ui/Drawer';

export default class ManualNavigation extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    menus: React.PropTypes.arrayOf(React.PropTypes.shape({
      key: React.PropTypes.any.isRequired,
      label: React.PropTypes.string.isRequired,
      subtitle: React.PropTypes.string,
      link: React.PropTypes.string,
      disable: React.PropTypes.bool,
      onTouchTap: React.PropTypes.func
    })),
    showManualDetails: React.PropTypes.bool,
    open: React.PropTypes.bool
  }

  static defaultProps = {
    showManualDetails: true,
    open: false
  }

  render = () => {
    const { menus, showManualDetails, handleShowDetails } = this.props;

    return (
      <Drawer
        className={styles.root}
        open={this.props.open}
        docked={true}
        containerStyle={{ height: 'calc(100% - 64px)', top: '64px' }}
        width={250}
        zDepth={0}
        onRequestChange={this.props.onCloseDrawer}>
        <List className={styles.links}>
          {menus.map(({key, label, subtitle, link, disable, onTouchTap}) => (
            <div>
              {link
                ? <ListItem
                  key={key}
                  href={disable ? '' : link}
                  primaryText={label}
                  secondaryText={subtitle} />
                : <ListItem
                  key={key}
                  onTouchTap={disable ? '' : e => onTouchTap(key)}
                  primaryText={label}
                  secondaryText={subtitle} />}
            </div>
          ))}
        </List>
        {showManualDetails
          ? <div className={styles.manualDetails}>
            <FlatButton
              onTouchTap={handleShowDetails}
              style={{ width: '100%' }}>
              Manual Details
            </FlatButton>
          </div>
          : ''
        }
      </Drawer>
    );
  }
}
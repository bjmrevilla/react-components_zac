'use strict';

import React from 'react';
import CustomProgress from './custom-progress.component.jsx';
import CustomPlay from './custom-play.component.jsx';
import styles from './video-viewer.scss';
import compose from 'recompose/compose';
import flattenProp from 'recompose/flattenProp';
import { Media } from 'react-media-player';

export default compose(
  flattenProp('video')
)(({ title, description, url, createdAt }) => (
  <div className={styles.root}>
    <Media src={url}>
      {Player =>
        <div className={styles.media}>
          <div className={styles.video}>
            {Player}
          </div>
          <div className={styles.controls}>
            <CustomPlay />
            <div className={styles.progress}>
              <CustomProgress />
            </div>
          </div>
        </div>
      }
    </Media>
    <div className={styles.meta}>
      <div>
        <h1>{title}</h1>
        <p className={styles.description}>{description}</p>
      </div>
      <div className={styles.date}>
        <p>{createdAt}</p>
      </div>
    </div>
  </div>
));

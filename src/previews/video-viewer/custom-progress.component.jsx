'use strict';

import React from 'react';
import { withMediaProps } from 'react-media-player';
import moment from 'moment';
import styles from './custom-progress.scss';
import Slider from 'material-ui/Slider';

class CustomProgress extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    media: React.PropTypes.object
  };

  handleMoveSlider = (event, value) => {
    let timeSeeked = value * this.props.media.duration;

    this.props.media.seekTo(timeSeeked);
  };

  handleDragStart = () => {
    // doesn't have anything to do with rendering, so don't set state
    this.wasPlaying = this.props.media.isPlaying;

    if (this.wasPlaying) this.props.media.pause();
  }

  handleDragStop = () => {
    if (this.wasPlaying) this.props.media.play();
  }

  getTimeString = (time) => {
    time = time * 1000; // moment.duration takes in milliseconds

    let hours = moment.duration(time).hours();
    let minutes = moment.duration(time).minutes();
    let seconds = moment.duration(time).seconds();
    let timeString = '';

    let forceTwoDigits = num => ('0' + num).slice(-2);

    if (hours === 0) {
      timeString = minutes + ':'
        + forceTwoDigits(seconds);
    } else {
      timeString = hours + ':'
        + forceTwoDigits(minutes) + ':'
        + forceTwoDigits(seconds);
    }

    return timeString;
  }

  getSliderProgress = () => {
    return this.props.media.currentTime * 1.0 / this.props.media.duration;
  }

  render = () => {
    return (
      <div className={styles.root}>
        <span className={styles.time}>
          {this.getTimeString(this.props.media.currentTime)}
        </span>
        <Slider
          style={{ flex: 1 }}
          sliderStyle={{margin: 0}}
          defaultValue={0}
          value={this.getSliderProgress()}
          onDragStart={this.handleDragStart}
          onDragStop={this.handleDragStop}
          onChange={this.handleMoveSlider} />
        <span className={styles.time}>
          {this.getTimeString(this.props.media.duration)}
        </span>
      </div>
    );
  };
}

export default withMediaProps(CustomProgress)
import React from 'react';
import { withMediaProps } from 'react-media-player';
import styles from './custom-play.scss';
import IconButton from 'material-ui/IconButton';

class CustomPlay extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    media: React.PropTypes.object
  };

  shouldComponentUpdate = ({ media }) => {
    return this.props.media.isPlaying !== media.isPlaying;
  };

  handleTouchTap = () => {
    if (this.props.media.isPlaying) {
      this.props.media.pause();
    } else {
      this.props.media.play();
    }
  };

  getSvg = () => {
    if (this.props.media.isPlaying) {
      return <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M9 16h2V8H9v8zm3-14C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8zm1-4h2V8h-2v8z"/></svg>;
    } else {
      return <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M10 16.5l6-4.5-6-4.5v9zM12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"/></svg>;
    }
  };

  render = () => {
    return (
      <IconButton onTouchTap={this.handleTouchTap}>
        {this.getSvg()}
      </IconButton>
    );
  }
}

export default withMediaProps(CustomPlay)
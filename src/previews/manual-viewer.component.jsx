'use strict';

import React from 'react';
import ManualNavigation
  from './manual-navigation/manual-navigation.component.jsx';
import ManualDetailsViewer
  from './manual-details-viewer/manual-details-viewer.component.jsx';
import ManualChapterViewer
  from './manual-chapter-viewer/manual-chapter-viewer.component.jsx';
import styles from './manual-viewer.scss';
import AppBar from 'material-ui/AppBar';
import Dialog from 'material-ui/Dialog';

export default class ManualViewer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isNavigationOpen: false,
      currentChapterIndex: 0,
      isShowingDetails: false
    };
  }

  static propTypes = {
    manual: React.PropTypes.object,
    manualUse: React.PropTypes.object
  }

  handleClickBurger = () => {
    this.setState({
      isNavigationOpen: !this.state.isNavigationOpen
    });
  }

  handleCloseDrawer = () => {
    this.setState({
      isNavigationOpen: false
    });
  }

  getChildWidth = () => {
    return (this.state.isNavigationOpen)
      ? 'calc(100% - 250px)'
      : '100%';
  };

  handleShowDetails = () => {
    this.setState({
      isShowingDetails: true
    });
  }

  handleHideDetails = () => {
    this.setState({
      isShowingDetails: false
    });
  }

  getNavigationMenus = () => {
    return this.props.manual.chapters.map((chapter, index) => {
      let handleScroll = () => {
        let ref = 'chapter' + chapter.sequenceNumber;
        let chapterTopPos = this.refs[ref].offsetTop;

        this.refs.child.scrollTop = chapterTopPos - 70; // for app bar
        this.handleCloseDrawer();
      }

      return {
        key: chapter.sequenceNumber,
        label: chapter.label,
        subtitle: chapter.title,
        onTouchTap: handleScroll,
        disable: false
      };
    });
  }

  getMenuOnTouchTap = (index) => {
    let menuOnTouchTap = () => {
      this.setState({
        currentChapterIndex: index,
        isShowingDetails: false
      });
    }

    return menuOnTouchTap;
  }

  getDetailsDialog = () => {
    return (
      <Dialog
        open={this.state.isShowingDetails}
        modal={false}
        onRequestClose={this.handleHideDetails}
        autoScrollBodyContent={true}>
        <ManualDetailsViewer
          manual={this.props.manual} />
      </Dialog>
    );
  }

  getChapters = () => {
    return this.props.manual.chapters.map((chapter) => {
      let createMarkup = (text) => {
        let markup = '<p>'
          + text.replace(/\n([ \t]*\n)+/g, '</p><p>')
            .replace(/\n/g, '<br />')
          + '</p>';

        return { __html: markup };
      }

      return (
        <div
          ref={'chapter' + chapter.sequenceNumber}
          className={styles.chapter}>
          <h3
            id={'chapter' + chapter.sequenceNumber}
            className={styles.chapterTitle}>
            {chapter.label}: {chapter.title}
          </h3>
          <div
            className={styles.chapterBody}
            dangerouslySetInnerHTML={createMarkup(chapter.body)} />
        </div>
      );
    });
  }

  render = () => {
    return (
      <div className={styles.root}>
        <AppBar
          title={this.props.manual.draftDescription}
          onLeftIconButtonTouchTap={this.handleClickBurger} />
        <ManualNavigation
          menus={this.getNavigationMenus()}
          onCloseDrawer={this.handleCloseDrawer}
          handleShowDetails={this.handleShowDetails}
          open={this.state.isNavigationOpen} />
        <div
          ref='child'
          className={styles.child}
          style={{ width: this.getChildWidth() }}>
          {this.getChapters()}
        </div>
        {this.getDetailsDialog()}
      </div>
    );
  }
}
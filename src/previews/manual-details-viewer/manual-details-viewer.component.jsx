'use strict';

import React from 'react';
import TagsList from '../../list-view-page/tags-list';
import styles from './manual-details-viewer.scss';
import { compose, flattenProp } from 'recompose';

export default class ManualDetailsViewer extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    manual: React.PropTypes.object
  }

  render = () => {
    const {
      manual: {
        title,
        description,
        creator,
        version,
        publisher,
        publishedAt,
        tags,
        viewFilters,
        exemptionFilters,
        editors
      }
    } = this.props;

    return (
      <div className={styles.root}>
        <div className={styles.meta}>
          <div>
            <h1 className={styles.top}>
              {title} <small>v.{version}</small>
            </h1>
            <p className={styles.description}>{description}</p>
            <p>by {creator}</p>
          </div>
          <div className={styles.publisher}>
            <p className={styles.top}>{publisher}</p>
            <p className={styles.publishedAt}>{publishedAt}</p>
          </div>
        </div>
        <TagsList tags={tags} />
        <TagsList tags={viewFilters} />
        <TagsList tags={exemptionFilters} />
        <TagsList tags={editors} />
      </div>
    );
  }
}
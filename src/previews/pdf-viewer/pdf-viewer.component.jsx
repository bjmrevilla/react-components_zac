'use strict';

import React from 'react';
import { compose, flattenProp } from 'recompose';
import styles from './pdf-viewer.scss';

export default compose(
  flattenProp('popularized')
)(({ title, description, url, createdAt }) => (
  <div className={styles.root}>
    <div className={styles.meta}>
      <div>
        <h1>{title}</h1>
        <p className={styles.description}>{description}</p>
      </div>
      <div className={styles.date}>
        <p>{createdAt}</p>
      </div>
    </div>
    <embed
      width='100%'
      height='100%'
      src={url}
      className={styles.pdf}
      name='plugin'
      type='application/pdf'
    />
  </div>
));

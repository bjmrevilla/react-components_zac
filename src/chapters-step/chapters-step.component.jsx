'use strict';

import _ from 'lodash';
import React from 'react';
import CarouselView from '../list-view-page/carousel-view/carousel-view.component.jsx';
import ChapterEditor from './chapter-editor/chapter-editor.component.jsx';
import styles from './chapters-step.scss';
import {diff} from '../list-view-page/utils/utils.js';

const sort = (arr) => arr.sort((a, b) => a.sequenceNumber - b.sequenceNumber);

export default class ChaptersStep extends React.Component {
	constructor(props) {
		super(props);
		const {material: {$, chapters = []}} = props;

		this.state = {
			chapterIndex: $.chapterIndex || 0,
			tempChapters: chapters.slice(0)
		};
	}

	static propTypes = {
		chapters: React.PropTypes.arrayOf(React.PropTypes.object),
		onChange: React.PropTypes.func,
		edit: React.PropTypes.bool,

		saveDraft: React.PropTypes.func,
		forkChapter: React.PropTypes.func,
		createChapterDraft: React.PropTypes.func
	};

	static defaultProps = {
		edit: false,
		chapters: []
	};

	createNewChapter = (sequenceNumber) => {
		const {createChapterDraft, onChange} = this.props;
		let {tempChapters} = this.state;
		sequenceNumber = sequenceNumber || tempChapters.length;

		// Create New
		const newChapter = {
			sequenceNumber,
			questionList: {
				genType: 'preset',
				numOfQuestions: 0,
				questions: []
			},
			tags: [],
			label: `Chapter ${sequenceNumber}`,
			title: `Chapter ${sequenceNumber}`,
			body: `Write body here`,
			new: true
		};

		// Create draft and save id
		createChapterDraft(newChapter, (err, id) => {
			if (err) {
				// TODO: alert?

				return;
			}

			// Give id
			newChapter._id = id;

			// Add to local and sort
			tempChapters = sort(tempChapters.concat(newChapter));

			// Find index
			const chapterIndex = tempChapters.indexOf(newChapter);

			// Bubble up change
			// onChange('chapters', tempChapters, {chapterIndex});

			// console.log(`chapters ${JSON.stringify(tempChapters, null, 2)}`);

			// Set local change
			// this.setState({ tempChapters, chapterIndex })
		})
	};

	editChapter = (field, key, index) => {
		const {saveDraft, forkChapter} = this.props;
		let {tempChapters, chapterIndex} = this.state;
		chapterIndex = index || chapterIndex;

		// Original
		const origTempChapters = tempChapters.slice(0);
		const origCurrentChapter = {...tempChapters[chapterIndex]};
		const id = origCurrentChapter._id;

		// Mutate and sort
		tempChapters[chapterIndex][field] = key;
		tempChapters = sort(tempChapters);

		// Changes
		const changed = tempChapters.find(({_id}) => _id === id);
		const changes = diff(origCurrentChapter, changed);

		if (!_.isEmpty(changed)) return;
		chapterIndex = tempChapters.findIndex(({_id}) => _id === id);

		if (!/$$(new|fork)::/.test(origCurrentChapter._id)) {
			// Not yet edited, fork
			forkChapter({ ...changes, _id: id }, (err, id) => {

				if (err) {
					// TODO: warning?

					// Rollback changes
					tempChapters = origTempChapters;
				} else {
					tempChapters[chapterIndex]._id = `$$fork::${id}`;
				}

				// Bubble up change
				onChange('chapters', tempChapters, {chapterIndex});
			});
		}

		// Save
		saveDraft({ ...changes, _id: id }, err => {
			if (err) {
				// TODO: warning?

				// Rollback changes
				tempChapters = origTempChapters;
			}

			// Bubble up change
			onChange('chapters', tempChapters, { chapterIndex });
		});
	};

	removeChapter = (index) => {
		let {tempChapters, chapterIndex} = this.state;
		chapterIndex = index || chapterIndex;

		this.setState({
			tempChapters: tempChapters.filter((chp, ind) => ind !== chapterIndex)
		})
	};

	handleChaptersIntegrity = (props) => {
		const {material: {$, chapters = []}} = props || this.props;

		const tempChapters = chapters.slice(0);
		const chapterIndex = !$.chapterIndex || $.chapterIndex < 0
			? 0
			: $.chapterIndex >= tempChapters.length
				? tempChapters.length - 1
				: $.chapterIndex;

		if (_.isEmpty(tempChapters)) this.createNewChapter();
		else this.setState({ chapterIndex, tempChapters });
	};

	componentWillMount = () => {
		console.log(`#componentWillMount`);
		this.handleChaptersIntegrity();
	};

	componentWillUpdate = (nextProps) => {
		console.log(`#componentWillUpdate`);
		this.handleChaptersIntegrity(nextProps);
	};

	getChaptersCarousel = () => {
		const {tempChapters} = this.state;
		const actions = [
			{
				label: 'Swap position',
				fn: (chp) => {

				}
			}, {
				label: 'Add new to right',
				fn: (chp) => {

				}
			}, {
				label: 'Add new to left',
				fn: (chp) => {

				}
			}
		];

		return (
			<CarouselView
				data={tempChapters}
				hideImage
				emphasize
				actions={actions}
				title={{label: 'Title', key: 'label'}}
				subtitle={{label: 'Sub', key: 'title'}}
			/>
		);
	};

	render = () => {
		return (
			<div className={styles.root}>
				{this.getChaptersCarousel()}
				{/*<ChapterEditor
				 chapter={this.props.chapters[this.state.chapterIndex]}
				 edit={this.props.edit}
				 onChange={this.editChapter}
				 onDelete={this.removeChapter} />*/}
			</div>
		);
	}
}

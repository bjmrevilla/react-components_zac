export const chapter = {
  label: 'Chapter label',
  title: 'Chapter Title',
  sequenceNumber: 1,
  body: ''
};

export const chapters = [chapter];
import React from 'react';
import { compose, withHandlers, withState } from 'recompose'
import {storiesOf, action} from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import ChaptersStep from '../chapters-step.component.jsx';
import {chapters} from './fixtures.js';

require('./chapter-editor.js');

storiesOf('chapters-step', module)
  .add('component', () => (
    <MuiThemeProvider>
      <ChaptersStep chapters={chapters} />
    </MuiThemeProvider>
  ));
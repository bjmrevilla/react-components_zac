import React from 'react';
import { compose, withHandlers, withState } from 'recompose'
import {storiesOf, action} from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import ChapterEditor from '../chapter-editor/chapter-editor.component.jsx';
import {chapter} from './fixtures.js';

const edit = true;

storiesOf('chapters-step', module)
  .add('chapter-editor', () => (
    <MuiThemeProvider>
      <ChapterEditor chapter={chapter} />
    </MuiThemeProvider>
  ));

storiesOf('chapters-step', module)
  .add('chapter-editor editable', () => (
    <MuiThemeProvider>
      <ChapterEditor chapter={chapter} edit={true} />
    </MuiThemeProvider>
  ));
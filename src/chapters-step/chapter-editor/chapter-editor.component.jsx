'use strict';

import React from 'react';
import scss from './chapter-editor.scss';
import TinyMCE from 'react-tinymce-input';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

export default class ChapterEditor extends React.Component {
  constructor(props) {
    super(props);
    this.state = { changes: { } };
  }

  static propTypes = {
    chapter: React.PropTypes.object,
    edit: React.PropTypes.bool,
    onChange: React.PropTypes.func,
    onDelete: React.PropTypes.func
  };

  static defaultProps = {
    edit: false
  };

  componentDidMount() {
    this.handleUpdate();
  }

  componentDidUpdate() {
    this.handleUpdate();
  }

  handleUpdate = () => {
    // don't update if there are no changes to prevent infinite render loop
    if (Object.keys(this.state.changes).length !== 0) {
      this.props.onChange(this.state.changes);

      this.setState({ changes: {} });
    }
  };

  onChangeTitle = (e) => {
    e.persist();
    const { value } = e.target;
    let newChanges = this.state.changes;
    newChanges[title] = value;

    this.setState(newChanges);
  };

  onChangeSequenceNumber = (e) => {
    e.persist();
    const { value } = e.target;
    let newChanges = this.state.changes;
    newChanges[sequenceNumber] = value;

    this.setState(newChanges);
  };

  onChangeBody = (body) => {
    let newChanges = this.state.changes;
    newChanges[body] = value;

    this.setState(newChanges);
  };

  render = () => {
    const { chapter = {}, edit, onDelete, onQuestions } = this.props;
    const { sequenceNumber, title, body } = chapter;

    return (
      <div className={scss.root} style={styles.container}>
        <div style={styles.inputRow}>
          <TextField
            style={styles.editorNumber}
            type='text'
            value={sequenceNumber}
            placeholder='#'
            onChange={this.onChangeSequenceNumber.bind(this) }
            disabled={!edit}
            />
          <TextField
            style={styles.editorTitle}
            type='text'
            value={title}
            placeholder='Enter chapter title here'
            onChange={this.onChangeTitle.bind(this) }
            disabled={!edit}
            />
          <RaisedButton label='Questions' primary onClick={onQuestions} />
        </div>
        <div style={styles.editorChapter}>
          <TinyMCE
            ref='tinyMCE'
            tinymceConfig={{
              plugins: 'autolink link image table lists paste preview',
              height: '400',
              automatic_uploads: true,
              paste_data_images: true,
              images_upload_url: '/cdn/upload/',
              images_upload_base_path: '/cdn/',
              images_upload_credentials: true,
              body_style: 'img { height: auto; max-width: 100%; }',
              readonly: !edit
            }}
            value={body}
            onChange={this.onChangeBody.bind(this) }
            />
        </div>
        <div>
          <h1>Danger Zone!</h1>
          <RaisedButton label='Delete' secondary onClick={onDelete} />
        </div>
      </div>
    )
  }
}

const styles = {
  container: {
    backgroundColor: 'white',
    borderColor: '#e8e8e8',
    borderStyle: 'solid',
    borderRadius: 6,
    borderWidth: 1,
    boxSizing: 'border-box',
    padding: '1rem 2rem 1rem'
  },
  inputRow: {
    width: '100%'
  },
  editorNumber: {
    border: 0,
    boxSizing: 'border-box',
    fontSize: '2rem',
    fontWeight: 'bold',
    outline: 'none',
    marginRight: '1rem',
    width: '3rem'
  },
  editorTitle: {
    border: 0,
    boxSizing: 'border-box',
    fontSize: '2rem',
    fontWeight: 'bold',
    marginRight: '1rem',
    outline: 'none'
  },
  editorChapter: {
    paddingTop: '1rem'
  }
};

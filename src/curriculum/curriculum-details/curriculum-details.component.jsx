/**
 * Created by metsys on 11/14/16.
 */
'use strict';
import React, { PropTypes } from 'react';
import styles from './curriculum-details.scss';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import TagList from '../../tag-list';
import CoverPic from '../../cover-photo';
import QuestionList from '../../question-list';

export default class CurriculumDetails extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			openQuestionList: false
		}
	}

	static PropTypes = {
		curriculum: PropTypes.object,
		uploadCoverPic: PropTypes.func,
		enrol: PropTypes.func,
		// ViewMode
		readOnly: PropTypes.bool,
		edit: PropTypes.bool,

		save: PropTypes.func,
		create: PropTypes.func,
		delete: PropTypes.func,

		onChange: PropTypes.func,
	};

	static defaultProps = {
		curriculum: {},
		uploadCoverPic: () => {},
		enrol: () => {},

		save: () => {},
		create: () => {},
		delete: () => {},
		onChange: () => {}
	};

	getQuestionListDialog = () => {
		const { openQuestionList } = this.state;
		const { curriculum } = this.props;

		const closeDialog = () => {
			this.setState({ openQuestionList: false });
		};

		return (
			<Dialog
				open={openQuestionList}
				onRequestClose={closeDialog}
				titleStyle={{ padding: 0, margin: 0 }}
				contentStyle={{ width: '95%', maxWidth: 'none' }}
			>
				<FlatButton
					label="Close"
					secondary
					style={{float: 'right'}}
					onTouchTap={closeDialog}
				/>
				<QuestionList
					{ ...this.props }
					label={'Curriculum'}
					questionList={JSON.parse(JSON.stringify(curriculum.questionList))}
				/>
				<FlatButton
					label="Close"
					secondary
					style={{marginTop: 10, float: 'right'}}
					onTouchTap={closeDialog}
				/>
			</Dialog>
		)
	};

	render(){
		const { searchRole, sourceRoles, canAddRemoveQuestions, canEnrol, enrol } = this.props;
		const { curriculum, uploadCoverPic, onChange, readOnly, disabled } = this.props;
		const { cover, title, description, requiredRoles } = curriculum;
		return (
			<div className={styles.root}>
				{
					this.getQuestionListDialog()
				}
				<CoverPic
					{ ...this.props }
					cover={cover}
					uploadFunc={uploadCoverPic}
				/>
				<div className={styles.details}>
					<TextField
						id='title'
						floatingLabelText='Title'
						value={title}
						readOnly={readOnly}
						disabled={disabled}
						fullWidth
						onChange={ e => onChange('title', e.target.value) }
					/>
					<TextField
						id='description'
						floatingLabelText='Description'
						value={description}
						readOnly={readOnly}
						disabled={disabled}
						fullWidth
						onChange={ e => onChange('description', e.target.value) }
					/>
					<TagList
						title="Required Roles"
						label="Role"

						readOnly={readOnly}
						disabled={disabled}

						tags={requiredRoles}
						onTagsChanged={ tags => onChange('requiredRoles', tags) }

						// Selection
						search={searchRole}
						sourceItems={sourceRoles}
					/>
					{
						canAddRemoveQuestions &&
						<div style={{ display: 'flex' }}>
							<RaisedButton
								onClick={() => this.setState({ openQuestionList: true })}
								label='Question List'
								primary
								disabled={disabled}
								style={{ marginRight: 10 }}
							/>
							{
								!canEnrol &&
								<RaisedButton
									onClick={enrol}
									label='Enrol'
									primary
									disabled={disabled}
								/>
							}
						</div>
					}
				</div>
			</div>
		)
	}
}

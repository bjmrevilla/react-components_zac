/**
 * Created by metsys on 11/14/16.
 */
'use strict';

import _ from 'lodash';
import React, { PropTypes } from 'react';
import styles from './curriculum.scss';
import Paper from 'material-ui/Paper';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import Tabs from 'material-ui/Tabs/Tabs';
import Tab from 'material-ui/Tabs/Tab';
import CurriculumDetails from './curriculum-details';
import CurriculumTab from './curriculum-tab';
import diff from '../utils/diff-data';

const STATUS_LOADING = 'status::loading';
const STATUS_NORMAL = 'status::normal';

export default class Curriculum extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			status: STATUS_NORMAL
		}
	}

	// Clone Original
	handleTempState = (props) => {
		const {curriculum} = props || this.props;
		console.log(`reClone`);
		this.setState({
			tempCurriculum: {...curriculum}
		})
	};

	componentWillReceiveProps = (newProps) => {
		this.handleTempState(newProps)
	};

	componentWillMount = () => {
		this.handleTempState()
	};

	static PropTypes = {
		curriculum: PropTypes.object,
		uploadCoverPic: PropTypes.func,
		enrol: PropTypes.func,

		// ViewMode
		readOnly: PropTypes.bool,
		edit: PropTypes.bool,

		save: PropTypes.func,
		create: PropTypes.func,
		delete: PropTypes.func
	};

	static defaultProps = {
		uploadCoverPic: () => {},
		enrol: () => {},

		save: () => {},
		create: () => {},
		delete: () => {}
	};

	actionCb = (err) => {
		if (err) {
			// TODO: improve
			alert (err.message);
		}

		this.setState({ status: STATUS_NORMAL });
	};

	onChange = (field, value) => {
		const { tempCurriculum } = this.state;
		tempCurriculum[field] = value;
		this.setState({ tempCurriculum });
	};

	onSaveAction = () => {
		const { edit, save, create, curriculum } = this.props;
		const { tempCurriculum } = this.state;
		const action = edit? save: create;

		const changes = diff(curriculum, tempCurriculum);

		this.setState({ status: STATUS_LOADING });
		action(changes, this.actionCb)
	};

	onEnrolAction = () => {
		const { curriculum, enrol } = this.props;
		this.setState({ status: STATUS_LOADING });
		enrol({
			_id: curriculum._id
		}, this.actionCb)
	};

	getAuxControls = () => {
		const { status } = this.state;
		const { readOnly } = this.props;
		if (readOnly) return;

		return (
			<div className={styles.controls}>
				<FlatButton
					disabled={status === STATUS_LOADING}
					onTouchTap={this.handleTempState}
					label='Cancel'
					secondary/>
				<RaisedButton
					label='Save'
					onTouchTap={this.onSaveAction}
					disabled={status === STATUS_LOADING}
					primary
				/>
			</div>
		);
	};

	getMaterialsTab = () => {
		const { tempCurriculum } = this.state;
		const { nextNMaterials, hasNextMaterials, canChangeMaterials, materialSources } = this.props;
		const { materialHeaders, materialCarouselKeys, materialSourcesSearch } = this.props;

		return (
			<Tab label={'Materials'}>
				<CurriculumTab
					{...this.props}
					compareUsingKeys={['_id', 'title']}
					label={'Material'}
					data={tempCurriculum.materials}
					nextPage={nextNMaterials}
					hasNextPage={hasNextMaterials}

					// Tabular headers
					headers={materialHeaders}

					// Carousel Keys
					{ ...materialCarouselKeys }

					// Changes
					canEdit={canChangeMaterials}
					changeKey={'materials'}
					onChange={this.onChange}

					// Choices
					dataSource={materialSources}
					dataSourceSearch={materialSourcesSearch}
				/>
			</Tab>
		)
	};

	getStudentsTab = () => {
		const { tempCurriculum } = this.state;
		const { nextNStudents, hasNextStudents, canChangeStudents, studentSourcesSearch } = this.props;
		const { studentHeaders, studentCarouselKeys, studentSources } = this.props;

		return (
			<Tab label={'Students'}>
				<CurriculumTab
					{...this.props}
					compareUsingKeys={['_id', 'username']}
					label={'Student'}
					data={tempCurriculum.students}
					nextPage={nextNStudents}
					hasNextPage={hasNextStudents}

					// Tabular headers
					headers={studentHeaders}

					// Carousel Keys
					{ ...studentCarouselKeys }

					// Changes
					canEdit={canChangeStudents}
					changeKey={'students'}
					onChange={this.onChange}

					// Choices
					dataSource={studentSources}
					dataSourceSearch={studentSourcesSearch}
				/>
			</Tab>
		)
	};

	getAdminsTab = () => {
		const { tempCurriculum } = this.state;
		const { nextNAdmins, hasNextAdmins, canChangeAdmins } = this.props;
		const { adminHeaders, adminCarouselKeys, adminSources, adminSourcesSearch } = this.props;

		return (
			<Tab label={'Admins'}>
				<CurriculumTab
					{...this.props}
					compareUsingKeys={['_id', 'username']}
					label={'Admin'}
					data={tempCurriculum.admins}
					nextPage={nextNAdmins}
					hasNextPage={hasNextAdmins}

					// Tabular headers
					headers={adminHeaders}

					// Carousel Keys
					{ ...adminCarouselKeys }

					// Changes
					canEdit={canChangeAdmins}
					changeKey={'admins'}
					onChange={this.onChange}

					// Choices
					dataSource={adminSources}
					dataSourceSearch={adminSourcesSearch}
				/>
			</Tab>
		)
	};

	render(){
		const { tempCurriculum } = this.state;
		return (
			<div className={styles.root}>
				<Paper className={styles.details}>

					{
						this.getAuxControls()
					}

					<CurriculumDetails
						{...this.props}
						enrol={this.onEnrolAction}
						curriculum={tempCurriculum}
						onChange={this.onChange}
					/>

				</Paper>
				<Tabs className={styles.tabs}>
					{ this.getMaterialsTab() }
					{ this.getStudentsTab() }
					{ this.getAdminsTab() }
				</Tabs>

				<div className={styles.footer}></div>
			</div>
		)
	}
}

/**
 * Created by metsys on 11/14/16.
 */
'use strict';

import _ from 'lodash';
import React, { PropTypes } from 'react';
import styles from './curriculum-tab.scss';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import ListViewPage from '../../list-view-page';
import Selection from '../../selection';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';

export default class Curriculum extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			openSelection: false
		}
	}

	static PropTypes = {
		data: PropTypes.arrayOf(PropTypes.object)
	};

	static defaultProps = {

	};

	getActions = () => {
		const { readOnly, disabled, canEdit, data, changeKey, onChange } = this.props;
		const { compareUsingKeys = [] } = this.props;
		if (readOnly || disabled || !canEdit) return;

		function isSameQ(a) {
			return (b) => compareUsingKeys.find(k => a[k] === b[k]);
		}

		return [{
			icon: <DeleteIcon />,
			label: 'Delete',
			fn: d => d => onChange(changeKey, data.filter(a => !isSameQ(q)(a)))
		}]
	};

	getAddNew = () => {
		const { readOnly, disabled, canEdit } = this.props;
		if (readOnly || disabled || !canEdit) return;

		return () => this.setState({ openSelection: true });
	};

	render(){
		const { openSelection } = this.state;
		const { data, onChange, changeKey, label, dataSource, dataSourceSearch, thumbTitle, compareUsingKeys } = this.props;

		const handleClose = () => this.setState({ openSelection: false });

		return (
			<div className={styles.root}>
				<Dialog
					title={ `Select ${label}s` }
					open={ !!openSelection }
					actions={[ <RaisedButton label="Finish" primary onTouchTap={ handleClose } /> ]}
					onRequestClose={ handleClose }
				>
					<Selection
						compareUsingKeys={compareUsingKeys}
						sourceItems={dataSource}
						search={dataSourceSearch}
						keyToUse={thumbTitle.key}
						label={label}
						selectedItems={data}
						onTagsChanged={tags => onChange(changeKey, tags)}
					/>
				</Dialog>
				<ListViewPage
					{ ...this.props }
					showTitle={false}
					enableSearch={false}
					viewModes={['tabular', 'fullpage', 'carousel']}
					itemsToViewList={[5]}

					onAddNewTouchTap={this.getAddNew()}

					// Actions
					actions={this.getActions()}
				/>
			</div>
		)
	}
}

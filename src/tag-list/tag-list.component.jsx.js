/**
 * Created by metsys on 11/13/16.
 */
'use strict';
const log = require('debug')('TagListView::Component');

import _ from 'lodash';
import React, { PropTypes } from 'react';
import styles from './tag-list.scss';
import Selection from '../selection';
import Dialog from 'material-ui/Dialog';
import Chip from 'material-ui/Chip';
import IconButton from 'material-ui/IconButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import RaisedButton from 'material-ui/RaisedButton';

export default class TagList extends React.Component {
	constructor(props){
		super(props);
		this.state = {}
	}

	static propTypes = {
		// Tags to display
		label: PropTypes.string.isRequired,
		tags: PropTypes.arrayOf(PropTypes.any),
		onTagsChanged: PropTypes.func,
		keyToUse: PropTypes.string,

		// Description
		title: PropTypes.string,
		showTitle: PropTypes.bool,
		description: PropTypes.string,
		showDescription: PropTypes.bool,
		tip: PropTypes.string,
		showTip: PropTypes.bool,

		// Read-only status
		readOnly: PropTypes.bool,
		disabled: PropTypes.bool,
		disableAddNew: PropTypes.bool,
		disableRemove: PropTypes.bool,

		hideFloatingAddButton: PropTypes.bool,
		hideAddButton: PropTypes.bool
	};

	static defaultProps = {
		label: 'Tag',
		tags: [],
		onTagsChanged: () => {},
		keyToUse: 'name',

		title: 'Tags',
		showTitle: true,
		showDescription: true,
		tip: 'Manipulate tags here',
		showTip: false,

		disabled: false,
		disableAddNew: false,
		disableRemove: false,

		hideFloatingAddButton: false,
		hideAddButton: false
	};

	deleteTag = (tag) => {
		log(`#deleteTag ${JSON.stringify(tag)}`);
		const { tags, onTagsChanged } = this.props;
		onTagsChanged(tags.filter(t => !_.isEqual(t, tag)))
	};

	getTags = () => {
		let { readOnly, tags, keyToUse, disableRemove, disableAddNew, hideFloatingAddButton } = this.props;
		disableAddNew = readOnly || disableAddNew || hideFloatingAddButton;
		disableRemove = readOnly || disableRemove;

		return (
			<div className={styles.tags}>
				{
					tags.map(( tag, ind ) => {

						const label = _.isPlainObject(tag)
							? tag[keyToUse]
							: tag;

						const onRequestDelete = disableRemove
							? undefined
							: () => this.deleteTag(tag);

						return (
							<Chip
								className={styles.tag}
								key={ind}
								onRequestDelete={onRequestDelete}
							>
								{ label }
							</Chip>
						)
					})
				}
				{
					!disableAddNew &&
					<IconButton
						style={{ position: 'relative', bottom: 8}}
						name='add'
						onClick={ () => this.setState({ open: true }) }
					><ContentAdd />
					</IconButton>
				}
			</div>
		)
	};

	render(){
		const { open } = this.state;
		const { tags, onTagsChanged, label } = this.props;
		const { showTitle, title, showDescription, description = `list of ${label.toLowerCase()}s`, showTip, tip } = this.props;

		const handleClose = ()  => {
			this.setState({ open: false })
		};

		return (
			<div className={styles.root}>
				<Dialog
					title={ `Select ${label}s` }
					open={ !!open }
					actions={[ <RaisedButton label="Finish" primary onTouchTap={ handleClose } /> ]}
					onRequestClose={ handleClose }
					>
					<Selection
						{ ...this.props }
						selectedItems={tags}
						onSelectedItemsChanged={onTagsChanged}
					/>
				</Dialog>

				{
					!!showTitle &&
						<div>
							<span className={styles.title}><strong>{ title }</strong></span>
							<br/>
						</div>
				}
				<br />
				{
					!!showDescription &&
						<div>
							<span className={styles.description}>{ description }</span>
							<br/>
						</div>
				}
				{
					!!showTip &&
						<div>
							<span className={styles.tip}>{ tip }</span>
							<br/>
						</div>
				}
				<div className={styles.tags}>
					<span className={styles.label}>{ `${label}s` }</span>
					<br/>
					{
						this.getTags()
					}
				</div>

			</div>
		)
	}
}

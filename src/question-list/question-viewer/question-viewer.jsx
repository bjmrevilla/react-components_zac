/**
 * Created by metsys on 11/16/16.
 */

'use strict';
const log = require('debug')('QuestionViewer::Component');

import _ from 'lodash';
import React, {PropTypes} from 'react';
import styles from './question-viewer.scss';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TagList from '../../tag-list';

const TYPE_IDENTIFICATION = 'identification';
const TYPE_MULTIPLE_CHOICE = 'multiplechoice';

const TYPE_DESCRIPTION = {
	[TYPE_IDENTIFICATION]: `Identification`,
	[TYPE_MULTIPLE_CHOICE]: 'Multiple choices. Provide at least 2 choices'
};

export default class QuestionViewer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	// Clone Original
	handleTempState = (props) => {
		const {question} = props || this.props;
		this.setState({
			tempQuestion: {
				...question,
				type: question && question.type || TYPE_IDENTIFICATION
			}
		})
	};

	componentWillReceiveProps = (newProps) => {
		this.handleTempState(newProps)
	};

	componentWillMount = () => {
		this.handleTempState()
	};

	static propTypes = {
		readOnly: PropTypes.bool,
		edit: PropTypes.bool,
		question: PropTypes.object,
		save: PropTypes.func,
		create: PropTypes.func
	};

	static defaultProps = {
		question: {},
		save: () => {},
		create: () => {}
	};

	onChange = (field, value) => {
		const { tempQuestion } = this.state;
		tempQuestion[field] = value;
		this.setState({ tempQuestion });
	};

	onSaveAction = (e) => {
		e && e.preventDefault();

		const { edit, save, create } = this.props;
		const { tempQuestion } = this.state;
		const { question, type, choices = [], answer } = tempQuestion;
		const action = edit? save: create;

		if (!question)
			return alert('Question required!');

		if (type === TYPE_MULTIPLE_CHOICE) {
			if (choices.length < 2)
				return alert(`At least 2 Choices required!`);

			if (answer && choices.indexOf(answer) === -1)
				return alert(`Answer must be one of the choices`);
		} else {
			delete tempQuestion.choices;
		}

		if (!answer)
			return alert('Answer required');

		action(tempQuestion, this.actionCb);
	};

	getTypeControl = () => {
		const {readOnly, disabled} = this.props;
		const {tempQuestion: { type }} = this.state;

		const qTypeDescription = TYPE_DESCRIPTION[type];

		return (
			<div>
				<div className={styles.genTypeControls}>
					<div className={styles.genType}>
						<span className={styles.genTypeLabel}>
							{`Type`}
						</span>
						{
							readOnly
								? <TextField
										readOnly
										style={{width: 100}}
										name="type"
										value={_.upperFirst(type)}
									/>
								: <SelectField
										disabled={disabled}
										style={{width: 200}}
										value={type}
										onChange={(e, i, value) => this.onChange('type', value)}
									>
										<MenuItem value={TYPE_IDENTIFICATION} primaryText="Identification"/>
										<MenuItem value={TYPE_MULTIPLE_CHOICE} primaryText="Multiple choices"/>
									</SelectField>
						}
					</div>
				</div>

				<p className={styles.genTypeDescription}>
					{ qTypeDescription }
				</p>

			</div>
		)
	};

	getHeader = () => {
		const {readOnly, edit } = this.props;
		const title = readOnly
			? `Question`
			: `${edit? 'Edit': 'Create'} Question`;
		const description = readOnly
			? `View a question`
			: `${edit? 'Edit': 'Create'} a question`;

		return (
			<div className={styles.header}>

				<div className={styles.label}>
						<span className={styles.title}>
							<strong>{title}</strong>
						</span>
					<p className={styles.description}>
						<emphasis>{description}</emphasis>
					</p>
				</div>

				<div style={{flex: 1}}></div>

				{
					this.getTypeControl()
				}

			</div>
		)
	};

	render() {
		const {readOnly, disabled, edit} = this.props;
		const {tempQuestion: { type, question, choices = [], answer }} = this.state;

		return (
			<form className={styles.root} onSubmit={this.onSaveAction}>
				{
					this.getHeader()
				}
				{
					!readOnly &&
					<p className={styles.instruction}>Must provide a question</p>
				}

				<TextField
					readOnly={readOnly}
					hintText={'Question'}
					floatingLabelText={'Question'}
					floatingLabelFixed
					value={question}
					fullWidth
					onChange={e => this.onChange('question', e.target.value)}
				/>
				{
					type === TYPE_MULTIPLE_CHOICE &&
					<TagList
						title="Choices"
						label="Choice"
						tip="Add Choices"
						disableSearch
						addInstruction="Create Choices"

						readOnly={readOnly}
						disabled={disabled}

						tags={choices}
						onTagsChanged={ tags => this.onChange('choices', tags) }
						addFunction={v => v}
					/>
				}

				{
					!readOnly &&
					type === TYPE_IDENTIFICATION
						? <p className={styles.instruction}>Provide an answer</p>
						: <p className={styles.instruction}>Must choose an answer that is in the choices</p>
				}

				{
					(type === TYPE_IDENTIFICATION || readOnly)
						? <TextField
								floatingLabelText={'Answer'}
								hintText={'Answer'}
								floatingLabelFixed
								fullWidth
								value={answer}
								readOnly={readOnly}
								onChange={ e => this.onChange('answer', e.target.value) }
							/>
						: <SelectField
								floatingLabelText={'Answer'}
								hintText={'Answer'}
								floatingLabelFixed
								fullWidth
								disabled={disabled}
								value={choices.find(c => _.isEqual(c, answer))}
								onChange={(e, i, value) => this.onChange('answer', value)}
							>
								{
									choices.map((c, i) => <MenuItem key={i} value={c} primaryText={c}/>)
								}
							</SelectField>
				}

				{
					!readOnly &&
					<div className={styles.footer}>
						<RaisedButton
							className={styles.addButton}
							disabled={disabled}
							label={edit? 'Save': 'Create'}
							primary
							// onTouchTap={this.onSaveAction}
							type='submit'
						/>
					</div>
				}
			</form>
		)
	}
}

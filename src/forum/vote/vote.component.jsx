/**
 * Created by metsys on 11/13/16.
 */
'use strict';

import React, { PropTypes } from 'react';
import styles from './vote.scss';
import IconButton from 'material-ui/IconButton';
import HardwareKeyboardArrowUp from 'material-ui/svg-icons/hardware/keyboard-arrow-up';
import HardwareKeyboardArrowDown from 'material-ui/svg-icons/hardware/keyboard-arrow-down';
import Check from 'material-ui/svg-icons/action/check-circle';

export default class Vote extends React.Component {
	constructor(props) {
		super(props);
	}

	static propTypes = {
		votes: PropTypes.number,
		vote: PropTypes.func,
		checked: PropTypes.bool
	};

	static defaultProps = {
		votes: 0,
		vote: () => {}
	};

	render(){
		const { votes, vote, checked } = this.props;

		return (
			<div className={styles.root}>
				<IconButton
					name='upvote'
					tooltip='This answer is useful!'
					tooltipPosition='bottom-center'
					className={styles.up}
					onTouchTap={() => vote('up')}>
					<HardwareKeyboardArrowUp
						className={styles.upIcon}
					/>
				</IconButton>
				<span> {votes} </span>
				<IconButton
					name='down vote'
					tooltip='This answer is not useful!'
					tooltipPosition='top-center'
					className={styles.down}
					onTouchTap={() => vote('down')}>
					<HardwareKeyboardArrowDown
						className={styles.downIcon}
					/>
				</IconButton>
				<span className={styles.footer}>Votes</span>
				<Check
					style={{
						height: 50,
						width: 50,
						color: checked? 'green': 'gray'
					}}
				/>
			</div>
		)
	}
}

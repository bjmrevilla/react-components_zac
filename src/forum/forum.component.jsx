/**
 * Created by metsys on 11/13/16.
 */
'use strict';
import React, { PropTypes } from 'react';
import styles from './forum.scss';
import {diff} from '../list-view-page/utils';
import Paper from 'material-ui/Paper';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import Vote from './vote';
import TagList from '../tag-list';
import AnswerList from './answer-list';
import defaultImage from '../assets/kc-user-profile-photo.png';

const STATUS_LOADING = 'status::loading';
const STATUS_NORMAL = 'status::normal';

export default class Forum extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			status: STATUS_NORMAL
		}
	}

	// Clone Original
	handleTempState = (props) => {
		const {topic} = props || this.props;
		this.setState({
			tempTopic: {...topic}
		})
	};

	componentWillReceiveProps = (newProps) => {
		this.handleTempState(newProps)
	};

	componentWillMount = () => {
		this.handleTempState()
	};

	static propTypes = {
		// Topic
		topic: PropTypes.object,

		// Edit mode
		readOnly: PropTypes.bool,

		// Actions
		save: PropTypes.func,
		answer: PropTypes.func,
		delete: PropTypes.func,

		canChooseAnswer: PropTypes.bool,
		chooseAnswer: PropTypes.func,

		canVote: PropTypes.bool,
		vote: PropTypes.func,

		voteAnswer: PropTypes.func
	};

	static defaultProps = {
		save: () => {},
		delete: () => {},
		answer: () => {},

		chooseAnswer: () => {},
		vote: () => {},
		voteAnswer: () => {}
	};

	actionCb = (err) => {
		if (err) {
			// TODO: improve
			alert (err.message);
		}

		this.setState({ status: STATUS_NORMAL });
	};

	onSaveAction = () => {
		const { save, topic } = this.props;
		const { tempTopic } = this.state;

		const changes = diff(topic, tempTopic);

		this.setState({ status: STATUS_LOADING });
		save(changes, this.actionCb)
	};

	onAnswerAction = ( answer ) => {
		const { topic, answer: answerAction } = this.props;
		this.setState({ status: STATUS_LOADING });
		answerAction({
			_id: topic._id,
			answer
		}, this.actionCb)
	};

	onChooseAnswerAction = ( answer ) => {
		const { topic, chooseAnswer } = this.props;
		this.setState({ status: STATUS_LOADING });
		chooseAnswer({
			_id: topic._id,
			answer
		}, this.actionCb)
	};

	onVoteAction = (direction) => {
		const { topic, vote } = this.props;
		this.setState({ status: STATUS_LOADING });
		vote({
			_id: topic._id,
			direction
		}, this.actionCb)
	};

	handleChange = (field, value) => {
		let { tempTopic } = this.state;
		tempTopic[field] = value;
		this.setState({ tempTopic });
	};

	getAuxControls = () => {
		const { status } = this.state;
		const { readOnly } = this.props;
		if (readOnly) return;

		return (
			<div className={styles.controls}>
				<FlatButton
					disabled={status === STATUS_LOADING}
					onTouchTap={this.handleTempState}
					label='Cancel'
					secondary/>
				<RaisedButton
					label='Save'
					onTouchTap={this.onSaveAction}
					disabled={status === STATUS_LOADING}
					primary
				/>
			</div>
		);
	};

	getTopicBody = () => {
		const { topic, canVote, readOnly, addTagAction, searchTag, sourceTags } = this.props;
		const { title, body, creator, voteCount } = topic;
		const { tempTopic: { tags } } = this.state;
		const _picSrc = creator.pic
			? `/cdn/${creator.pic}`
			: defaultImage;

		return (
			<Paper
				zDepth={3}
				className={styles.topic}
			>
				{
					this.getAuxControls()
				}

				<div
					className={styles.withVote}
				>
					<Vote
						disabled={canVote}
						votes={voteCount}
						vote={this.onVoteAction}
					/>
					<img src={_picSrc} className={styles.pic}/>
					<div className={styles.details}>
						<span className={styles.title}>{title}</span>
						<p className={styles.body}> {body} </p>
						<span className={styles.creator}>by {creator.username} </span>
					</div>
				</div>

				<TagList
					title='Tags'
					description='Tags for the topic'
					label='Forum tags'

					addFunction={addTagAction}
					search={searchTag}
					sourceItems={sourceTags}

					showTitle={false}
					showDescription={false}
					readOnly={ readOnly }
					tags={ tags }
					onTagsChanged={tags => this.handleChange('tags', tags)}
				/>
			</Paper>
		)
	};

	getAnswerList = () => {
		const { topic, canChooseAnswer } = this.props;
		const { chosenAnswer, answers } = topic;
		const { status } = this.state;

		return (
			<AnswerList
				{ ...this.props }
				chosenAnswer={chosenAnswer}
				answers={answers}

				chooseAnswer={this.onChooseAnswerAction}

				answer={this.onAnswerAction}
				disabled={status === STATUS_LOADING}
				canChooseAnswer={canChooseAnswer && !chosenAnswer}
			/>
		)
	};

	render(){
		return (
			<div className={styles.root}>
				{
					this.getTopicBody()
				}
				<hr/>
				{
					this.getAnswerList()
				}
			</div>
		)
	}
}

/**
 * Created by metsys on 11/13/16.
 */
'use strict';

import React, { PropTypes } from 'react';
import styles from './answer.scss';
import Paper from 'material-ui/Paper';
import Vote from '../vote';
import RaisedButton from 'material-ui/RaisedButton';
import defaultImage from '../../assets/kc-user-profile-photo.png';

const STATUS_LOADING = 'status::loading';
const STATUS_NORMAL = 'status::normal';

export default class Answer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			status: STATUS_LOADING
		}
	}

	static propTypes = {
		// Answer
		answer: PropTypes.object,

		// Edit mode
		chosenAnswer: PropTypes.bool,

		// Actions
		save: PropTypes.func,
		delete: PropTypes.func,

		canChooseAnswer: PropTypes.bool,
		chooseAnswer: PropTypes.func,

		canVote: PropTypes.bool,
		vote: PropTypes.func,

		voteAnswer: PropTypes.func
	};

	static defaultProps = {
		save: () => {},
		delete: () => {},
		answer: () => {},

		chooseAnswer: () => {},
		vote: () => {},
		voteAnswer: () => {}
	};

	onVoteAction = (direction) => {
		const { answer, vote } = this.props;
		this.setState({ status: STATUS_LOADING });
		vote({
			_id: answer._id,
			direction
		}, (err) => {
			if (err) {
				// TODO: improve
				alert (err.message);
			}

			this.setState({ status: STATUS_NORMAL });
		})
	};

	render(){
		const { answer, canVote, chosenAnswer, canChooseAnswer, chooseAnswer } = this.props;
		const { body, creator } = answer;
		const _picSrc = answer.creator.pic
			? `/cdn/${answer.creator.pic}`
			: defaultImage;

		return (
			<Paper
				zDepth={3}
				className={styles.root}
			>
				{
					!!canChooseAnswer &&
					<RaisedButton
						style={{ float: 'right' }}
						label='Choose answer'
						onTouchTap={() => chooseAnswer(answer._id)}
						primary
					/>
				}

				<div
					className={styles.withVote}
				>
					<Vote
						disabled={canVote}
						vote={this.onVoteAction}
						checked={chosenAnswer}
					/>
					<img src={_picSrc} className={styles.pic}/>
					<div className={styles.details}>
						<p className={styles.body}> {body} </p>
						<span className={styles.creator}>by {creator.username} </span>
					</div>
				</div>

			</Paper>
		)
	}
}

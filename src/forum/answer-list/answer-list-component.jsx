/**
 * Created by metsys on 11/13/16.
 */
'use strict';

import _ from 'lodash';
import React, { PropTypes } from 'react';
import styles from './answer-list.scss';
import Answer from '../answer';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import CircularProgress from 'material-ui/CircularProgress';
import Paper from 'material-ui/Paper';

const STATUS_LOADING = 'status::loading';
const STATUS_NORMAL = 'status::normal';

export default class AnswerList extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			answerText: '',
			status: STATUS_NORMAL
		}
	}

	static propTypes = {
		readOnly: PropTypes.bool,

		chosenAnswer: PropTypes.object,
		answers: PropTypes.arrayOf(PropTypes.object),

		canChooseAnswer: PropTypes.bool,
		chooseAnswer: PropTypes.func,

		canAnswer: PropTypes.bool,
		answer: PropTypes.func,

		hasMoreAnswers: PropTypes.bool,
		loadMoreAnswers: PropTypes.func,

		voteAnswer: PropTypes.func
	};

	static defaultProps = {
		answers: [],
		answer: () => {},
		chooseAnswer: () => {},
		voteAnswer: () => {}
	};

	onLoadMoreAnswers = () => {
		const { loadMoreAnswers } = this.props;
		// this.setState({ status: STATUS_LOADING });
		loadMoreAnswers(/*err => {
			if (err) {
				//TODO: improve
				alert(err.message)
			}

			this.setState({ status: STATUS_NORMAL });
		}*/)
	};

	getLoadMoreAnswersControl = () => {
		const { hasMoreAnswers } = this.props;
		const { status } = this.state;
		if (!hasMoreAnswers) return;

		return (
			<div className={styles.loadMore}>
				{
					status === STATUS_LOADING
						? <CircularProgress style={{ flex: 1 }} size={0.5}/>
						: <RaisedButton
								style={{ flex: 1 }}
								label='Load more'
								onTouchTap={this.onLoadMoreAnswers}
								primary
							/>
				}
			</div>
		)
	};

	getAnswerControl = () => {
		const { canAnswer, disabled, answer } = this.props;
		const { answerText } = this.state;
		if (!canAnswer) return;

		return (
			<Paper
				className={styles.answerControl}
				zDepth={2}
			>
				<h4>Answer</h4>
				<TextField
					className={styles.answerText}
					value={answerText}
					onChange={e => this.setState({ answerText: e.target.value })}
					hintText='Type your answer...'
					disabled={disabled}
					multiLine
					fullWidth
				/>
				{
					disabled
						? <CircularProgress size={0.5}/>
						: <RaisedButton
								label='Answer'
								disabled={!answerText}
								onTouchTap={() => {
									answer(answerText);
									this.setState({ answerText: '' })
								}}
								primary
							/>
				}
			</Paper>
		)
	};

	render(){
		const { chosenAnswer, answers, voteAnswer } = this.props;

		return (
			<div className={styles.root}>
				{
					!!chosenAnswer &&
					<Answer
						{ ...this.props }
						chosenAnswer={ true }
						answer={ chosenAnswer }
						canVote={ chosenAnswer.accountCanVote }
						vote={voteAnswer}
					/>
				}
				{
					answers.filter(a => !_.isEqual(a, chosenAnswer))
						.map((answer, ind) =>
							<Answer
								{ ...this.props }
								key={ ind }
								chosenAnswer={ false }
								answer={ answer }
								canVote={ answer.accountCanVote }
								vote={voteAnswer}
							/>
						)
				}
				{
					this.getLoadMoreAnswersControl()
				}
				{
					this.getAnswerControl()
				}
			</div>
		)
	}
}

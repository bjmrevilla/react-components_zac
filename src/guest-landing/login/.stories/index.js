import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Login from '../login.component.jsx';

storiesOf('guestLanding.login', module)
  .add('component', () => (
    <MuiThemeProvider>
      <Login />
    </MuiThemeProvider>
  ))
  .add('errorUsername', () => (
    <MuiThemeProvider>
      <Login
        username='SomeUsername'
        passwordErrorText='Password is required.'
        onClick={ action('Login Button clicked') }
        snackbarFlag={true}
      />
    </MuiThemeProvider>
  ))
  .add('errorPassword', () => (
    <MuiThemeProvider>
      <Login
        password='SomePassword'
        usernameErrorText='Username is required.'
        onClick={ action('Login Button clicked') }
        snackbarFlag={true}
      />
    </MuiThemeProvider>
  ))

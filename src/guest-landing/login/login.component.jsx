'use strict';

import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Snackbar from 'material-ui/Snackbar';

import styles from './login.scss';

const inlineStyles = {
  labelStyle: {
    color: 'whitesmoke'
  }
}

export default class Login extends React.Component {
  constructor() {
    super();

    this.state = {
      open: false,
      username: '',
      password: '',
      snackBarText: '',
      usernameErrorText: '',
      passwordErrorText: ''
    };
  }

  static propTypes = {
    usernameHint: React.PropTypes.string,
    passwordHint: React.PropTypes.string,
    onLoginTouchTap: React.PropTypes.func
  }

  static defaultProps = {
    usernameHint: 'Username',
    passwordHint: 'Password'
  }

  handleRequestClose = () => { this.setState({ open: false }); }

  onUsernameChange = (e) => {
    this.setState({ username: e.target.value });
    this.setState({ usernameErrorText: '' });
  }

  onPassChange = (e) => {
    this.setState({ password: e.target.value });
    this.setState({ passwordErrorText: '' });
  }

  handleOnClick = (e) => {
    e.preventDefault();

    this.setState({ open: true });

    if (this.handleError()) {
      this.setState({ snackBarText: 'Logging in...' });

      this.props.onLoginTouchTap(this.state.username, this.state.password,
        (err) => {
          if (err) {
            this.setState({snackBarText: 'Username/password not found.'});
          }
        }
      );
    } else {
      this.setState({ snackBarText: 'Fill out the required fields.' });
    }
  }

  handleError = (username, password) => {
    let usernameIsValid = this.isValid(this.state.username);
    let passwordIsValid = this.isValid(this.state.password);
    let usernameErrorText = (usernameIsValid)
      ? ''
      : 'Username is required.';
    let passwordErrorText = (passwordIsValid)
      ? ''
      : 'Password is required.';

    this.setState({
      usernameErrorText: usernameErrorText,
      passwordErrorText: passwordErrorText
    });

    return (usernameIsValid && passwordIsValid);
  }

  isValid = (field) => {
    if (!field) return false;

    // check for whitespace-only fields
    if (/^\s*$/.test(field)) return false;

    return true;
  }

  render = () => {
    return (
      <form className={styles.root} onSubmit={this.handleOnClick}>
        <TextField
          hintText={this.props.usernameHint}
          onChange={this.onUsernameChange}
          value={this.state.username}
          errorText={this.state.usernameErrorText}
          floatingLabelText={this.props.usernameHint}
          inputStyle={inlineStyles.labelStyle}
          hintStyle={inlineStyles.labelStyle}
          floatingLabelStyle={inlineStyles.labelStyle}
          /><br />
        <TextField
          hintText={this.props.passwordHint}
          onChange={this.onPassChange}
          value={this.state.password}
          errorText={this.state.passwordErrorText}
          floatingLabelText={this.props.passwordHint}
          inputStyle={inlineStyles.labelStyle}
          hintStyle={inlineStyles.labelStyle}
          floatingLabelStyle={inlineStyles.labelStyle}
          type='password'
          /><br />
        <RaisedButton
          label='Login'
          className={styles.button}
          fullWidth
          primary
          type='submit'
          />
        <Snackbar
          open={this.state.open}
          message={this.state.snackBarText}
          autoHideDuration={4000}
          onRequestClose={this.handleRequestClose}
          />
      </form>
    );
  }
}

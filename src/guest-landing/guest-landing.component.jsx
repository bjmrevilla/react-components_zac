'use strict';

import React from 'react';
import styles from './guest-landing.scss';

export default class GuestLanding extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {}

  render = () => {
    return (
      <div className={styles.root}>
        {this.props.children}
      </div>
    );
  }
}
'use strict';

import React from 'react';
import styles from './login-page.scss';
import Login from '../login';

export default class LoginPage extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {};

  getPics = () => {
    return (
      <div className={styles.pics}>
        <div className={styles.adb} />
        <div className={styles.dswd} />
        <div className={styles.kalahi} />
      </div>
    );
  };

  render = () => {
    return (
      <div className={styles.root}>
        <div className={styles.textLogo} />
				<Login
					{...this.props}
				/>
        {this.getPics()}
        <span className={styles.text}>
          The grant which financed KALAHI-CIDSS NCDPP eLEARNING was received under the Japan Fund for Poverty Reduction which is financed by the Government of Japan.
        </span>
      </div>
    );
  }
}

import React from 'react';
import {storiesOf, action} from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import GuestLanding from '../guest-landing.component.jsx';
import LoginPage from '../login-page/login-page.component.jsx';
import Login from '../login/login.component.jsx';

require('../login/.stories');

storiesOf('guest-landing', module)
  .add('component', () => (
    <MuiThemeProvider>
      <GuestLanding>
        <LoginPage>
          <Login
            onLoginTouchTap={action('Login')} />
        </LoginPage>
      </GuestLanding>
    </MuiThemeProvider>
  ));
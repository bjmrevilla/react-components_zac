'use strict';

import React from 'react';
import styles from './row.scss';
import { TableRow, TableRowColumn } from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';

export default class Row extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isActionsOpen: false,
      anchorEl: null
    };
  }

  static propTypes = {
    headers: React.PropTypes.arrayOf(React.PropTypes.shape({
      label: React.PropTypes.string,
      key: React.PropTypes.string,
      format: React.PropTypes.func
    })),
    rowObject: React.PropTypes.object,
    actions: React.PropTypes.arrayOf(React.PropTypes.shape({
      label: React.PropTypes.string,
      fn: React.PropTypes.func
    }))
  };

  getColumns = () => {
    let row = this.props.headers.map(({ format = a => a, key }, ind) => {
      return (
        <TableRowColumn key={ind}>
          {format(this.props.rowObject[key])}
        </TableRowColumn>
      );
    });

    if (this.props.actions) {
      row.push(this.getActionsButton());
    }

    return row;
  };

  getActionsButton = () => {
    return (
      <TableRowColumn key={-1} className={styles.actionsContainer}>
        <RaisedButton
          className={styles.actionsButton}
          onTouchTap={this.handleTouchTap}
          label="Actions"
          primary />
        <Popover
          open={this.state.isActionsOpen}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
          targetOrigin={{ horizontal: 'left', vertical: 'top' }}
          onRequestClose={this.handleRequestClose}>
          <Menu>
            {this.getActions()}
          </Menu>
        </Popover>
      </TableRowColumn>
    );
  };

  getActions = () => {
    return this.props.actions.map((action, ind) => {
      let handleFn = () => { action.fn(this.props.rowObject) };

      return (
        <MenuItem
          key={ind}
					leftIcon={action.icon}
          primaryText={action.label}
          onTouchTap={handleFn} />
      );
    });
  };

  handleTouchTap = (event) => {
    event.preventDefault();

    this.setState({
      isActionsOpen: true,
      anchorEl: event.currentTarget,
    });
  };

  handleRequestClose = () => {
    this.setState({
      isActionsOpen: false,
    });
  };

  render = () => {
    return (
      <TableRow className={styles.root}>
        {this.getColumns()}
      </TableRow>
    );
  }
}

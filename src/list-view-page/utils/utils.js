'use strict';

import _ from 'lodash';

export const search = (data, filter) => {
  if (!filter) return [];

  let result = [];

  // worst case O(n^2) operation!
  for (let i in data) {
    let obj = data[i];

    for (let key in obj) {
      let stringified = '' + obj[key];

      stringified = stringified.toLowerCase();
      filter = filter.toLowerCase();

      if (stringified.indexOf(filter) >= 0) {
        result.push(obj);
        break;
      }
    }
  }

  return result;
};
export const sort = (data, key) => {
  data.sort((a, b) => {
    let first = (typeof a[key] === 'string')
      ? a[key].toLowerCase()
      : a[key];
    let second = (typeof b[key] === 'string')
      ? b[key].toLowerCase()
      : b[key];

    if (first < second) {
      return -1;
    }
    if (first > second) {
      return 1;
    }

    return 0;
  });
};

export const diff = (originalData, changedData, options, rootDiff = true) => {
	if (rootDiff) console.log(`orig data ${JSON.stringify(originalData, null, 2)}`);
	if (rootDiff) console.log(`changed data ${JSON.stringify(changedData, null, 2)}`);

	const { keysToExempt = [], keysToRemove = [] } = options || {};

	// Exempted from comparison
	const exempted = keysToExempt.concat(keysToRemove);

	return _.reduce(changedData, (res, val, key) => {
		// Exempted
		if (exempted.indexOf(key) !== -1) return res;

		// Newly added property
		if (!originalData.hasOwnProperty(key)) return _.set(res, key, val);

		// Original Data
		const origValue = originalData[key];

		// Date diff
		if (val instanceof Date && origValue instanceof Date) {
			return val.getTime() === origValue.getTime()
				? res
				: _.set(res, key, val);
		}

		// Recursive array diff
		if (Array.isArray(val)) {
			// Compare to original
			const changed = diffArray(origValue, val, options);
			return _.isEmpty(changed)
				? res
				: _.set(res, key, changed);
		}

		// Recursive object diff
		if (_.isPlainObject(val)) {
			const changed = diff(origValue, val, options, false);
			return _.isEmpty(changed)
				? res
				: _.set(res, key, changed);
		}

		// console.log(`${typeof val}::${val} vs ${typeof origValue}::${origValue} changed -> ${_.isEqual(val, origValue)}`);
		// Scalar diff
		return _.isEqual(val, origValue)
			? res
			: _.set(res, key, val);
	}, { });
};

function isArrayValueEqual (a, b, options){
	// Entities are objects
	if ([a, b].every(_.isPlainObject)) {
		const { compareUsingKeys = ['_id', 'id', 'name'] } = options || {};
		return !!compareUsingKeys.find(key => a.hasOwnProperty(key) && _.isEqual(a[key], b[key]))
	}

	// Entities are scalars
	return _.isEqual(a, b);
}

export const diffArray = (orig = [], changed = [], options) => {
	console.log(`orig arr ${JSON.stringify(orig, null, 2)}`);
	console.log(`changed arr ${JSON.stringify(changed, null, 2)}`);

	// Changes
	const changes = _.reduce(changed, (res, val) => {
		// Find orig data
		const origValue = orig.find(e => isArrayValueEqual(e, val, options));

		// Newly added data
		if (!origValue) return res.concat(val);

		// Recursive object diff
		if (_.isPlainObject(val)) {
			const changed = diff(origValue, val, null, false);
			return _.isEmpty(changed)
				? res
				: res.concat(val);
		}

		// Scalar diff
		return _.isEqual(val, origValue)
			? res
			: res.concat(val);
	}, []);

	// Deleted
	const deleted = _.differenceWith(orig, changed, (a, b) => isArrayValueEqual(a, b, options))
		// Add remove flags
		.map(value => {
			if (_.isPlainObject(value)) {
				return _.set(value, '__action', 'remove');
			}

			return {
				value,
				__action: 'remove'
			}
		});

	return changes.concat(deleted);
};

// export const diff2 = (originalData = {}, changedData = {}, keysToExempt = [], keysToRemove = []) => {
//   let exemptionFlag;
//   let removeKeysFlag;
//   let jsondiffpatch = require('jsondiffpatch');
//   let rawDiff;
//   // merge conflict, commented out to make sure
//   // export const diff = (originalData = {}, changedData = {}, keysToExempt = [], keysToRemove = []) => {
//   //   // Did not implement add or remove of un-nested key-value pair. Only in Array Of Objects. (Can add if needed)
//   //   // Currently implementing KeysToExempt and KeysToRemove
//   //   let diffFxn = require('deep-diff').diff;
//   //   let rawDiff = diffFxn(originalData, changedData);
//   //   let processedDiff = {};
//   //
//   //   let isExempted = (path) => {
//   //     keysToExempt.forEach(function(array) {
//   //       // Fast eliminations
//   //       if(path.length === array.length) {
//   //         for(let i = 0; i < path.length; i++) {
//   //           if(path[i] !== array[i]) {
//   //             return false;
//   //           }
//   //         }
//   //         return true;
//   //       }
//   //     }, this);
//   //   }
//
//   // Clone Data
//   let cloneOriginalData = JSON.parse(JSON.stringify(originalData));
//   let cloneChangedData = JSON.parse(JSON.stringify(changedData));
//
//   (keysToExempt.length === 0 ) ? exemptionFlag = true : exemptionFlag = false;
//   (keysToRemove.length === 0 ) ? removeKeysFlag = true : removeKeysFlag = false;
//
//   let removeKeys = (data) => {
//     let ptr = data;
//     let failFlag = false;
//     keysToRemove.forEach(function(path) {
//       for(let i = 0; i < path.length - 1; i++) {
//         if(ptr.hasOwnProperty(path[i])) {
//           ptr = ptr[path[i]];
//         } else {
//           failFlag = true;
//           break;
//         }
//       }
//
//       if(failFlag !== true) {
//         delete ptr[path[path.length - 1]];
//       }
//     }, this);
//   };
//
//   let isExempted = (stringPath, pathLength) => {
//     keysToExempt.forEach(function(path) {
//       if(pathLength === path.length) {
//         if(stringPath === path.join('.')){
//           return true;
//         }
//       }
//     }, this);
//
//     return false;
//   };
//
//   // Remove Keys
//   if(removeKeysFlag) {
//     removeKeys(cloneOriginalData);
//     removeKeys(cloneChangedData);
//   }
//
//   // Compute differences.
//   rawDiff = jsondiffpatch.diff(cloneOriginalData, cloneChangedData);
//
//   // Editing fxns.
//   let recursiveEdit = (object) => {
//     for(let key in object) {
//     	if (!object.hasOwnProperty(key)) continue;
//       if( Object.prototype.toString.call( object[key] ) === '[object Array]' ) {
//         let obj = {};
//         if(object[key].length === 1) { // Add a value
//           obj[key] = object[key][0];
//           obj['__action'] = 'add';
//           object[key] = obj;
//         } else if(object[key].length === 2) { // Modified a value
//           object[key] = object[key][1];
//         } else if(object[key].length === 3) { // Deleted a value
//           obj[key] = object[key][0];
//           obj['__action'] = 'remove';
//           object[key] = obj;
//         }
//         recursiveEdit(object[key]);
//       } else if( Object.prototype.toString.call( object[key] ) === '[object Object]' ) {
//         recursiveEdit(object[key]);
//       }
//     }
//   };
//
//   let stringContainsNumber = (string) => {
//     return /\d/.test(string);
//   };
//
//   let array = [];
//   let prevKey;
//   let recursiveArrayEdit = (object) => {
//     let arrayFlag = false;
//     Object.keys(object).forEach(function(key) {
//       if( Object.prototype.toString.call( object[key] ) === '[object Object]' ) {
//         if(stringContainsNumber(key)) {
//           if(typeof prevKey == 'undefined') {
//             array.push(object[key]);
//           } else if(prevKey != key) {
//             array.push(object[key]);
//           }
//           prevKey = key
//         }
//         recursiveArrayEdit(object[key]);
//       }
//     });
//   };
//
//   prevKey = undefined;
//   let recursiveFind = (object) => {
//     Object.keys(object).forEach(function(key) {
//       if( Object.prototype.toString.call( object[key] ) === '[object Object]' ) {
//         if(object[key].hasOwnProperty('_t')) {
//           object[key] = array;
//         } else {
//           recursiveFind(object[key])
//         }
//       }
//     });
//   };
//
//   recursiveEdit(rawDiff);
//   recursiveArrayEdit(rawDiff);
//   recursiveFind(rawDiff);
//
//   return rawDiff;
// };


import _ from 'lodash';
import React from 'react';
import TabularView from './tabular-view/tabular-view.component.jsx';
import CarouselView from './carousel-view/carousel-view.component.jsx';
import FullPageView from './full-page-view/full-page-view.component.jsx';
import styles from './list-view-page.scss';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';

export default class ListViewPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      searchText: '',
      viewModeIndex: 0
    };
  }

  static propTypes = {
    data: React.PropTypes.arrayOf(React.PropTypes.object),
    title: React.PropTypes.string,
    viewModes: React.PropTypes.arrayOf(React.PropTypes.oneOf([
      'tabular',
      'carousel',
      'fullpage'
    ])),
    onAddNewTouchTap: React.PropTypes.func,
    enableSearch: React.PropTypes.bool,
    groupByKey: React.PropTypes.shape({
      path: React.PropTypes.arrayOf(React.PropTypes.string),
      title: React.PropTypes.string,
      showTitle: React.PropTypes.bool
    }),
    showFloatingAddNew: React.PropTypes.bool
  };

  static defaultProps = {
		showTitle: true,
    viewModes: ['tabular'],
    enableSearch: true,
    showFloatingAddNew: false
  };

  handleChangeViewMode = (event, index, value) => {
    this.setState({ viewModeIndex: value });
  };

  handleType = (e) => {
  	const searchText = e.target.value;
  	const { searchData } = this.props;

		if (_.isFunction(searchData)) searchData(searchText);
    this.setState({ searchText });
  };

  getControls = () => {
  	const { showTitle, title, viewModes } = this.props;

    let search = (this.props.enableSearch)
      ? <span className={styles.searchField}>
        <TextField
          value={this.state.searchText}
          onChange={this.handleType}
          hintText='Search' />
      </span>
      : '';
    let addNew = (this.props.onAddNewTouchTap)
      ? <RaisedButton
        className={styles.addNewButton}
        label='Add New'
        onTouchTap={this.props.onAddNewTouchTap}
        primary={true} />
      : '';

    return (
      <div className={styles.controls}>
				{
					showTitle && <span className={styles.title}> {title} </span>
				}
				{
					viewModes.length > 1 &&
					<SelectField
						className={styles.viewModePicker}
						value={this.state.viewModeIndex}
						onChange={this.handleChangeViewMode}>
						{this.getViewModes()}
					</SelectField>
				}
        {search}
        {addNew}
      </div>
    );
  };

  getViewModes = () => {
    return this.props.viewModes.map((viewMode, index) => {
      return <MenuItem key={index} primaryText={viewMode} value={index} />;
    });
  };

  groupByKey = (data, path) => {
    let walkPathArray = (item, path) => {
      let tempValue = item;

      path.forEach((pathKey) => {
        tempValue = tempValue[pathKey];
      });

      return tempValue;
    };
    let result = {};

    data.forEach((item) => {
      let value = (typeof path === 'string')
        ? item[path]
        : walkPathArray(item, path);

      if (!result.hasOwnProperty(value)) result[value] = [];

      result[value].push(item);
    });

    return result;
  };

  getChild = () => {
    let viewMode = this.props.viewModes[this.state.viewModeIndex];
    let searchString = (this.props.enableSearch)
      ? this.state.searchText
      : '';

    let groups = (this.props.data && this.props.groupByKey)
      ? this.groupByKey(this.props.data, this.props.groupByKey.path)
      : { data: this.props.data };

    return Object.keys(groups).map((key, ind) => {
      let group = groups[key];
      let beautifiedKey = key.charAt(0).toUpperCase() + key.slice(1) + 's';
      let label = (!this.props.groupByKey)
        ? ''
        : (this.props.groupByKey.showTitle)
          ? this.props.groupByKey.title
          : beautifiedKey;

      if (viewMode === 'tabular') {
        return (
          <div key={ind} className={styles.group}>
            <TabularView
              headers={this.props.headers}
              data={group}
              label={label}
              searchString={searchString}
              itemsToViewList={this.props.itemsToViewList}
              hasNextPage={this.props.hasNextPage}
              nextPage={this.props.nextPage}
              search={this.props.search}
              getActions={this.props.getActions}
              actions={this.props.actions} />
          </div>
        );
      }
      if (viewMode === 'carousel') {
        return (
          <div key={ind} className={styles.group}>
            <CarouselView
              data={group}
              label={label}
              title={this.props.thumbTitle}
              subtitle={this.props.subtitle}
              image={this.props.image}
              searchString={searchString}
              hasNextPage={this.props.hasNextPage}
              nextPage={this.props.nextPage}
              search={this.props.search}
              getActions={this.props.getActions}
              actions={this.props.actions} />
          </div>
        );
      }
      if (viewMode === 'fullpage') {
        return (
          <div key={ind} className={styles.group}>
            <FullPageView
              data={group}
              label={label}
              title={this.props.thumbTitle}
              subtitle={this.props.subtitle}
              image={this.props.image}
              searchString={searchString}
              itemsToViewList={this.props.itemsToViewList}
              hasNextPage={this.props.hasNextPage}
              nextPage={this.props.nextPage}
              search={this.props.search}
              getActions={this.props.getActions}
              actions={this.props.actions} />
          </div>
        );
      }

      return '';
    });
  };

  getFloatingAddNewButton = () => {
    return (this.props.showFloatingAddNew && this.props.onAddNewTouchTap)
      ? <FloatingActionButton
        className={styles.fab}
        onTouchTap={this.props.onAddNewTouchTap}>
        <ContentAdd />
      </FloatingActionButton>
      : '';
  };

  render = () => {
    return (
      <div className={styles.root}>
        {this.getControls()}
        {this.getChild()}
        {this.getFloatingAddNewButton()}
      </div>
    );
  }
}

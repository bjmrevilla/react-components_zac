import React, { PropTypes } from 'react'
import Thumbnail from '../thumbnail/thumbnail.component.jsx';
import { search, sort } from '../utils/utils.js';
import TextField from 'material-ui/TextField'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'
import FloatingActionButton from 'material-ui/FloatingActionButton';
import Left from 'material-ui/svg-icons/navigation/chevron-left'
import Right from 'material-ui/svg-icons/navigation/chevron-right'
import styles from './carousel-view.scss'

export default class CarouselView extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      currentSortIndex: -1,
      searchText: '',
      isScrollable: false
    };
  }

  static propTypes = {
    data: React.PropTypes.arrayOf(React.PropTypes.object),
    title: React.PropTypes.object, //ObjectKey
    subtitle: React.PropTypes.object,
    image: React.PropTypes.object,
    defaultImage: React.PropTypes.string,
    showSearch: React.PropTypes.bool,
    searchString: React.PropTypes.string,
    actions: React.PropTypes.arrayOf(React.PropTypes.shape({
      label: React.PropTypes.string,
      fn: React.PropTypes.func
    })),
    emphasizeIndices: React.PropTypes.arrayOf(React.PropTypes.number)
  };

  static defaultProps = {
    showSearch: false
  };

  componentDidMount = () => {
    this.checkIfScrollable();
  };

  checkIfScrollable = () => {
    let isScrollable = (this.refs.row.scrollWidth > this.refs.row.clientWidth);

    this.setState({
      isScrollable: isScrollable
    });
  };

  handleType = (e) => {
    this.setState({ searchText: e.target.value });
  };

  handleChangeSort = (event, index, value) => {
    this.setState({
      currentSortIndex: value
    });
  };

  onLeft = () => {
    this.refs.row.scrollLeft -= 100
  };

  onRight = () => {
    this.refs.row.scrollLeft += 100
  };

  getControls = () => {
    let search = (this.props.showSearch)
      ? <TextField
        value={this.state.searchText}
        onChange={this.handleType}
        floatingLabelText='Search' />
      : '';

    return (
      <div className={styles.controls}>
        <div className={styles.search}>
          {search}
        </div>
        {this.getSortSelect()}
      </div>
    );
  };

  getSortSelect = () => {
    return (this.props.data === null
      || this.props.data === undefined
      || this.props.data.length === 0)
      ? ''
      : <SelectField
        value={this.state.currentSortIndex}
        floatingLabelText='Sort by'
        onChange={this.handleChangeSort}>
        {this.getKeysToSortBy()}
      </SelectField>;
  }

  getKeysToSortBy = () => {
    let keyMenuItems = Object.keys(this.props.data[0]).map((key, index) => {
      return <MenuItem
        key={index}
        value={index}
        primaryText={key} />;
    });
    let unsortedMenuItem = [
      <MenuItem key={-1} value={-1} primaryText='unsorted' />
    ];

    return unsortedMenuItem.concat(keyMenuItems);
  };

  getThumbnails = () => {
    if (this.props.data === null
      || this.props.data === undefined
      || this.props.data.length === 0) {
      return (
        <span className={styles.noResultsWarning}>
          No results.
        </span>
      );
    }

    // clone so props don't get mutated
    let cloneData = this.props.data.slice(0);

    // search first...
    let searchText = (this.props.searchString
      && !/^\s*$/.test(this.props.searchString))
      ? this.props.searchString
      : this.state.searchText;
    let shouldSearch = !/^\s*$/.test(searchText);
    let searchArr = (shouldSearch)
      ? search(cloneData, searchText)
      : cloneData;

    if (searchArr.length === 0) {
      return (
        <span className={styles.noResultsWarning}>
          No results.
        </span>
      );
    }

    // ...then sort
    let sortArr = searchArr; // initialize
    if (this.state.currentSortIndex !== -1) {
      sort(sortArr, Object.keys(sortArr[0])[this.state.currentSortIndex]);
    }

    let data = sortArr;
    return data.map((item, index) => {
      return (
        <div key={index} className={styles.thumbnailContainer}>
          <Thumbnail
            data={item}
            title={this.props.title}
            subtitle={this.props.subtitle}
            image={this.props.image}
            hideImage={this.props.hideImage}
            actionsPosition={this.props.actionsPosition}
            emphasize={this.props.emphasize}
            defaultImage={this.props.defaultImage}
            getActions={this.props.getActions}
            actions={this.props.actions} />
        </div>
      );
    });
  };

  render() {
    const { onUpdateSearch } = this;
    const { children, search } = this.props;
    const { sort } = this.state;
    let rowJustify = (this.state.isScrollable)
      ? 'flex-start'
      : 'center';

    return (
      <div className={styles.root}>
        <span className={styles.label}>
          {this.props.label}
        </span>
        {this.getControls()}
        <div className={styles.rowContainer}>
          <FloatingActionButton onClick={this.onLeft} className={styles.rowLeft}>
            <Left />
          </FloatingActionButton>
          <FloatingActionButton onClick={this.onRight} className={styles.rowRight}>
            <Right />
          </FloatingActionButton>
          <div
            ref='row'
            className={styles.row}
            style={{ justifyContent: rowJustify }}>
            {this.getThumbnails()}
          </div>
        </div>
      </div>
    )
  }
}

import React, { PropTypes } from 'react'
import R from 'ramda'
import Chip from 'material-ui/Chip'
import Paper from 'material-ui/Paper'
import IconButton from 'material-ui/IconButton'
import RaisedButton from 'material-ui/RaisedButton'
import ContentAdd from 'material-ui/svg-icons/content/add'
import Dialog from 'material-ui/Dialog';

import Selection from '../selection/selection.component.jsx';
import styles from './tags-list.scss'

export default class TagsList extends React.Component {
  constructor (props) {
    super(props);
		this.state = {
			isDialogOpen: false
		}
  }

  static propTypes = {
		title: PropTypes.string,
		description: PropTypes.string,
		tip: PropTypes.string,
		label: PropTypes.string,
		showTitle: PropTypes.bool,
		showDescription: PropTypes.bool,
		showTip: PropTypes.bool,

		hideFloatingAddButton: PropTypes.bool,
		hideAddButton: PropTypes.bool,

    tags: PropTypes.arrayOf(PropTypes.object),

    disableAddNew: PropTypes.bool,
    disableRemove: PropTypes.bool,

    readOnly: PropTypes.bool,

		add: PropTypes.func
  };

  static defaultProps = {
  	tags: [],
    showTitle: true,
    showDescription: true,
    showTip: true,
    disableAddNew: true,
    disableRemove: false,
    hideFloatingAddButton: false,
    hideAddButton: false,
    readOnly: false
  };

  handleOpenDialog = () => {
    this.setState({ isDialogOpen: true });
  };
  handleCloseDialog = () => {
    this.setState({ isDialogOpen: false })
  };

  render () {
    const { title, description, tip, label, tags, sourceTags, add, selectionProps } = this.props;
    const { showTitle, showDescription, showTip } = this.props;
    const { hideAddButton, hideFloatingAddButton } = this.props;
    const { readOnly } = this.props;

    let { disableAddNew, disableRemove } = this.props;

    if (readOnly) {
      disableAddNew = true;
      disableRemove = true;
    }

    const onFinish = () => {
      this.handleCloseDialog();
      this.props.onFinish();
    };

    const dialogStyle = {
      width: '500px'
    };

    return (
      <div className={styles.root}>
        <Dialog
          open={this.state.isDialogOpen}
          contentStyle={dialogStyle}
          onRequestClose={onFinish}
        >
          <Selection
            {...selectionProps}
            sourceItems={sourceTags}
            onFinish={onFinish}
          />
        </Dialog>
        {showTitle
          ? <span className={styles.title}>{title}</span>
          : ''
        }
        {showDescription
          ? <div className={styles.desc}>{description}</div>
          : ''
        }
        {showTip
          ? <div className={styles.tip}>{tip}</div>
          : ''
        }
        <div className={styles.chips}>
          <div className={styles.label}>{label}</div>
          {tags.map(({key, name}, ind) => <Chip key={ind} className={styles.chip}>{key ? key : name}</Chip>)}
          {disableAddNew || hideAddButton
            ? ''
            : <IconButton onClick={add ? add : this.handleOpenDialog}><ContentAdd /></IconButton>
          }
        </div>
      </div>
    )
  }
}

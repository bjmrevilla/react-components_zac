import React from 'react'
import R from 'ramda'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import TabularView from '../tabular-view/tabular-view.component.jsx';
import ListViewPage from '..';
import Selection from '../selection/selection.component.jsx';
import TagsList from '../tags-list/';
import { compose, withState } from 'recompose'
import {
  headers,
  data,
  thumbTitle,
  thumbSubtitle,
  thumbImage,
  groupByName,
  groupByNestedObject,
  actions} from './fixtures';

require('./tabular-view.js');
require('./thumbnail.js');
require('./full-page-view.js');
require('./carousel-view.js');

let multipleData = [];
for (let i = 0; i < 5; i++) {
  multipleData = multipleData.concat(data);
}

const Controlled = compose(
   withState('sourceTags', 'setSourceTags', [{key: 'test'}]),
   withState('selectedItems', 'setSelectedItems', [])
)(({sourceTags, setSourceTags, selectedItems, setSelectedItems}) => (
  <MuiThemeProvider>
    <TagsList
      tags={[{key: 'name'}]}
      sourceTags={sourceTags}
      disableAddNew={false}
      hideAddButton={false}
      selectionProps={{
        title: 'Title',
        selectedItems: selectedItems,
        onSelectedItemsChanged: (key) => {
          if (R.contains({key}, selectedItems)) {
            setSelectedItems(R.remove(R.indexOf({key}, selectedItems), 1, selectedItems))
          } else {
            setSelectedItems(selectedItems.concat({key}))
          }
        },
        addFunction: (tag) => {
          setSourceTags(sourceTags.concat({key: tag}))
        }
      }}
    />
  </MuiThemeProvider>
))

storiesOf('list-view-page', module)
  .add('component', () => (
    <MuiThemeProvider>
      <ListViewPage
        viewModes={['tabular','carousel','fullpage']}
        data={multipleData}
        title='Fruits'
        headers={headers}
        thumbTitle={thumbTitle}
        subtitle={thumbSubtitle}
        image={thumbImage}
        showFloatingAddNew={true}
        actions={actions} />
    </MuiThemeProvider>
  ))
  .add('tags-list', () => <Controlled />);
storiesOf('list-view-page', module)
  .add('no data', () => (
    <MuiThemeProvider>
      <ListViewPage
        viewModes={['tabular','carousel','fullpage']}
        title='Fruits'
        headers={headers}
        thumbTitle={thumbTitle}
        subtitle={thumbSubtitle}
        image={thumbImage}
        showFloatingAddNew={true}
        actions={actions} />
    </MuiThemeProvider>
  ));
storiesOf('list-view-page', module)
  .add('group by name', () => (
    <MuiThemeProvider>
      <ListViewPage
        viewModes={['tabular','carousel','fullpage']}
        data={multipleData}
        title='Fruits'
        headers={headers}
        thumbTitle={thumbTitle}
        subtitle={thumbSubtitle}
        image={thumbImage}
        groupByKey={groupByName}
        showFloatingAddNew={true}
        actions={actions} />
    </MuiThemeProvider>
  ));
storiesOf('list-view-page', module)
  .add('group by path', () => (
    <MuiThemeProvider>
      <ListViewPage
        viewModes={['tabular','carousel','fullpage']}
        data={multipleData}
        title='Fruits'
        headers={headers}
        thumbTitle={thumbTitle}
        subtitle={thumbSubtitle}
        image={thumbImage}
        showFloatingAddNew={true}
        groupByKey={groupByNestedObject}
        actions={actions} />
    </MuiThemeProvider>
  ));

export const headers = [
  {
    label: 'All Lowercase',
    key: 'name',
    format: (x) => x.toLowerCase()
  },
  {
    label: 'All Uppercase',
    key: 'fruitNo',
    format: (x) => x.toUpperCase()
  },
  {
    label: 'header 3',
    key: 'hiddenNo',
    format: (x) => x
  }
];

export const groupByName = {
  path: 'name'
};

export const groupByNestedObject = {
  path: ['path', 'to', 'key']
};

export const data = [
  {
    name: 'apple',
    fruitNo: 'fruit 1',
    hiddenNo: '3',
    path: {to: {key: 1}},
    pic: 'https://staticdelivery.nexusmods.com/mods/110/images/74627-0-1459502036.jpg'
  },
  {
    name: 'banana',
    fruitNo: 'fruit 3',
    hiddenNo: '1',
    path: {to: {key: 3}},
    pic: 'http://www.bbcgoodfood.com/sites/default/files/glossary/banana-crop.jpg'
  },
  {
    name: 'orange',
    fruitNo: 'fruit 2',
    hiddenNo: '2',
    path: {to: {key: 2}},
    pic: 'https://a2ua.com/orange/orange-023.jpg'
  }
];

export const actions = [
  {
    label: 'action 1',
    fn: () => {}
  },
  {
    label: 'action 2',
    fn: () => {}
  }
];

const tagsData = {
  title: 'Sample Book',
  subtitle: 'Book Subtitle',
  image: 'http://kalahi-web.herokuapp.com/cdn/577e87a0ea22591100c94601.png'
}
const title = 'title'
const description = 'description for tag list'
const tip = 'this is a tip'
const label = 'label:'
const tags = [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
]

export const tagList = { title, description, tip, label, tags }

export const thumbTitle = {
  label: 'Title',
  key: 'name',
  format: (x) => x
};

export const thumbSubtitle = {
  label: 'Subtitle',
  key: 'fruitNo',
  format: (x) => x
};

export const thumbImage = {
  label: 'Image',
  key: 'pic',
  format: (x) => x
};

export const extraLongData = {
  name: 'ksljfslkdsjflsdkfjsldfkjsdlfkjsldkfjsldkfj',
  fruitNo: 'asldjasldkasjdlaskjdlakjqioeuqwoidjlkcnalkjsd',
  hiddenNo: '3',
  path: { to: { key: 1 } },
  pic: 'https://staticdelivery.nexusmods.com/mods/110/images/74627-0-1459502036.jpg'
};

export const utilsTestDataNad = {
  test1Original: {
    foo: 'bar',
    bar: 'foo'
  },
  test1Changed: {
    foo: 'bar',
    bar: 'bar'
  },
  test2Original: {
    foo: 'bar',
    bar: [{
      _id: 'id',
      name: 'name1'
    }, 
    {
      _id: 'id1',
      name: 'name2'
    }]
  },
  test2Changed: {
    foo: 'bar',
    bar: [{
      _id: 'id',
      name: 'name1'
    }]
  },
  test3Original: {
    foo: 'bar',
    bar: [{
      _id: 'id',
      name: 'name1'
    },
    {
      _id: 'id1',
      name: 'name2'
    }]
  },
  test3Changed: {
    foo: 'bar',
    bar: [{
      _id: 'id',
      name: 'namechanged'
    },
    {
      _id: 'id1',
      name: 'name2'
    }]
  },
  test4Original: {
    foo: 'bar',
    bar: [{
      _id: 'id',
      name: 'name1'
    },
    {
      _id: 'id1',
      name: 'name2'
    }]
  },
  test4Changed: {
    foo: 'bar',
    bar: [{
      _id: 'id',
      name: 'name1'
    },
    {
      _id: 'id1',
      name: 'name2'
    },
    {
      _id: 'id3',
      name: 'name3'
    }]
  },
  test5Original: {
    foo: 'bar',
    bar: ['foo', 'bar']
  },
  test5Changed: {
    foo: 'bar',
    bar: ['foo']
  },
  test6Original: {
    foo: 'bar',
    bar: ['foo', 'bar', 'ber' ]
  },
  test6Changed: {
    foo: 'bar',
    bar: ['foo', 'ber', 'sic']
  }, 
}

// export const utilsTestData = {
// 	testData1: {
// 		foo: 'foo',
// 		bar: 'bar',
// 		fuu: 'fuu',
//     suchNestedObject: {
//       such: {
//         nested: 'nested',
//         object: 'object'
//       }, 
//       nested: {
//         such: 'such',
//         object: 'object'
//       },
//       object: {
//         such: 'nested',
//         nested: 'nested'
//       }
//     },
// 		war: [{
// 			_id: 'id',
// 			age: 13,
// 			name: 'name1'
// 		}, 
// 		{
// 			_id: 'id1',
// 			age: 14,
// 			name: 'name2'
// 		},
// 		{
// 			_id: 'id2',
// 			age: 15,
// 			name: 'name3'
// 		}],
//     mou: [{
//       id: 'id',
//       age: '1',
//       name: 'namae'
//     },
//     {
//       id: 'id2',
//       age: '2',
//       name: 'namae2'
//     },
//     {
//       id: 'id3',
//       age: '3',
//       name: 'namae3'
//     }],
//     yuu: [0 , 1, 2, 'wow', 'such', 'array']
// 	}, 
// 	testData2: {
// 		foo: 'moo',
// 		bar: 'mar',
// 		fuu: 'fuu',
//     suchNestedObject: {
//       such: {
//         nested: 'nested',
//         object: 'object'
//       }, 
//       nested: {
//         such: 'such',
//         object: 'object'
//       },
//       object: {
//         such: 'wow',
//         nested: 'doge'
//       },
//       doge: {
//         such: 'such',
//         object: 'object',
//         nested: 'nested'
//       },
//     },
// 		war: [{
// 			_id: 'id',
// 			age: 20,
// 			name: 'name1'
// 		},
// 		{
// 			_id: 'id2',
// 			age: 15,
// 			name: 'name3'
// 		},
// 		{
// 			_id: 'id3',
// 			age: 123,
// 			name: 'meow'
// 		}, 
//     {
// 			_id: 'id4',
// 			age: 124,
// 			name: 'meowee'
// 		}],
//     mou: [
//     {
//       id: 'id',
//       age: '1',
//       name: 'namae'
//     }],
//     yuu: [0, 3, 2]
// 	}, 
// }

export const utilsTestData = {
  testData1: {
    wow: 'tot',
    wat: {
      wit: {
        wot: {
          waw: 'yay',
          wiw: 'yow'
        }, 
        wew: {
          waw: 'yay',
          wiw: 'yow'
        }
      }, wot: {
          waw: 'yay',
          wiw: 'yow'
        }
    },
    foo: {
      poo: 'poo',
      bar: {
        maa: [{
          muu: 'yaa',
          kuu: 'ksu'
        }, {
          muu: 'muu',
          kuu: 'kuu'
        }, {
          muu: 'mau',
          kuu: 'waa'
        }],
        baa: {
          waa: 'yaa',
          naa: 'yaa'
        }    
      }
    }, 
    bar: {
      puu: 'puu',
      muu: 'muu'
    }
  },
  testData2: {
    wow: 'yay',
    wat: {
      wit: {
        wot: {
          waw: 'yay',
          wiw: 'yow'
        }, 
        wew: {
          waw: 'yew',
          wiw: 'yow'
        }
      }, 
      wot: {
        waw: 'yay',
        wiw: 'yat'
      }, 
      wat: {
        waw: 'yay',
        wiw: 'yat'
      }
    },
    foo: {
      poo: 'poo',
      bar: {
        maa: [{
          muu: 'yaa',
          kuu: 'kuu'
        }, {
          muu: 'muu',
          kuu: 'kuu'
        }, {
          muu: 'muu',
          kuu: 'waa'
        }, {
          muu: 'muu',
          kuu: 'waa'
        }],
        baa: {
          waa: 'waa',
          naa: 'yaa'
        }    
      }
    }, 
    bar: {
      puu: 'puu',
      muu: 'muu'
    }
  }
} 
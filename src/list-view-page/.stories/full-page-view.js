import React from 'react';
import {storiesOf, action} from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import FullPageView from '../full-page-view/full-page-view.component.jsx';
import {
  data,
  thumbTitle,
  thumbSubtitle,
  thumbImage,
  actions,
  utilsTestData} from './fixtures.js';


let multipleData = [];
for (let i = 0; i < 5; i++) {
  multipleData = multipleData.concat(data);
}

storiesOf('list-view-page', module)
  .add('full-page-view', () => (
    <MuiThemeProvider>
      <FullPageView
        data={multipleData}
        title={thumbTitle}
        subtitle={thumbSubtitle}
        image={thumbImage}
        itemsToViewList={[1,2,3,4,100]}
        showSearch={true}
        actions={actions}
        utilsTestData={utilsTestData} />
    </MuiThemeProvider>
  ));
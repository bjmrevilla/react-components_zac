import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import CarouselView from '../carousel-view/carousel-view.component.jsx';
import Thumbnail from '../thumbnail/thumbnail.component.jsx';
import {
  data,
  thumbTitle,
  thumbSubtitle,
  thumbImage,
  actions
} from './fixtures.js';

let multipleData = [];
for (let i = 0; i < 5; i++) {
  multipleData = multipleData.concat(data);
}

storiesOf('list-view-page', module)
  .add('carousel-view', () => (
    <MuiThemeProvider>
      <CarouselView
        data={multipleData}
        title={thumbTitle}
        subtitle={thumbSubtitle}
        image={thumbImage}
        actions={actions} />
    </MuiThemeProvider>
  ));
import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import TabularView from '../tabular-view/tabular-view.component.jsx';
import {headers, data, actions} from './fixtures';

storiesOf('list-view-page', module)
  .add('tabular-view', () => (
    <MuiThemeProvider>
      <TabularView
        headers={headers}
        data={data}
        actions={actions} />
    </MuiThemeProvider>
  ))

import React from 'react';
import {storiesOf, action} from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Thumbnail from '../thumbnail/thumbnail.component.jsx';
import {
  extraLongData,
  data,
  thumbTitle,
  thumbSubtitle,
  thumbImage,
  actions} from './fixtures.js';

storiesOf('list-view-page', module)
  .add('thumbnail', () => (
    <MuiThemeProvider>
      <Thumbnail
        data={data[0]}
        title={thumbTitle}
        subtitle={thumbSubtitle}
        image={thumbImage}
        actions={actions} />
    </MuiThemeProvider>
  ));
storiesOf('list-view-page', module)
  .add('thumbnail extra long', () => (
    <MuiThemeProvider>
      <Thumbnail
        data={extraLongData}
        title={thumbTitle}
        subtitle={thumbSubtitle}
        image={thumbImage}
        actions={actions} />
    </MuiThemeProvider>
  ));
storiesOf('list-view-page', module)
  .add('thumbnail actions-bottom', () => (
    <MuiThemeProvider>
      <Thumbnail
        data={data[0]}
        title={thumbTitle}
        subtitle={thumbSubtitle}
        image={thumbImage}
        actionsPosition='bottom'
        actions={actions} />
    </MuiThemeProvider>
  ));
storiesOf('list-view-page', module)
  .add('thumbnail hide-image actions-top emphasize', () => (
    <MuiThemeProvider>
      <Thumbnail
        data={data[0]}
        title={thumbTitle}
        subtitle={thumbSubtitle}
        image={thumbImage}
        hideImage={true}
        actionsPosition='top'
        emphasize={true}
        actions={actions} />
    </MuiThemeProvider>
  ));
storiesOf('list-view-page', module)
  .add('thumbnail image-only', () => (
    <MuiThemeProvider>
      <Thumbnail
        data={data[0]}
        title={thumbTitle}
        subtitle={thumbSubtitle}
        image={thumbImage}
        imageOnly={true}
        actions={actions} />
    </MuiThemeProvider>
  ));
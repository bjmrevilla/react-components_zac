'use strict';

import React from 'react';
import NavProfile from '../nav-profile/nav-profile.component.jsx';
import NavMessages from '../nav-messages/nav-messages.component.jsx';
import NavNotifs from '../nav-notifs/nav-notifs.component.jsx';
import styles from './app-bar.scss';
import MaterialUIAppBar from 'material-ui/AppBar';

export default class AppBar extends React.Component {
  constructor(props) {
    super(props);
  }

  // TODO handle navmessages and navnotifs prop name collision:
  // onSeeAllTouchTap and showSeeAll
  static propTypes = {
    onDrawerToggle: React.PropTypes.func,
    title: React.PropTypes.string.isRequired,
    account: React.PropTypes.object.isRequired,
    showNavProfile: React.PropTypes.bool,
    showNavMessages: React.PropTypes.bool,
    showNavNotifs: React.PropTypes.bool,
    isDrawerOpen: React.PropTypes.bool
  };

  static defaultProps = {
    showNavProfile: true,
    showNavMessages: true,
    showNavNotifs: true
  };

  handleClickBurger = (event) => {
    const toggled = !this.props.isDrawerOpen;

    this.props.onDrawerToggle(toggled);
  };

  getNavProfile = () => {
    return (this.props.showNavProfile)
      ? <NavProfile
        account={this.props.account}
        onProfileTouchTap={this.props.onProfileTouchTap}
        onSettingsTouchTap={this.props.onSettingsTouchTap}
        onLogoutTouchTap={this.props.onLogoutTouchTap}
        showProfile={this.props.showProfile}
        showSettings={this.props.showSettings}
        showLogout={this.props.showLogout} />
      : '';
  };

  getNavMessages = () => {
    return (this.props.showNavMessages)
      ? <NavMessages
        messages={this.props.account.messages}
        onMessageTouchTap={this.props.onMessageTouchTap}
        onSeeAllTouchTap={this.props.onSeeAllTouchTap}
        showSeeAll={this.props.showSeeAll} />
      : '';
  };

  getNavNotifs = () => {
    return (this.props.showNavNotifs)
      ? <NavNotifs
        notifications={this.props.account.notifications}
        onNotificationTouchTap={this.props.onNotificationTouchTap}
        onSeeAllTouchTap={this.props.onSeeAllTouchTap}
        showSeeAll={this.props.showSeeAll} />
      : '';
  };

  render = () => {
    return (
      <MaterialUIAppBar
        className={styles.root}
        title={this.props.title}
        onLeftIconButtonTouchTap={this.handleClickBurger}>
        {this.getNavProfile()}
        {/*{this.getNavMessages()}*/}
        {this.getNavNotifs()}
      </MaterialUIAppBar>
    );
  }
}

'use strict';

import React from 'react';
import Popup from './popup.component.jsx';
import styles from './nav-messages.scss';
import IconButton from 'material-ui/IconButton';
import Popover from 'material-ui/Popover';

export default class NavMessages extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isPopoverOpen: false,
      anchorEl: null
    };
  }

  static propTypes = {
    messages: React.PropTypes.array, //of message, TODO fix this
    onMessageTouchTap: React.PropTypes.func,
    onSeeAllTouchTap: React.PropTypes.func,
    showSeeAll: React.PropTypes.bool
  }

  static defaultProps = {
    showSeeAll: true
  }

  handleTouchTap = (event) => {
    event.preventDefault();

    this.setState({
      isPopoverOpen: true,
      anchorEl: event.currentTarget
    });
  }

  handleRequestClose = () => {
    this.setState({
      isPopoverOpen: false
    });
  }

  render = () => {
    return (
      <div className={styles.root}>
        <IconButton
          iconClassName={styles.icon}
          onTouchTap={this.handleTouchTap}
          tooltip='Messages'>
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M20 2H4c-1.1 0-1.99.9-1.99 2L2 22l4-4h14c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zm-2 12H6v-2h12v2zm0-3H6V9h12v2zm0-3H6V6h12v2z"/></svg>
        </IconButton>
        <Popover
          open={this.state.isPopoverOpen}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
          targetOrigin={{ horizontal: 'right', vertical: 'top' }}
          onRequestClose={this.handleRequestClose}>
          <Popup
            messages={this.props.messages}
            onMessageTouchTap={this.props.onMessageTouchTap}
            onSeeAllTouchTap={this.props.onSeeAllTouchTap}
            showSeeAll={this.props.showSeeAll} />
        </Popover>
      </div>
    );
  }
}
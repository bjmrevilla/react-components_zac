import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import DrawerTest from './drawer.test.js'
import {testAccount,
  testOnDrawerToggle,
  menus,
  themes} from './fixtures'

storiesOf('user-landing', module)
  .add('drawer-test', () => (
    <MuiThemeProvider>
      <DrawerTest />
    </MuiThemeProvider>
  ));
'use strict';

import React from 'react';
import Drawer from 'material-ui/Drawer';
import AppBar from 'material-ui/AppBar';

export default class DrawerTest extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false
    };
  }

  static propTypes = {}

  onTouchTap = () => {
    this.setState({
      open: !this.state.open
    });
  }

  render = () => {
    return (
      <div>
        <AppBar
          onLeftIconButtonTouchTap={this.onTouchTap}
          title='App' />
        <Drawer
          open={this.state.open}
          docked={true}
          containerStyle={{top: '72px'}}
          onRequestChange={this.onTouchTap}
          zDepth={2} />
      </div>
    );
  }
}
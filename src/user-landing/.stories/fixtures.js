export const testOnDrawerToggle = (open) => {
  console.log('toggle!');
  console.log('open: ' + open);
}

export const messages = [
  {
    id: 1,
    title: 'Username',
    subtitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque dignissim velit non metus tristique',
    creator: {
      pic: 'http://www.material-ui.com/images/ok-128.jpg'
    }
  },
  {
    id: 2,
    title: 'Username 2',
    subtitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque dignissim velit non metus tristique',
    creator: {
      pic: 'http://www.material-ui.com/images/ok-128.jpg'
    }
  },
  {
    id: 3,
    title: 'Username 3',
    subtitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque dignissim velit non metus tristique',
    creator: {
      pic: 'http://www.material-ui.com/images/ok-128.jpg'
    }
  }
];

export const notifications = [
  {
    id: 1,
    type: 'Username',
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque dignissim velit non metus tristique',
  },
  {
    id: 2,
    type: 'Username',
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque dignissim velit non metus tristique',
  },
  {
    id: 3,
    type: 'Username',
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque dignissim velit non metus tristique',
  }
];

export const testAccount = {
  username: 'username',
  email: 'email',
  messages: messages,
  notifications: notifications,
  pic: 'http://www.material-ui.com/images/ok-128.jpg'
}

export const subMenus = [
  {
    key: 101,
    icon: '',
    label: 'sub zero',
    onTouchTap: (key) => {}
  },
  {
    key: 102,
    icon: '',
    label: 'scorpion',
    onTouchTap: (key) => {}
  }
];

export const menus = [
  {
    key: 1,
    icon: '',
    label: 'go to Google',
    link: 'google.com'
  },
  {

    key: 2,
    icon: '',
    label: 'menu menu',
    onTouchTap: (key) => {}
  },
  {
    key: 3,
    icon: '',
    label: 'nested',
    childMenus: subMenus
  }
];

export const themes = [
  'theme 1',
  'theme 2',
  'theme 3'
];
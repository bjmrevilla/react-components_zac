import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import UserLanding from '..';
import Chat from '../../chat/chat.component.jsx';
import ProfilePage from '../../profile-page/profile-page.component.jsx'
import {testAccount,
  menus,
  themes} from './fixtures'
import {
  accounts,
  currentAccount,
  longMessage,
  messages,
  room1,
  rooms,
  search,
  createRoom,
  sendMessage,
  updateRoomDetails
} from '../../chat/.stories/fixtures'
import {testAccount as profileAccount,
  testOnChange,
  testSave,
  testDelete,
  testChangePassword,
  testUploadProfilePic,
  testCancel,
  testSourceRoles,
  testSourceViewFilters,
  testSourceExemptionFilters} from '../../profile-page/.stories/fixtures';

require('./drawer.js');
require('./nav-profile.js');

storiesOf('user-landing', module)
  .add('with chat', () => (
    <MuiThemeProvider>
      <UserLanding
        account={testAccount}
        menus={menus}
        themes={themes}
        title='Chat'>
        <Chat
          currentAccount={currentAccount}
          room={room1}
          rooms={rooms}
          accounts={accounts}
          search={search}
          createRoom={createRoom}
          sendMessage={sendMessage}
          updateRoomDetails={updateRoomDetails}
          />
      </UserLanding>
    </MuiThemeProvider>
  ));

storiesOf('user-landing', module)
  .add('with profile-page', () => (
    <MuiThemeProvider>
      <UserLanding
        account={testAccount}
        menus={menus}
        themes={themes}
        title='Profile'>
        <ProfilePage account={profileAccount}
          onChange={testOnChange}
          save={testSave}
          delete={testDelete}
          changePassword={testChangePassword}
          uploadProfilePic={testUploadProfilePic}
          cancel={testCancel}
          sourceRoles={testSourceRoles}
          sourceViewFilters={testSourceViewFilters}
          sourceExemptionFilters={testSourceExemptionFilters} />
      </UserLanding>
    </MuiThemeProvider>
  ));
import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import NavProfile from '../nav-profile/nav-profile.component.jsx'
import {testAccount} from './fixtures';

storiesOf('user-landing', module)
  .add('nav-profile', () => (
    <MuiThemeProvider>
      <NavProfile account={testAccount} showLogout={false} />
    </MuiThemeProvider>
  ));
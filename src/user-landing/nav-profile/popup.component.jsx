'use strict';

import React from 'react';
import Avatar from 'material-ui/Avatar';
import Account from 'material-ui/svg-icons/action/account-circle';
import RaisedButton from 'material-ui/RaisedButton';
import styles from './popup.scss';
import defaultImage from '../../assets/kc-user-profile-photo.png';

export default class Popup extends React.Component {
  render = () => {
    const { account: { username, email, pic } } = this.props;
    const {
      onProfileTouchTap,
      onSettingsTouchTap,
      onLogoutTouchTap,
      showProfile,
      showSettings,
      showLogout
    } = this.props;

		const _profSrc = pic
			? `/cdn/${pic}`
			: defaultImage;

    return (
      <div className={styles.root}>
        <img
          className={styles.avatar}
          src={_profSrc}
          />
        <h2>{username}</h2>
        <h3>{email}</h3>
        {showProfile
          ? <RaisedButton
            className={styles.button}
            label='Profile'
            onClick={onProfileTouchTap}
            fullWidth
            primary
            />
          : ''}
        {showSettings
          ? <RaisedButton
            className={styles.button}
            label='Settings'
            onClick={onSettingsTouchTap}
            fullWidth
            primary
            />
          : ''}
        {showLogout
          ? <RaisedButton
            className={styles.button}
            label='Logout'
            onClick={onLogoutTouchTap}
            fullWidth
            primary
            />
          : ''}
      </div>
    );
  }
}

'use strict';

import React from 'react';
import Popup from './popup.component.jsx';
import styles from './nav-notifs.scss';
import IconButton from 'material-ui/IconButton';
import Popover from 'material-ui/Popover';

export default class NavNotifs extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isPopoverOpen: false,
      anchorEl: null
    };
  }

  static propTypes = {
    notifications: React.PropTypes.arrayOf(React.PropTypes.object),
    onNotificationTouchTap: React.PropTypes.func,
    onSeeAllTouchTap: React.PropTypes.func,
    showSeeAll: React.PropTypes.bool
  };

  static defaultProps = {
    showSeeAll: true
  };

  handleTouchTap = (event) => {
    event.preventDefault();

    this.setState({
      isPopoverOpen: true,
      anchorEl: event.currentTarget
    });
  };

  handleRequestClose = () => {
    this.setState({
      isPopoverOpen: false
    });
  };

  render = () => {
    return (
      <div className={styles.root}>
        <IconButton
          // iconClassName={styles.icon}
          onTouchTap={this.handleTouchTap}
          tooltip='Notifications'>
          <svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
						<path d="M12 22c1.1 0 2-.9 2-2h-4c0 1.1.89 2 2 2zm6-6v-5c0-3.07-1.64-5.64-4.5-6.32V4c0-.83-.67-1.5-1.5-1.5s-1.5.67-1.5 1.5v.68C7.63 5.36 6 7.92 6 11v5l-2 2v1h16v-1l-2-2z"/>
					</svg>
        </IconButton>
        <Popover
          open={this.state.isPopoverOpen}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
          targetOrigin={{ horizontal: 'right', vertical: 'top' }}
          onRequestClose={this.handleRequestClose}>
          <Popup
            notifications={this.props.notifications}
            onNotificationTouchTap={this.props.onNotificationTouchTap}
            onSeeAllTouchTap={this.props.onSeeAllTouchTap}
            showSeeAll={this.props.showSeeAll} />
        </Popover>
      </div>
    );
  }
}

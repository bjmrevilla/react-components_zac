/**
 * Created by metsys on 11/16/16.
 */
'use strict';

export default function (data, filter) {
	if (!filter) return [];

	let result = [];

	// worst case O(n^2) operation!
	for (let i in data) {
		if (!data.hasOwnProperty(i)) continue;
		let obj = data[i];

		for (let key in obj) {
			if (!obj.hasOwnProperty(key)) continue;
			let stringified = '' + obj[key];

			stringified = stringified.toLowerCase();
			filter = filter.toLowerCase();

			if (stringified.indexOf(filter) >= 0) {
				result.push(obj);
				break;
			}
		}
	}

	return result;
}

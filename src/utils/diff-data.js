/**
 * Created by metsys on 11/16/16.
 */
'use strict';
const log = require('debug')('DiffData::Utils');

import _ from 'lodash';

export function diff (originalData = { }, changedData = { }, options, rootDiff = true) {
	if (rootDiff) log(`orig data ${JSON.stringify(originalData, null, 2)}`);
	if (rootDiff) log(`changed data ${JSON.stringify(changedData, null, 2)}`);

	const { keysToExempt = [], keysToRemove = [] } = options || {};

	// Exempted from comparison
	const exempted = keysToExempt.concat(keysToRemove);

	return _.reduce(changedData, (res, val, key) => {
		// Exempted
		if (exempted.indexOf(key) !== -1) return res;

		// Newly added property
		if (!originalData.hasOwnProperty(key)) return _.set(res, key, val);

		// Original Data
		const origValue = originalData[key];

		// Date diff
		if (val instanceof Date && origValue instanceof Date) {
			return val.getTime() === origValue.getTime()
				? res
				: _.set(res, key, val);
		}

		// Recursive array diff
		if (Array.isArray(val)) {
			// Compare to original
			const changed = diffArray(origValue, val, options);
			return _.isEmpty(changed)
				? res
				: _.set(res, key, changed);
		}

		// Recursive object diff
		if (_.isPlainObject(val)) {
			const changed = diff(origValue, val, options, false);
			return _.isEmpty(changed)
				? res
				: _.set(res, key, changed);
		}

		// Scalar diff
		return _.isEqual(val, origValue)
			? res
			: _.set(res, key, val);

	}, { });
}

export default diff;

function isArrayValueEqual (a, b, options){
	// Entities are objects
	if ([a, b].every(_.isPlainObject)) {
		const { compareUsingKeys = ['_id', 'id', 'name'] } = options || {};

		// Equal if objects have equal values in one of the keys (compareUsingKeys)
		return !!compareUsingKeys.find(key => a.hasOwnProperty(key) && _.isEqual(a[key], b[key]))
	}

	// Entities are scalars
	return _.isEqual(a, b);
}

function diffArray (orig = [], changed = [], options) {
	log(`orig arr ${JSON.stringify(orig, null, 2)}`);
	log(`changed arr ${JSON.stringify(changed, null, 2)}`);

	// Changes
	const changes = _.reduce(changed, (res, val) => {
		// Find orig data
		const origValue = orig.find(e => isArrayValueEqual(e, val, options));

		// Newly added data
		if (!origValue) return res.concat(val);

		// Recursive object diff
		if (_.isPlainObject(val)) {
			const changed = diff(origValue, val, null, false);
			return _.isEmpty(changed)
				? res
				: res.concat(val);
		}

		// Scalar diff
		return _.isEqual(val, origValue)
			? res
			: res.concat(val);
	}, []);

	// Deleted
	const deleted = _.differenceWith(orig, changed, (a, b) => isArrayValueEqual(a, b, options))
		// Add remove flags
		.map(value => {
			if (_.isPlainObject(value)) {
				return _.set(value, '__action', 'remove');
			}

			return {
				value,
				__action: 'remove'
			}
		});

	return changes.concat(deleted);
}

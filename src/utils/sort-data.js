/**
 * Created by metsys on 11/16/16.
 */
'use strict';

export default function (data = [], key) {
	data.sort((a, b) => {
		let first = (typeof a[key] === 'string')
			? a[key].toLowerCase()
			: a[key];
		let second = (typeof b[key] === 'string')
			? b[key].toLowerCase()
			: b[key];

		if (first < second) {
			return -1;
		}
		if (first > second) {
			return 1;
		}

		return 0;
	});
}

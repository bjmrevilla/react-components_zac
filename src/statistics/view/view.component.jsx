'use strict';

import React from 'react';
import styles from './view.scss';
import Paper from 'material-ui/Paper';
import {BarChart,
  PieChart,
  LineChart,
  AreaChart,
  ScatterplotChart,
  Legend} from 'react-easy-chart';

export default class View extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    displayType: React.PropTypes.oneOf(
      ['bar', 'pie', 'line', 'area', 'scatterplot']),
    displaySize: React.PropTypes.oneOf(
      ['normal', 'wide', 'small']),
    rootWidth: React.PropTypes.number.isRequired,
    title: React.PropTypes.string,
    description: React.PropTypes.string,
    data: React.PropTypes.arrayOf(React.PropTypes.object),
    xLabel: React.PropTypes.string,
    xKey: React.PropTypes.arrayOf(React.PropTypes.string),
    yLabel: React.PropTypes.string,
    yKey: React.PropTypes.any,
    typeLabel: React.PropTypes.string,
    typeKey: React.PropTypes.arrayOf(React.PropTypes.string),
    showLegend: React.PropTypes.bool
  }

  static defaultProps = {
    displaySize: 'normal',
    xKey: ['x'],
    yKey: ['y'],
    typeKey: ['type'],
    showLegend: true
  }

  getViewWidth = () => {
    // subtract 20 for margin
    if (this.props.displaySize === 'normal') {
      return this.props.rootWidth / 2 - 20;
    }
    if (this.props.displaySize === 'wide') {
      return this.props.rootWidth - 20;
    }
    if (this.props.displaySize === 'small') {
      return this.props.rootWidth / 4 - 20;
    }
  }

  getViewHeight = () => {
    //make rectangular if wide, make square otherwise
    if (this.props.displaySize === 'wide') {
      return this.props.rootWidth / 2 - 20;
    }
   
    return this.getViewWidth();
  }

  getGraphWidth = () => {
    // must be lower than view width
    return this.getViewWidth() * 0.8;
  }

  getGraphHeight = () => {
    // must be lower than view height
    return this.getViewHeight() * 0.7;
  }

  getPieGraphSize = () => {
    // must be lower than normal graph width/height
    return this.getViewHeight() * 0.6;
  }

  getGraph = () => {
    if (this.props.displayType === 'bar') {
      return (
        <BarChart
          colorBars
          axes
          grid
          width={this.getGraphWidth()}
          height={this.getGraphHeight()}
          data={this.props.data}
          axisLabels={{x: this.props.xLabel, y: this.props.yLabel}} />
      );
    }
    if (this.props.displayType === 'pie') {
      var legend = (this.props.showLegend)
        ? <Legend data={this.props.data} dataId={'key'} horizontal />
        : '';
      return (
        <div>
          <PieChart
            labels
            size={this.getPieGraphSize() }
            data={this.props.data} />
          {legend}
        </div>
      );
    }
    if (this.props.displayType === 'line') {
      return (
        <LineChart
          axes
          grid
          verticalGrid
          dataPoints
          width={this.getGraphWidth()}
          height={this.getGraphHeight()}
          data={this.props.data}
          axisLabels={{x: this.props.xLabel, y: this.props.yLabel}} />
      );
    }
    if (this.props.displayType === 'area') {
      return (
        <AreaChart
          axes
          grid
          verticalGrid
          width={this.getGraphWidth()}
          height={this.getGraphHeight()}
          data={this.props.data}
          axisLabels={{x: this.props.xLabel, y: this.props.yLabel}} />
      );
    }
    if (this.props.displayType === 'scatterplot') {
      // For some infuriating reason, scatterplot chart bleeds out of set size
      // width/height doesn't take axes into account, I think
      // TODO: figure this out; the size hardcoding is bad enough as it is
      var legend = (this.props.showLegend)
        ? <Legend data={this.props.data} dataId={'type'} horizontal />
        : '';
      return (
        <div>
          <ScatterplotChart
            axes
            width={this.getGraphWidth() }
            height={this.getGraphHeight() }
            data={this.props.data}
            axisLabels={{ x: this.props.xLabel, y: this.props.yLabel }} />
          {legend}
        </div>
      );
    }
  }

  render() {
    return (
      <Paper className={styles.root}
        style={{width: this.getViewWidth(), height: this.getViewHeight()}}>
        <div className={styles.flexbox}>
          <span className={styles.title}>
            {this.props.title}
          </span>
          <span className={styles.description}>
            {this.props.description}
          </span>
          <div className={styles.graph}>
            {this.getGraph() }
          </div>
        </div>
      </Paper>
    );
  }
}
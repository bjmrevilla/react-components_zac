'use strict';

import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import CircularProgress from 'material-ui/CircularProgress';
import TagList from '../../tag-list';
import styles from './question-thread-creation.scss';

export default class QuestionThreadCreation extends React.Component {
  constructor(props) {
    super(props);
		this.state = {
			title: '',
			body: '',
			status: '',
			tags: []
		};
  }

  static propTypes = {
    sourceTags: React.PropTypes.arrayOf(React.PropTypes.object),
    create: React.PropTypes.func,
    searchTag: React.PropTypes.func
  };

  handleCreate = () => {
  	const { title, body, tags } = this.state;
		this.setState({ status: 'loading' });

    this.props.create( { title, body, tags }, (err) => {
    	console.log(`# create fn`);
      if(err) {
      	// TODO: change this
				alert(err.message);
      }
      this.setState({ status: '' })
    });
  };

	handleCancel = () => {
		this.setState({
			title: '',
			body: '',
			createStatus: '',
			tags: []
		})
	};

	handleChange = (event) => {
		this.setState({
			[event.target.id]: event.target.value,
		});
	};


  render = () => {
  	const { title, body, tags, status } = this.state;
		const { sourceTags, addTagAction, searchTag } = this.props;

    return (
      <Paper className={styles.root} zDepth={2}>
        <div className={styles.cover}>
          <div className={styles.auxButtons}>
						{
							status === 'loading'
								? <CircularProgress />
								: <div>
										<FlatButton
											label='Cancel'
											onClick={this.handleCancel}
											secondary
										/>
										<RaisedButton
											label='Create'
											onClick={this.handleCreate}
											primary
											disabled={!title || !body}
										/>
									</div>
						}

          </div>
          <div>
            <TextField
              id='title'
              floatingLabelText='Title'
              value={title}
							onChange={this.handleChange}
							errorText={title? undefined: `This field is required`}
              fullWidth
            />
            <TextField
              id='body'
              floatingLabelText='Body'
              value={body}
							onChange={this.handleChange}
							errorText={body? undefined: `This field is required`}
              fullWidth
            />
          </div>

          <TagList
						title='Tags'
						description='Tags for the topic'
						label='Forum tags'

						tags={tags}
						addFunction={addTagAction}
						search={searchTag}
						sourceItems={sourceTags}
						onTagsChanged={tags => this.setState({ tags })}
					/>
        </div>
      </Paper>
    );
  }
}

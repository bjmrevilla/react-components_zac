'use strict';

import React from 'react';
import styles from './post-creator.scss';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import CircularProgress from 'material-ui/CircularProgress';

export default class PostCreator extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      disableFlag: false
    }
  }

  static propTypes = {
    answerText: React.PropTypes.string,
    onAnswer: React.PropTypes.func,
    onType: React.PropTypes.func
  };

  handleAnswer = (e) => {
    e.preventDefault();
    if(!this.props.checkInputIfEmpty()) {
      this.setState({ disableFlag: true });
      this.props.onAnswer();
    }
  };

  render = () => {
    return (
      <form className={styles.root} onSubmit={this.handleAnswer}>
        <span className={styles.textbox}>
          <TextField
            type='text'
            value={this.props.answerText}
            onChange={this.props.onType}
            hintText='Type your answer...'
            disabled={this.state.disableFlag}
            multiLine={true}
            fullWidth={true} />
        </span>
        <div className={styles.answer}>
          {(this.state.disableFlag)
              ? <CircularProgress size={0.5}/>
              : <RaisedButton
                  type='submit'
                  label='Answer'
                  disabled={this.state.disableFlag}
                  primary={true} />
          }

        </div>
      </form>
    );
  }
}

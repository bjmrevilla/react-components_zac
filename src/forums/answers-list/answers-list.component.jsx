'use strict';

import React from 'react';
import Answer from '../answer/answer.component.jsx';
import PostCreator from './post-creator.component.jsx';
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress';
import styles from './answers-list.scss';

export default class AnswersList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      answerText: '',
      loadingFlag: false
    };
  }

  static propTypes = {
    answers: React.PropTypes.arrayOf(React.PropTypes.object),
    readOnly: React.PropTypes.bool,
    canAnswer: React.PropTypes.bool,
    answer: React.PropTypes.func,
    loadMore: React.PropTypes.func
  };

  static defaultProps = {
    readOnly: false,
    canAnswer: true
  };

  handleAnswer = () => {
    this.setState({ answerText: '' });
		const post = {};
    this.props.answer(post, (err) => {
      //loading
      if (err) {
        // disable
      } else {
        // loading
      }
    });
  };

  checkInputIfEmpty = () => {
    const { answerText } = this.state;
    if(answerText.length < 3 || answerText.trim() === '') {
      return true;
    }

    return false;
  };

  handleType = (e) => {
    this.setState({
      answerText: e.target.value
    });
  };

  handleLoadMore = () => {
    this.setState({ loadingFlag: true });
    this.props.loadMore(options, (err) => {
      if(err) {

      } else {
        this.setState({ loadingFlag: false });
      }
    });
  };

  getAnswers = () => {
    return this.props.answers.map((answer) => {
      return <Answer
        chooseAnswerFlag={this.props.chooseAnswerFlag}
        chooseAnswer={this.props.chooseAnswer}
        answer={answer} />;
    });
  };

  getPostCreator = () => {
    return (this.props.canAnswer && !this.props.readOnly)
      ? <PostCreator
          answerText={this.state.answerText}
          checkInputIfEmpty={this.checkInputIfEmpty}
          onType={this.handleType}
          onAnswer={this.handleAnswer} />
      : '';
  };

  render = () => {
    return (
      <div>
        {((this.props.answers != null) &&
          (typeof this.props.answers != 'undefined') &&
          (this.props.answers).length !== 0)
          ? <div className={styles.root}>
              {this.getAnswers()}
              {(this.state.loadingFlag)
                ? <CircularProgress size={0.5} />
                : <RaisedButton
                    label='View more'
                    primary
                    onTouchTap={this.handleLoadMore} />
              }
            </div>
          : ''
        }
        {this.getPostCreator()}
      </div>
    );
  }
}

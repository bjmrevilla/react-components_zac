'use strict';

import React from 'react';
import styles from './forums.scss';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card'
import PostsList from '../curriculum-view/posts-list/posts-list.component.jsx';
import Answer from './answer/answer.component.jsx';
import TagList from '../tag-list';
import AnswerList from './answers-list/answers-list.component.jsx';
import CircularProgress from 'material-ui/CircularProgress';
import Paper from 'material-ui/Paper';
import Vote from './answer/votes.component.jsx';
import {diff} from '../list-view-page/utils';

export default class Forums extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			loadingFlag: false,
			hasChosenAnswer: false
		}
	}

	// Clone Original
	handleTempState = (props) => {
		const {questionThread} = props || this.props;
		this.setState({
			tempQuestionThread: {...questionThread}
		})
	};

	componentWillReceiveProps = (newProps) => {
		this.handleTempState(newProps)
	};

	componentWillMount = () => {
		this.handleTempState()
	};


	static propTypes = {
		questionThread: React.PropTypes.object.isRequired,
		readOnly: React.PropTypes.bool,
		vote: React.PropTypes.func,
		save: React.PropTypes.func,
		canChooseAnswer: React.PropTypes.bool,
		chooseAnswer: React.PropTypes.func
	};

	static defaultProps = {
		readOnly: false,
		canChooseAnswer: true
	};

	handleSave = () => {
		const {save, questionThread} = this.props;
		const {tempQuestionThread} = this.state;
		this.setState({loadingFlag: true});

		const changes = diff(questionThread, tempQuestionThread);

		save(changes, (err) => {
			if (err) {
				// TODO: change this
				alert(err.message);
			}

			this.setState({loadingFlag: false});
		});
	};

	handleQuestionVote = (direction) => {
		const {vote, questionThread} = this.props;
		this.setState({loadingFlag: true});

		vote({_id: questionThread._id, direction}, (err) => {
			if (err) {

			} else {
				this.setState({loadingFlag: false});
			}
		});
	};

	handleChooseAnswer = (answerID) => {
		const {questionThread, chooseAnswer} = this.props;

		this.setState({loadingFlag: true});
		chooseAnswer({_id: questionThread._id, answer: answerID}, (err) => {
			if (err) {

			} else {
				this.setState({loadingFlag: false});
			}
		});
	};

	/*handleComment = () => {

	};

	getComments = () => {
		return (
			<PostsList
				posts={this.props.questionThread.comments}
				readOnly={this.props.readOnly}
				canPost={this.props.canPost}
				post={this.props.post}
				postCreatorPosition={this.props.postCreatorPosition}
				loadMore={this.props.loadMore}/>
		);
	};

	getCommentButton = () => {
		return (
			<RaisedButton
				label='Comment'
				disabled={this.state.loadingFlag}
				onClick={this.handleComment}
				primary/>
		);
	};*/

	getAuxButtons = () => {
		return (
			<div className={styles.auxButtons}>
				<FlatButton
					disabled={this.state.loadingFlag}
					label='Cancel'
					secondary/>
				<RaisedButton
					label='Save'
					onTouchTap={this.handleSave}
					disabled={this.state.loadingFlag}
					primary/>
			</div>
		);
	};

	getTopicBody = () => {
		const { questionThread, readOnly } = this.props;
		const {title, body, voteCount, creator, tags} = questionThread;

		return (
			<Paper
				zDepth={3}
				style = {{
					padding: 30
				}}
			>
				<div className={styles.wrapper}>
					<div className={styles.voteControl}>
						<Vote
							voteFunction={this.handleQuestionVote}
							votes={voteCount}
						/>
					</div>

					<div className={styles.thread}>
						<span className={styles.title}>{title}</span>
						<div className={styles.threadBody}> {body} </div>
						<span className={styles.creatorInfo}>by {creator.username} </span>
					</div>
				</div>

				<div className={styles.tags}>
					<TagList
						showTitle={false}
						showDescription={false}
						readOnly={ readOnly }
						tags={ tags }/>
				</div>
			</Paper>
		)
	};

	render = () => {
		const { chosenAnswer } = this.props.questionThread;

		return (
			<div className={styles.root}>
				{
					!this.props.readOnly && this.getAuxButtons()
				}

				{
					this.getTopicBody()
				}

				{/*{(comments != null || typeof comments != 'undefined')
					? <div className={styles.comments}>
					{this.getComments()}
				</div>
					: ''
				}*/}
				{
					(chosenAnswer !== null) &&
					<Answer
						chosenAnswer
						answer={chosenAnswer}
					/>
				}
				<div className={styles.otherAnswers}>
					<AnswerList
						chooseAnswerFlag={this.props.canChooseAnswer}
						chooseAnswer={this.handleChooseAnswer}
						answers={this.props.questionThread.answers}/>
				</div>
			</div>
		);
	}
}

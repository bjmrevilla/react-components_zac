'use strict';

import React from 'react';
import Votes from './votes.component.jsx';
import styles from './answer.scss';
import PostsList from '../../curriculum-view/posts-list/posts-list.component.jsx';
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import ActionCheckCircle from 'material-ui/svg-icons/action/check-circle';

const inlineStyles = {
  icon: {
    height: 36,
    width: 36
  }
};

export default class Answer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loadingComments: false
    };
  }

  static propTypes = {
    answer: React.PropTypes.object,
    canVote: React.PropTypes.bool,
    voteAnswer: React.PropTypes.func,
    readOnly: React.PropTypes.bool,
    canComment: React.PropTypes.bool,
    loadComments: React.PropTypes.func
  };

  static defaultProps = {
    canVote: false,
    readOnly: true,
    canComment: false
  };

  handleVote = (direction) => {
    const { answer, voteAnswer } = this.props;
    this.setState({ loadingComments: true });

    voteAnswer({_id: answer._id, direction: direction}, (err) => {
      if(err){

      } else {
        this.setState({ loadingComments: false });
      }
    });
  };

  handleShowComments = () => {
    this.setState({ loadingComments: true });
    this.props.loadComments(options, (err) => {
      if(err) {
        // Error
      } else {
        //Load Comments.
        this.setState({ loadingComments: false });
      }
    });
  };

  handleChooseAnswer = () => {
    const { answer } = this.props;
    this.setState({ loadingComments: true });
    this.props.handleChooseAnswer(answer._id);
  };

  getComments = () => {
    return(
      (this.props.canComment)
        ? <PostsList
            posts={this.props.answer.comments}
            readOnly={this.props.readOnly}
            postCreatorPosition='bottom'
            loadMore={this.props.loadMore} />
        : ''
    );
  };

  getChooseAnswerButton = () => {
    return(
      <RaisedButton
        className={styles.showCommentsButton}
        onTouchTap={this.handleChooseAnswer}
        label='Choose Answer'
        disabled={this.state.loadingComments}
        secondary
        />
    );
  };

  render = () => {
    const {
      chooseAnswerFlag,
      chosenAnswer,
      answer
    } = this.props;

    const {
      creator,
      body,
      voteCount,
      createdAt,
      comments
    } = answer;

    return (
      <div className={styles.root}>
        <Card>
          <CardHeader
            title={creator.username}
            subtitle={creator.email}
            avatar={creator.pic}>
            {(createdAt != null || createdAt != 'undefined')
              ? <span className={styles.date}>{createdAt}</span>
              : ''
            }

          </CardHeader>
          <CardText>
            {body}
          </CardText>
          <CardActions>
            <div className={styles.actions}>
              <Votes
                fromAnswer={true}
                voteFunction={this.handleVote}
                votes={voteCount}/>
              <div className={styles.showComments}>
                { (this.state.loadingComments)
                  ? <CircularProgress size={0.5}/>
                  : ''
                }
                { (chooseAnswerFlag)
                    ? this.getChooseAnswerButton()
                    : ''
                }
                {
                  (chosenAnswer)
                    ? <ActionCheckCircle
                        className={styles.showCommentsButton}
                        style={inlineStyles.icon}
                        color='green' />
                    : ''
                }
                <RaisedButton
                  className={styles.showCommentsButton}
                  onTouchTap={this.handleShowComments}
                  primary={true}
                  disabled={this.state.loadingComments}
                  label='Show Comments'/>
              </div>
            </div>
          </CardActions>
        </Card>
        {(comments != null || typeof comments == 'undefined')
          ? this.getComments()
          : ''
        }
      </div>
    );
  }
}

// TODO
//  - Voting

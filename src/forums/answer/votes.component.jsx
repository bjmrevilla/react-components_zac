'use strict';

import React from 'react';
import styles from './votes.scss';
import SvgIcon from 'material-ui/SvgIcon';
import IconButton from 'material-ui/IconButton';
import HardwareKeyboardArrowUp from 'material-ui/svg-icons/hardware/keyboard-arrow-up';
import HardwareKeyboardArrowDown from 'material-ui/svg-icons/hardware/keyboard-arrow-down';

const inlineStyles = {
  icon: {
    width: 36,
    height: 36
  }, 
  buttonStyle: {
    width: 45,
    height: 45,
    padding: 0
  }
}

export default class Votes extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      choseAnAnswer: false,
      colorUp: 'black',
      colorDown: 'black'
    }
  }

  static propTypes = {
    votes: React.PropTypes.number.isRequired
  };

  handleUpVote = () => {
    if(!this.state.choseAnAnswer) {
      this.setState({colorUp: 'orange', choseAnAnswer: true});
      this.props.voteFunction('up');
    }
  };

  handleDownVote = () => {
    if(!this.state.choseAnAnswer) {
      this.setState({colorDown: 'orange', choseAnAnswer: true});
      this.props.voteFunction('down');
    }
  };

  getUpvoteButton = (position) => {
    return(
      <IconButton
        tooltip='This answer is useful!'
        tooltipPosition={position}
        iconStyle={inlineStyles.icon}
        style={inlineStyles.buttonStyle}
        onTouchTap={this.handleUpVote}>
        <HardwareKeyboardArrowUp color={this.state.colorUp} />
      </IconButton>
    );
  }

  getDownvoteButton = (position) => {
    return(
      <IconButton
        tooltip='This answer is not useful!'
        tooltipPosition={position}
        iconStyle={inlineStyles.icon}
        style={inlineStyles.buttonStyle}          
        onTouchTap={this.handleDownVote}>
        <HardwareKeyboardArrowDown color={this.state.colorDown} />
      </IconButton>
    );
    
  }

  returnToAnswer = () => {
    return(
      <div className={styles.horizontalRoot}>
        {this.getUpvoteButton('bottom-center')}
        <span> {this.props.votes} </span>
        {this.getDownvoteButton('top-center')}
      </div>
    );
  }

  returnToQuestionThread = () => {
    return(
      <div className={styles.root}>
        {this.getUpvoteButton('top-right')}
        <span> {this.props.votes} </span>
        {this.getDownvoteButton('bottom-right')}
      </div>
    );
  }

  render = () => {
    return (
      <div>
        {(this.props.fromAnswer)
          ? this.returnToAnswer()
          : this.returnToQuestionThread()}
      </div>
    );
  }
}

// Things to do 
// - Proper manipulation of states when voting or fetching data failed. 
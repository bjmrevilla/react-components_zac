'use strict';

import React from 'react';
import Answer from '../answer/answer.component.jsx';
import {storiesOf, action} from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { 
  answer
} from './fixtures.js';

storiesOf('forums', module)
  .add('answer', () => (
    <MuiThemeProvider>
      <Answer answer={answer} readOnly={false}/>
    </MuiThemeProvider>
  ))
  

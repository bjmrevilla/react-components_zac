export const creator = {
  pic: '',
  username: 'bjmrevilla',
  email: 'bjmrevilla@gmail.com'
};

export const answerInfo = {
  creator: creator,
  createdAt: '1 January 1970',
  body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  votes: 20
};

export const post = {
  creator: {
    username: 'Secretmapper',
    email: 'Secretmapper16@gmail.com',
    createdAt: 'January 16, 2016'
  },
  title: 'Post title',
  body: `Lorem ipsum dlor sit amet, consectetur adipiscing elit.
  Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
  Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.
  Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.`
};

export const comments = [
  post, post, post
];

export const answer = {
  creator: {
    username: 'bjmrevilla',
    email: 'bjmrevilla@gmail.com',
    pic: null
  },
  body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
  voteCount: 20,
};

export const answersList =  [answer, answer, answer];

export const tags = [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
];

export const questionThread = {
  title: 'muhhapi',
  body: 'De ejotu wo ral nuvjoh cukvosipe ebajasip daciho jot dojehhel apieh ic lisdo wubet az sedcintiw asdhfj.', 
  voteCount: 0,
  creator: {
    username: 'sevdi',
    email: 'wacsemdol@zawrewmi.sm',
    name: {
      firstName: 'Lilly',
      lastName: 'Haynes'
    }
  },
  questionTags: tags,
  chosenAnswer: null,
  answers: [answer, answer, answer]
}
import React from 'react';
import {storiesOf, action} from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Forums from '../forums.component.jsx';
import {
  answer, 
  answersList,
  questionThread
} from './fixtures.js';

require('./answer.js');
require('./answers-list.js');

storiesOf('forums', module)
  .add('component', () => (
    <MuiThemeProvider>
      <Forums 
        questionThread={questionThread} 
        chosenAnswer={answer}
        answer={answersList}
        postCreatorPosition={'bottom'}/>
    </MuiThemeProvider>
  ));
import React from 'react';
import {storiesOf, action} from '@kadira/storybook';
import PostCreator from '../answers-list/post-creator.component.jsx';
import AnswersList from '../answers-list/answers-list.component.jsx';
import QuestionThreadCreation from '../question-thread-creation/question-thread-creation.component.jsx';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {
  answersList,
  tags
} from './fixtures.js';

storiesOf('forums', module)
  .add('post-creator', () => (
    <MuiThemeProvider>
      <PostCreator answer={action('answer')} />
    </MuiThemeProvider>
  ));

storiesOf('forums', module)
  .add('answer-list', () => (
    <MuiThemeProvider>
      <AnswersList answers={answersList} answer={action('answer')} />
    </MuiThemeProvider>
  ));


storiesOf('forums', module)
  .add('question-creation', () => (
    <MuiThemeProvider>
      <QuestionThreadCreation sourceTags={tags}/>      
    </MuiThemeProvider>
  ))
import { configure } from '@kadira/storybook';
import injectTapEventPlugin from 'react-tap-event-plugin'

injectTapEventPlugin()

function loadStories() {
  require('../src/app/.stories/');
  require('../src/chat/.stories/');
  require('../src/curriculum-view/.stories/');
  require('../src/forums/.stories/');
  require('../src/guest-landing/.stories/');
  require('../src/list-view-page/.stories/');
  require('../src/material-creator/.stories');
  require('../src/previews/.stories');
  require('../src/profile-page/.stories/');
  require('../src/statistics/.stories/');
  require('../src/super-admin-settings-page/.stories/');
  require('../src/user-landing/.stories/');
  require('../src/forums/.stories/');
  require('../src/chapters-step/.stories/');
}

configure(loadStories, module);
